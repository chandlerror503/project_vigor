﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]

public class Item
{
    public string itemInfo;
    public string itemName;

    public Item() { }
    public Item(string name)
    {
        itemName = name;
    }

    public Item(string name, string info)
    {
        itemName = name;
        itemInfo = info;
    }

    public class Weapon : Item
    {
        public string weaponDamage;
        public string itemReichweite;

        public Weapon(string name, string damage, string reichweite)
        {
            itemName = name;
            weaponDamage = damage;
            itemReichweite = reichweite;
        }

    }

    public class Tool : Item
    {
        public string itemReichweite;

        public Tool(string name, string info, string reichweite)
        {
            itemName = name;
            itemInfo = info;
            itemReichweite = reichweite;
        }

        public Tool(string name, string info)
        {
            itemName = name;
            itemInfo = info;
        }
    }


}
