﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Diagnostics;


public class CharacterSheet : MonoBehaviour
{

    //public Button createCharacterButton;
    public string charName;
    public string volk;
    public string volkStaerke;
    public string volkStaerkeInfo;
    public string modul;
    public string stil;

    //Fähigkeiten
    public string basisf1;
    public string basisf1Info;
    public string basisf2;
    public string basisf2Info;
    public string basisf3;
    public string basisf3Info;
    public string basisStaerke;
    public string basisStaerkeInfo;
    public string stilf1;
    public string stilf1Info;
    public string stilf2;
    public string stilf2Info;
    public string stilStaerke;
    public string stilStaerkeInfo;
    public bool stilSPicked;
    public bool modulSPicked;

    public string profession;
    public List<string> professionsStaerken;
    public List<string> professionsInfo;
    public string ausstattungTyp;
    public string schild;


    public string charakterAussehen;
    public string charakterHintergrundgeschichte;
    public string charakterNotizen;
    public string[] charakterZug;

    public int level;

    public int kk;
    public int ge;
    public int wn;
    public int em;
    public int in_;
    public int wk;

    public int k;
    public int g;
    public int v;
    public int t;
    public int f;

    public int lp;
    public int gg;
    public int sl;
    public int ve;

    public List<string> schwaechen;
    public List<string> schwaechenInfo;

    public string charSchild;
    public string charWaffe;
    public string charZusatz1;
    public string charZusatz2;
    public string charZusatz3;
    public string charZusatz4;
    public string charWerkzeug1;
    public string charWerkzeug2;
    public string charWerkzeug3;
    public string charGeld;

    public int leonen;
    
    public int xp;
    public int hp;


    // Use this for initialization


    public void Awake()
    {
        // characterTraits = new string[14];
        professionsStaerken = new List<string>();
        professionsInfo = new List<string>();
        schwaechen = new List<string>();
        schwaechenInfo = new List<string>();
        charakterZug = new string[14];

        level = 1;
        //DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (charGeld == null || charGeld == "" && leonen == 0) { }
        else
        {
            charGeld = charGeld.Remove(charGeld.Length - 7);
            leonen = int.Parse(charGeld);
        }
    }

    /*public void AddSheet(CharacterSheet c )
    {
        //duplicate then add
        CharacterSheet characterSheetClone = new CharacterSheet(); 
        characterSheetClone = c;
       //
        FindObjectOfType<GameManager>().characterProfiles.Add(characterSheetClone);
    }*/

    public void Output()
    {
        string path = Application.dataPath + "/CharacterSheet.txt";
        File.WriteAllText(path, "Charakter Blatt \r\n\r\n");
        File.AppendAllText(path, "Name: " + charName + "\r\n");
        File.AppendAllText(path, "Volk: " + volk + "\r\n");
        File.AppendAllText(path, "Profession:" + profession + "\r\n");
        File.AppendAllText(path, "Modifikation: " + modul + "\r\n");
        File.AppendAllText(path, "Stil: " + stil + "\r\n");
        File.AppendAllText(path, "Aussehen:" + charakterAussehen + "\r\n\r\n");
        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");


        File.AppendAllText(path, "Wurfwerte: \r\nKörperkraft (KK): " + kk +
                                 "\r\nGeschick (GE): " + ge +
                                 "\r\nWahrnehmung (WN): " + wn +
                                 "\r\nEmpathie (EM): " + em +
                                 "\r\nIntelligenz (IN): " + in_ +
                                 "\r\nWillenskraft (WK): " + wk + "\r\n\r\n");

        File.AppendAllText(path, "Ausgleichswerte: \r\nKörper (K): " + k +
                                "\r\nGeist (G): " + g +
                                "\r\nVerstand (V): " + v +
                                "\r\nTechnik (T): " + t +
                                "\r\nFinesse (F): " + f + "\r\n\r\n");

        File.AppendAllText(path, "Attribute:\r\nLebenspunkte (LP): " + lp +
                                 "\r\nGeistige Gesundheit (GG): " + gg +
                                 "\r\nSchnelligkeit (SL): " + sl +
                                 "\r\nVigoritenergie (VE): " + ve + "\r\n\r\n");
        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");


        File.AppendAllText(path, "Stärken: \r\n");

        volkStaerkeInfo.Replace("<", "");
        volkStaerkeInfo.Replace(">", "");
        volkStaerkeInfo.Replace("b", "");
        volkStaerkeInfo.Replace("i", "");
        volkStaerkeInfo.Replace("/", "");
        File.AppendAllText(path, "Volksstärke: " + volkStaerkeInfo + "\r\n\r\n");

        File.AppendAllText(path, "Professionsstärken: \r\n\r\n");

        for (int i = 0; i < professionsStaerken.Count; i++)
        {
            int j = i + 1;
            File.AppendAllText(path, j + ": " + professionsStaerken[i] + "\r\n\r\n");
            //Professions Info
        }

        if (stilSPicked == true)
        {
            stilStaerkeInfo.Replace("<", "");
            stilStaerkeInfo.Replace(">", "");
            stilStaerkeInfo.Replace("b", "");
            stilStaerkeInfo.Replace("i", "");
            stilStaerkeInfo.Replace("/", "");
            File.AppendAllText(path, "Stilstärke:" + stilStaerkeInfo + "\r\n\r\n");
        }

        if (modulSPicked == true)
        {
            basisStaerkeInfo.Replace("<", "");
            basisStaerkeInfo.Replace(">", "");
            basisStaerkeInfo.Replace("b", "");
            basisStaerkeInfo.Replace("i", "");
            basisStaerkeInfo.Replace("/", "");
            File.AppendAllText(path, "Modulstärke:" + basisStaerkeInfo + "\r\n\r\n");
        }

        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");

        File.AppendAllText(path, "Schwächen: \r\n\r\n");

        for (int i = 0; i < schwaechen.Count; i++)
        {
            int j = i + 1;
            File.AppendAllText(path, j + ": " + schwaechen[i] + "\r\n\r\n");
            //schwächen info
        }

        //Technikart + WW ´+ KF???????
        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");


        File.AppendAllText(path, "Fähigkeiten: \r\n<b>Basisfähigkeiten:</b>" +
                                 "\r\nBasisfähigkeit 1: " + basisf1Info +
                                 "\r\nBasisfähigkeit 2: " + basisf2Info +
                                 "\r\nBasisfähigkeit 3: " + basisf3Info +
                                 "\r\n\r\n");

        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");


        File.AppendAllText(path, "Stilfähigkeiten:" +
                                 "\r\nStilfähigkeit 1: " + stilf1Info +
                                 "\r\nStilfähigkeit 2: " + stilf2Info +
                                 "\r\n\r\n");
        File.AppendAllText(path, "-----------------------------------------------------------------------------------------------------------------------\r\n\r\n");


        File.AppendAllText(path, "Ausstattung: " + ausstattungTyp + "\r\n\r\n");
        File.AppendAllText(path, "Hintergrundgeschichte: " + charakterHintergrundgeschichte + "\r\n\r\n");
        File.AppendAllText(path, "Weitere Notizen zum Charakter: " + charakterNotizen + "\r\n\r\n");
        File.AppendAllText(path, "Charakterzüge: 0-10 \r\n\r\n" +
                                 "Rechtverpflichtet - Chaotisch  <" + charakterZug[0].ToString() + ">\r\n" +
                                 "Ehrenhaft - Hinterlistig  <" + charakterZug[1].ToString() + ">\r\n" +
                                 "Introvertiert - Extrovertiert  <" + charakterZug[2].ToString() + ">\r\n" +
                                 "Pazifistisch - Streitsuchend  <" + charakterZug[3].ToString() + ">\r\n" +
                                 "Eigennützig - Gemeinnützig  <" + charakterZug[4].ToString() + ">\r\n" +
                                 "Arbeitsam - Faul  <" + charakterZug[5].ToString() + ">\r\n" +
                                 "Loyal - Selbstständig  <" + charakterZug[6].ToString() + ">\r\n" +
                                 "Nachtragend - Vergebend  <" + charakterZug[7].ToString() + ">\r\n" +
                                 "Wortgewand - Wortkarg  <" + charakterZug[8].ToString() + ">\r\n" +
                                 "Großzügig - Geizig  <" + charakterZug[9].ToString() + ">\r\n" +
                                 "Naiv - Hinterfragend  <" + charakterZug[10].ToString() + ">\r\n" +
                                 "Witzig - Ernst " + charakterZug[11].ToString() + ">\r\n" +
                                 "Traditionell - Weltoffen  <" + charakterZug[12].ToString() + ">\r\n" +
                                 "Kalkulierend - Willkürlich  <" + charakterZug[13].ToString() + ">\r\n");


        System.Diagnostics.Process.Start(path);
    }

    public void AssignProfInfo()
    {
        if (professionsStaerken.Count == 2)
        {
            professionsInfo.Clear();

            for (int i = 0; i < professionsStaerken.Count; i++)
            {
                switch (professionsStaerken[i])
                {
                    case "Coup de Grace":

                        professionsInfo.Add("Waffenangriffe richten +3 Schaden an, wenn der Gegner betäubt ist.");
                        break;

                    case "Respektable Erscheinung":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Täuschungen des Helden festzustellen ist dem Gegner um 1 Würfel erschwert.");
                        break;

                    case "Adrenalin":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der AW von Körper ist um 2 erhöht wann immer der Held unter der Hälfte seines Lebens besitzt.");
                        break;

                    case "Steuermann":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Das Steuern von Raumschiffen in Weltraumschlachten ist um 1 Würfel erleichtert.");

                        break;

                    case "Assassine":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Die kritische Erfolgschance von Angriffen ist um 2 erhöht, wenn der Held unentdeckt ist.");
                        break;

                    case "Tausend Gesichter":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Wann immer der Held sich erfolgreich für eine andere Person ausgibt, sind Geist-Proben um 1 Würfel erleichtert.");
                        break;

                    case "Kühler Kopf":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Finesse-Proben sind nicht von Erschwernissen durch fehlende GG betroffen.");
                        break;

                    case "Geheimcode":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held beherrscht eine hochkomplexe Geheimsprache, die nur unter Spionen bekannt ist.");

                        break;

                    case "Waffentest":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held richtet +2 Schaden an Zielen an, dessen Spezies er schon im Labor analysiert hat.");
                        break;

                    case "Psychoanalytisch":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Das Einschätzen Anderer ist um 1 Würfel erleichter.");
                        break;

                    case "Verrücktes Genie":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der AW von Verstand ist um 2 erhöht wann immer er unter der Hälfte seiner GG besitzt.");
                        break;

                    case "Materialverständnis":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann einmal pro Herstellung einen Wert eines Materials um 1 erhöhen.");

                        break;

                    case "Anatomie":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann Verstand WN-Proben benutzen um eine körperliche Schwachstelle bei seinem Feind zu erkennen.");
                        break;

                    case "Therapeut":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann Zielen mit Geist-EM Proben 1W6 GG pro Tag wiederherstellen.");
                        break;

                    case "Notarzt":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Sobald ein Verbündeter unter 10 LP besitzt, darf der Held die Zugreihenfolge ignorieren und eine Aktion machen.");
                        break;

                    case "Ärztlicher Eid":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held regeneriert 2W6 GG, wenn er einen verwundeten Feind versorgt.");

                        break;

                    case "MacVigor":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann eine Waffe mit 0 SS in eine Bombe umwandeln, die nächste Runde 3W6 Schaden in einem kleinen Radius anrichtet.");
                        break;

                    case "Computersprache":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann Geist-Proben für die Sabotage von mechanischen Lebewesen verwenden.");
                        break;

                    case "Verstecktes Potenzial":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Kritische Erfolge bei der Herstellung von Modulchips des Helden erhöhen, bei Bedarf die Modulstufe um 1.");
                        break;

                    case "Ausschlachtung":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann mit einer IN-Technik-Probe zu Beginn des Kampfs ermitteln, was für Waffen ein gegnerisches Raumschiff besitzt.");

                        break;

                    case "Energiesparmodus":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Die erste Fähigkeit des Tages kostet 1 VE weniger.");
                        break;

                    case "Hoffnungsfunke":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held hat um 2 erhöhte AW abhängig von der Vigoritart seines Moduls, wenn er keine VE hat.\nGrün, Blau: Verstand\nGelb, Rot: Geist");
                        break;

                    case "Kristallgespür":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann durch Technik-WN-Proben erkennen, dass Maschinen Vigorit verwenden und kann bestimmen um welche Art es sich handelt.");
                        break;

                    case "Notumleitung":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held kann mit einer IN-Technik-Probe kostenlos das Raumschiff-Schild für die Runde verdoppeln. Dafür darf das Schiff sich diese Runde nicht bewegen.");

                        break;

                    case "Scharfe Worte":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Das Verspotten von Gegnern kann ihre  Angriffe um 1 Würfel erschweren.");
                        break;

                    case "Erster Eindruck":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Die kritische Trefferchance bei Geist-Proben im ersten Gespräch mit Personen ist um 2 erhöht.");
                        break;

                    case "Gehobene Gesellschaft":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held erhält +2W4 XP für die Erfüllung von Aufträgen aus höheren sozialen Schichten.");
                        break;

                    case "Politischer Einfluss":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Held hat einflussreiche Freunde in der Regierung, die ihm einen Gefallen schulden.");

                        break;

                    case "Vom Glück gesegnet":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Stresswürfe dürfen in Kampfsituationen  auch kritische Misserfolge ersetzen.");
                        break;

                    case "Vertrauensperson":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der AW von Geist-Proben ist um 3 erhöht, wenn der Held mit Personen aus unteren Schichten interagiert.");
                        break;

                    case "Inspirierende Präsenz":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Vom Helden rekrutierte Crewmitglieder starten einmalig mit 1W4 Moral mehr.");
                        break;

                    case "Glaubenssprung":

                        FindObjectOfType<CharacterSheet>().professionsInfo.Add("Der Mitspieler kann seine eigene GG verwenden, um Spielern Stresswürfe zu ermöglichen.");

                        break;
                }
            }
        }

    }

}

