﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Charakter : MonoBehaviour
{
    public Text aussehen;
    public Text hintergrundgeschichte;
    public Text notizen;
    public Text charName;

    public Text cz1;
    public Text cz2;
    public Text cz3;
    public Text cz4;
    public Text cz5;
    public Text cz6;
    public Text cz7;
    public Text cz8;
    public Text cz9;
    public Text cz10;
    public Text cz11;
    public Text cz12;
    public Text cz13;
    public Text cz14;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FindObjectOfType<CharacterSheet>().charName = charName.text;
        FindObjectOfType<CharacterSheet>().charakterAussehen = aussehen.text;
        FindObjectOfType<CharacterSheet>().charakterHintergrundgeschichte = hintergrundgeschichte.text;
       FindObjectOfType<CharacterSheet>().charakterNotizen = notizen.text;

      
          FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz1.text, 0);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz2.text, 1);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz3.text, 2);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz4.text, 3);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz5.text, 4);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz6.text, 5);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz7.text, 6);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz8.text, 7);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz9.text, 8);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz10.text, 9);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz11.text, 10);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz12.text, 11);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz13.text, 12);
            FindObjectOfType<CharacterSheet>().charakterZug.SetValue(cz14.text, 13);
            
        
    }
}
