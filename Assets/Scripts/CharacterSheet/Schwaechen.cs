﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Schwaechen : MonoBehaviour
{
    public Text gesamtPunkte;
    public Text auswahl;

    public int punkte;
    public string wahl;

    public int toggleCountCHAR;
    public int toggleCountKOER;
    public int toggleCountETC;

    public Toggle[] togsChar;
    public Toggle[] togsKoer;
    public Toggle[] togsEtc;

    public string[] auswahlA;

    public Button firstEnabled;

    //Start is called before the first frame update
    void Awake()
    {
        togsChar = new Toggle[19];
        togsChar = GameObject.Find("CharakterlicheAuswahl").GetComponentsInChildren<Toggle>();

        for (int i = 0; i < togsChar.Length; i++)
        {
            int j = i;
            togsChar[j].onValueChanged.AddListener(delegate
        {
            CharToggleValueChanged(togsChar[j]);

        });

            togsChar[j].onValueChanged.AddListener(delegate
            {
                CalculationOnValueChange(togsChar[j]);

            });
        }

        togsKoer = new Toggle[16];
        togsKoer = GameObject.Find("KörperlicheAuswahl").GetComponentsInChildren<Toggle>();

        for (int i = 0; i < togsKoer.Length; i++)
        {
            int j = i;
            togsKoer[j].onValueChanged.AddListener(delegate
            {
                KoerToggleValueChanged(togsKoer[j]);

            });

            togsKoer[j].onValueChanged.AddListener(delegate
            {
                CalculationOnValueChange(togsKoer[j]);

            });
        }

        togsEtc = new Toggle[11];
        togsEtc = GameObject.Find("SonstigeAuswahl").GetComponentsInChildren<Toggle>();

        for (int i = 0; i < togsEtc.Length; i++)
        {
            int j = i;
            togsEtc[j].onValueChanged.AddListener(delegate
            {
                EtcToggleValueChanged(togsEtc[j]);

            });

            togsEtc[j].onValueChanged.AddListener(delegate
            {
                CalculationOnValueChange(togsEtc[j]);

            });
        }

        GameObject.Find("CharakterlicheAuswahl").SetActive(false);
        GameObject.Find("KörperlicheAuswahl").SetActive(false);
        GameObject.Find("SonstigeAuswahl").SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        CheckToggleCount(togsChar, toggleCountCHAR);
        CheckToggleCount(togsKoer, toggleCountKOER);
        CheckToggleCount(togsEtc, toggleCountETC);

    }

    void OnGUI()
    {
        gesamtPunkte.text = punkte.ToString();

        auswahl.text = wahl;
    }

    void CheckToggleCount(Toggle[] togs, int toggleCount)
    {
        if (toggleCount >= 2)
        {
            for (int i = 0; i < togs.Length; i++)
            {
                if (!togs[i].isOn)
                {
                    togs[i].interactable = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < togs.Length; i++)
            {
                if (!togs[i].isOn)
                {
                    togs[i].interactable = true;
                }
            }
        }
    }

    void CharToggleValueChanged(Toggle change)
    {
        string[] selec = null;
        string selection = change.GetComponentInChildren<Text>().text;
        selec = selection.Split(':');
        selec[0] = selec[0].Remove(0, 6);

        selec[1] = selec[1].Remove(0, 8);
        string[] info = null;
        info = selec[1].Split('<');



        if (change.isOn)
        {
            toggleCountCHAR++;
            FindObjectOfType<CharacterSheet>().schwaechen.Add(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Add(info[0]);
        }
        if (!change.isOn)
        {
            toggleCountCHAR--;
            FindObjectOfType<CharacterSheet>().schwaechen.Remove(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Remove(info[0]);
        }
    }

    void KoerToggleValueChanged(Toggle change)
    {
        string[] selec = null;
        string selection = change.GetComponentInChildren<Text>().text;
        selec = selection.Split(':');
        selec[0] = selec[0].Remove(0, 6);

        selec[1] = selec[1].Remove(0, 8);
        string[] info = null;
        info = selec[1].Split('<');

        if (change.isOn)
        {
            toggleCountKOER++;
            FindObjectOfType<CharacterSheet>().schwaechen.Add(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Add(info[0]);
        }
        if (!change.isOn)
        {
            toggleCountKOER--;
            FindObjectOfType<CharacterSheet>().schwaechen.Remove(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Remove(info[0]);
        }
    }

    void EtcToggleValueChanged(Toggle change)
    {
        string[] selec = null;
        string selection = change.GetComponentInChildren<Text>().text;
        selec = selection.Split(':');
        selec[0] = selec[0].Remove(0, 6);

        selec[1] = selec[1].Remove(0, 8);
        string[] info = null;
        info = selec[1].Split('<');

        if (change.isOn)
        {
            toggleCountETC++;
            FindObjectOfType<CharacterSheet>().schwaechen.Add(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Add(info[0]);
        }
        if (!change.isOn)
        {
            toggleCountETC--;
            FindObjectOfType<CharacterSheet>().schwaechen.Remove(selec[0]);
            FindObjectOfType<CharacterSheet>().schwaechenInfo.Remove(info[0]);
        }
    }

    void CalculationOnValueChange(Toggle change)
    {
        string[] selec = null;
        string selection = change.GetComponentInChildren<Text>().text;
        selec = selection.Split(':');
        selec[0] = selec[0].Remove(0, 6);


        if (change.isOn)
        {

            wahl = wahl + " " + selec[0] + ",";

            switch (selec[0])
            {
                case "Gefräßig":
                case "Lüstern":
                case "Jähzornig":
                case "Stolz":
                case "Neidisch":
                case "Gierig":
                case "Faul":
                case "Naiv":
                case "Fanatisch":
                case "Neugierig":
                case "Risikofreudig":
                case "Nachtragend":
                case "Ehrenhaft":
                case "Reinlichkeit":
                case "Verunstaltet":
                case "Hitzeschock":
                case "Kälteschock":
                case "Kleinwüchsig":
                case "Riesig":
                case "Leichtgewicht":
                case "Von Tieren gehasst":
                case "Wiedererkennungswert":
                case "Besondere Nahrung":
                case "Kultist":

                    punkte = punkte + 1;
                    break;

                case "Geistige Instabilität":
                case "Angst vor __":
                case "Rassistisch":
                case "Chronisch Ehrlich":
                case "Sucht":
                case "Immunschwäche":
                case "Amnesie":
                case "Insomnia":
                case "Raumschiffkrank":
                case "Sinnestaubheit":
                case "Gesucht":
                case "Umweltschützer":
                case "Analphabet":
                case "Feind":

                    punkte = punkte + 2;
                    break;

                case "Medizinunverträglichkeit":
                case "Schmerzempfindlich":
                case "Abwehrsystem":
                    punkte = punkte + 3;
                    break;

                case "Tollpatschig":
                    punkte = punkte + 4;
                    break;

                case "Bionische Gliedmaße":
                    FindObjectOfType<CharacterSheet>().sl -= 2; //Schnelligkeit minus 2
                    punkte = punkte + 2;
                    break;

                case "Bionisches Auge":
                    //WN Probe = ein zusätzlicher Würfel
                    punkte = punkte + 2;
                    break;

                case "Verschuldet":
                    punkte = punkte + 1;
                    //minus leonen Schulden
                    break;

                case "Isoliert":
                    punkte = punkte + 2;
                    //Geist-Proben um 1 Würfel erschwert
                    break;
            }
        }

        if (!change.isOn)
        {

            wahl = wahl.Replace(" " + selec[0] + ",", "");

            switch (selec[0])
            {
                case "Gefräßig":
                case "Lüstern":
                case "Jähzornig":
                case "Stolz":
                case "Neidisch":
                case "Gierig":
                case "Faul":
                case "Naiv":
                case "Fanatisch":
                case "Neugierig":
                case "Risikofreudig":
                case "Nachtragend":
                case "Ehrenhaft":
                case "Reinlichkeit":
                case "Verunstaltet":
                case "Hitzeschock":
                case "Kälteschock":
                case "Kleinwüchsig":
                case "Riesig":
                case "Leichtgewicht":
                case "Von Tieren gehasst":
                case "Wiedererkennungswert":
                case "Besondere Nahrung":
                case "Kultist":

                    punkte = punkte - 1;
                    break;

                case "Geistige Instabilität":
                case "Angst vor __":
                case "Rassistisch":
                case "Chronisch Ehrlich":
                case "Sucht":
                case "Immunschwäche":
                case "Amnesie":
                case "Insomnia":
                case "Raumschiffkrank":
                case "Sinnestaubheit":
                case "Gesucht":
                case "Umweltschützer":
                case "Analphabet":
                case "Feind":

                    punkte = punkte - 2;
                    break;

                case "Medizinunverträglichkeit":
                case "Schmerzempfindlich":
                case "Abwehrsystem":
                    punkte = punkte - 3;
                    break;

                case "Tollpatschig":
                    punkte = punkte - 4;
                    break;

                case "Bionische Gliedmaße":
                    FindObjectOfType<CharacterSheet>().sl += 2; //Schnelligkeit minus 2
                    punkte = punkte - 2;
                    break;

                case "Bionisches Auge":
                    //WN Probe = ein zusätzlicher Würfel
                    punkte = punkte - 2;
                    break;

                case "Verschuldet":
                    punkte = punkte - 1;
                    //minus leonen Schulden
                    break;

                case "Isoliert":
                    punkte = punkte - 2;
                    //Geist-Proben um 1 Würfel erschwert
                    break;
            }
        }
    }
}