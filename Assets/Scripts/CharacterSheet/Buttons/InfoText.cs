﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoText : MonoBehaviour
{

    public Dropdown peopleDropdown;
    public Dropdown weakness;
    //public Dropdown weakness2;
    //public Dropdown weakness3;
    //public Dropdown weakness4;
    //  public Dropdown volkstrength;
    // public Dropdown techstylestrength;
    // public Dropdown profStrength;
    //  public Dropdown profStrength;
    public Dropdown technikArt;
    public Dropdown.OptionData[] weaknessArray;
    public Text infoTextpl1;
    public Text infoTextpl3;
    // public Text infoTextpl4;
    //public Text infoTextpl5;
    public string[,] toolTips;


    // Use this for initialization
    void Start()
    {
        toolTips = new string[108, 2];

        //set up array reference rows
        //Völker
        toolTips[0, 0] = "Erstmensch";
        toolTips[0, 1] = "„Wir sind durch die Hölle gegangen und haben sie überlebt. Was da draußen soll uns noch Angst machen?“\n–Commander Brightley\n\nDie Erstmenschen sind die Bewohner der Scherben von Nede. Mit dem Großen Bruch zerbrach nicht nur ihr Planet, sondern auch ihre Gesellschaft in Scherben. In dieser postapokalyptischen Gesellschaft bildeten sich viele gesetzlose Gegenden, die noch heute von kleineren unabhängigen Fraktionen geführt werden.\nNede war nur noch ein Schatten seiner selbst, doch Alternativen gab es für Viele nicht. Mesos nam nur wenige Flüchtlinge auf, da zwischen Mesos und Nede zuvor noch Krieg herrschte und Omeg war für Viele eine noch unwirtlichere Alternative, als das zerstörte Nede, also blieben sie auf den Überresten von Nede.\nHeute ist der Planet von Gesetzlosen und Scharlatanen geplagt, doch viele der Erstmenschen erkannten, dass sie sich zusammenschließen müssen, um zu überleben.\nDas Leben auf einem zerstörten Planeten ist hart, doch nicht so hart, wie die Menschen, die ihn bevölkern.\n";

        toolTips[1, 0] = "Omegon";
        toolTips[1, 1] = "„Anpassungsfähigkeit war schon immer ein großer Faktor des Überlebenskampfs. In gewisser Weise sind wir dadurch menschlicher, als alle anderen Völker vor uns.“\n– Neran\n\nDer Planet Omeg: Schwefelige Gase füllen die Luft, saurer Regen fällt vom Himmel und eine Seuche drohte einst fast alle Bewohner des Planeten auszulöschen. Warum sollte sich dann also ein Mensch der noch bei Verstand ist hier ansiedeln?\nViele würden argumentieren, dass die Omegon weder bei Verstand noch wirkliche Menschen sind. Um genetisches Material von früherem Leben auf dem Planeten zu erforschen, siedelten sich Wissenschaftler auf dem unwirtlichen Planeten an und führten Experimente durch. Diese Experimente sorgten allerdings dafür, dass eine Seuche die Anwohner befiel, dessen Immunsystem für die fremden Viren nicht gerüstet war. Die Wissenschaftler von Omeg erkannten jedoch, dass ein beheimatetes Immunsystem ihm standhalten könnte. Mit Hilfe von Gensplycing veränderten sie ihre Genetik soweit, dass sie dem Virus trotzen konnten und verteilten ihr Antiserum an die weiteren Überlebenden.\nDas Vermischen der Gene führte allerdings nicht nur zu einer Immunität gegen das Virus oder eine bessere Anpassung an die Umweltbedingungen des Planeten, sondern veränderte sie auch körperlich und diese Änderungen wurden erblich weitergegeben.\nEinige Bewohner Omegs versuchten vergebens ihre menschliche Form zurückzuerhalten, doch Viele akzeptierten ihr Dasein als eine neue Art Mensch, wie sie die Welt zuvor noch nicht kannte: Die Omegon.\n";

        toolTips[2, 0] = "Mesossi";
        toolTips[2, 1] = "„Wohlstand, Kultur und Exzellenz. All dies ist Mesos… zumindest für diejenigen, die danach streben.“\n – Alexis Dajing\n\nDie Kolonisierung von Mesos war ein voller Erfolg für die Menschheit. Nie verlief eine Ansiedlung eines anderen Planeten so reibungslos und nie verlief sie auch nur ansatzweise so ertragreich. Die Entwicklung von Mesos ist geprägt von einem Wirtschaftswunder nach dem Anderen und schon bald entstand auf dem Planeten eine neue Hochkultur. Trotz ihrer boomenden Wirtschaft erfuhr die Bevölkerung von Mesos nicht den Wohlstand, den sie verdient hätte. Der Grund dahinter war die Versteuerung von seinen Investoren auf Nede. Dies sorgte schon bald für Tumult in der mesossi Gesellschaft und es entbrannte ein Unabhängigkeitskrieg und damit der erste interplanetere Krieg der Menschheitsgeschichte.\nWer diesen Krieg schlussendlich gewonnen hätte, kann man nur spekulieren, da er durch den Großen Bruch ein für alle Mal beendet wurde.\nMesos fand sich nicht nur in einer unabhängigen Position doch in der Führungsposition der Menschheit wieder, da es nun die stabilste und mächtigste Zivilisation darstellte.\nSeit dem Bruch hat sich die mesossi Kultur zu einer starken Klassenspaltung entwickelt, in der einige Individuen unfassbare Reichtümer angesammelt haben, während andere ihr Leben lang von einen Monat auf den Nächsten arbeiten.\nEins ist jedoch klar, die Mesossi sind die letzte Säule auf der sich die alte Lebensweise der Menschheit stützt.\n";

        //Technikart
        toolTips[3, 0] = "Robotik";
        toolTips[3, 1] = "Der Held nutzt Roboter, um seine Fähigkeiten zu wirken. Zusammen mit seinem robotischen Begleiter ist es ihm möglich mit einer Vielzahl an verschiedenen Robotern eine mächtige Armee aus Dienern zu erstellen, oder Gadgets für sich und seine Verbündete zu erschaffen. \nAnders, als andere Technikarten, kann die Robotik genutzt werden um mehrere Einheiten außerhalb des Spielers steuern.\n";

        toolTips[4, 0] = "Psychokinese";
        toolTips[4, 1] = "Der Held nutzt die Kraft seiner Gedanken, um seine Fähigkeiten zu wirken. Mit Hilfe von neuronalen Verstärkungen ist es ihm möglich Halluzinationen zu erschaffen, die Psyche Anderer anzugreifen und seine Umwelt mit der Kraft seines Willens allein zu beeinflussen.\nAnders, als andere Technikarten, kann die Psychokinese auch genutzt werden ohne permanent Vigorit zu verbrauchen.\n";

        toolTips[5, 0] = "Biochemie";
        toolTips[5, 1] = "Der Held nutzt biologische und chemische Mixturen, um seine Fähigkeiten zu wirken. Diese wirken meist über einen längeren Zeitraum und zeigen ihre Wirkungen nicht immer sofort nach ihrem Einsatz. Gefährliche Viren, Geflechte und Säuren bilden den Hauptteil des biochemischen Arsenals. Anders, als andere Technikarten ist es der Biochemie jedoch auch möglich Verbündete zu heilen.";

        toolTips[6, 0] = "Strahlung";
        toolTips[6, 1] = "Der Held nutzt Strahlungsenergie, um seine Fähigkeiten zu wirken. Vertreter dieser Technikart entfesseln die rohe Macht des Vigorits, um mächtige Energiestraheln, Schilde oder Explosionen zu erzeugen. Das rohe Energiepotenzial der Technikart darf auf keinen Fall unterschätzt werden.\nAnders, als andere Technikarten benötigen viele der Fähigkeiten präzise Vorbereitung, bevor ihre Kraft dann in einem Schwall freigelassen wird.\n";

        //Stil
        toolTips[7, 0] = "Kontrolle";
        toolTips[8, 0] = "Zerstörung";
        toolTips[9, 0] = "Risiko";
        toolTips[10, 0] = "Unterstützung";
        //Profession
        toolTips[11, 0] = "Mechanik";
        toolTips[12, 0] = "Wissenschaft";
        toolTips[13, 0] = "Militär";
        toolTips[14, 0] = "Diplomatie";
        toolTips[15, 0] = "Spionage";
        toolTips[16, 0] = "Vigorismus";
        //Schwächen 51
        weaknessArray = weakness.options.ToArray();
        for (int i = 0; i <= 50; i++)
        {
            toolTips[17 + i, 0] = weaknessArray[i].text;
            Debug.Log(i.ToString() + " -- " + toolTips.GetValue(17 + i, 0));
        }
        //Schwächen Text
        toolTips[17, 1] = "Der Held ist durchgängig hungrig und benötigt Nahrung bei jeder Landung\n";
        toolTips[18, 1] = "Der Held hat erhöhtes sexuelles Begehren\n";
        toolTips[19, 1] = "Der Held verfällt schnell in Rage und ist schwer zu beruhigen\n";
        toolTips[20, 1] = "Der Held ist arrogant, was dazu führt, dass er sich überschätzt, einige Aufgaben verweigert und Fehlentscheidungen schlechter einsieht.\n";
        toolTips[21, 1] = "Der Held hat Schwierigkeiten mit dem Erfolg Anderer und möchte nicht überschattet werden.\n";
        toolTips[22, 1] = "Der Held lässt sich leicht von Belohnungen ködern und es fällt ihm schwer materielles Gut und Schätze zu ignorieren\n";
        toolTips[23, 1] = "Der Held ist schwer zu motivieren und setzt immer mal wieder Aktionen aus, sofern es nicht zu brenzlig wird.\n";
        toolTips[24, 1] = "Der Held verliert 2 GG mehr, wann immer er GG verliert.\n";
        toolTips[25, 1] = "Der Held ist abergläubisch und sieht hintergründige Motive Anderer nur selten.\n";
        toolTips[26, 1] = "Der Held hat eine (leichte) bestimmte Angst (mit dem GM absprechbar)\n";
        toolTips[27, 1] = "Der Held hat eine bestimmte Angst (mit dem GM absprechbar)\n";
        toolTips[28, 1] = "Der Held hat eine (starke) bestimmte Angst (mit dem GM absprechbar)\n";
        toolTips[29, 1] = "Der Held ist komplett vernarrt in eine Ideologie oder Religion\n";
        toolTips[30, 1] = "Der Held ist gegenüber Vertretern außerhalb seines Volks missmutig und stellt dies offen dar\n";
        toolTips[31, 1] = "Der Held ist unachtsam gegenüber Gefahren, wenn er im Gegenzug etwas herausfinden kann\n";
        toolTips[32, 1] = "Der Held hat große Probleme damit zu lügen\n";
        toolTips[33, 1] = "Der Held liebt die Gefahr\n";
        toolTips[34, 1] = "Dem Helden fällt es sehr schwer zu vergeben\n";
        toolTips[35, 1] = "Der Held hat eine (leichte) Abhängigkeit\n";
        toolTips[36, 1] = "Der Held hat eine Abhängigkeit\n";
        toolTips[37, 1] = "Der Held hat eine (starke) Abhängigkeit\n";
        toolTips[38, 1] = "Der Held hält sich an einen Ehrenkodex\n";
        toolTips[39, 1] = "Der Held legt großen Wert auf Hygiene und verliert GG bei besonders unhygienischen Aktionen.\n";
        toolTips[40, 1] = "";
        toolTips[41, 1] = "";
        toolTips[42, 1] = "";
        toolTips[43, 1] = "";
        toolTips[44, 1] = "";
        toolTips[45, 1] = "";
        toolTips[46, 1] = "";
        toolTips[47, 1] = "";
        toolTips[48, 1] = "";
        toolTips[49, 1] = "";
        toolTips[50, 1] = "";
        toolTips[51, 1] = "";
        toolTips[52, 1] = "";
        toolTips[53, 1] = "";
        toolTips[54, 1] = "";
        toolTips[55, 1] = "";
        toolTips[56, 1] = "";
        toolTips[57, 1] = "";
        toolTips[58, 1] = "";
        toolTips[59, 1] = "";
        toolTips[60, 1] = "";
        toolTips[61, 1] = "";
        toolTips[62, 1] = "";
        toolTips[63, 1] = "";
        toolTips[64, 1] = "";
        toolTips[65, 1] = "";
        toolTips[66, 1] = "";
        toolTips[67, 1] = "";

        //Stärken 53
        toolTips[68, 0] = "Hartnäckig";
        toolTips[68, 1] = "Der Held kann nicht durch Schaden allein in Ohnmacht fallen.\n";

        toolTips[69, 0] = "Anführernatur";
        toolTips[69, 1] = "Crewmitglieder, die vom Helden rekrutiert werden, starten mit 1 Moral mehr. (Dies ist nicht mehrfach möglich)\n";

        toolTips[70, 0] = "Splyce of life";
        toolTips[70, 1] = "Der Held regeneriert sofort 1W4 Leben bei dem ersten Schaden erhält. Dies geht nur einmal am Tag.\n";

        toolTips[71, 0] = "Schneller als sein Schatten";
        toolTips[71, 1] = "Der Held hat 2 Schnelligkeitspunkte mehr, als er sonst durch seine Werte hätte.\n";

        toolTips[72, 0] = "Kraft von Mesos";
        toolTips[72, 1] = "Der Held hat 3 Lebenspunkte mehr, als er sonst durch seine Werte hätte.\n";

        toolTips[73, 0] = "Genetisches Design";
        toolTips[73, 1] = "Der Held darf einen seiner WW auf 15, statt 14 erhöhen, wenn er dafür mindestens einen WW auf 5 hat.\n";

        toolTips[74, 0] = "Never tell me the odds";
        toolTips[74, 1] = "Der Held darf einen AW-Punkt mehr verteilen.\n";

        toolTips[75, 0] = "Bürokrat";
        toolTips[75, 1] = "Verwaltungs-Proben von Crewmitgliedern sind um 1 Würfel erleichtert, wenn der Held sie in Auftrag gibt.\n";

        toolTips[76, 0] = "Übermenschlicher Instikt";
        toolTips[76, 1] = "Der Held muss bei Sinnesschärfe-Proben einen Würfel weniger verwenden.\n";

        toolTips[77, 0] = "Potenzial";
        toolTips[77, 1] = "";



        //set up array with text

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < toolTips.GetLength(0); i++)
        {
            if (peopleDropdown.captionText.text == toolTips[i, 0])
            {
                infoTextpl1.text = toolTips[i, 1];
            }

            if (technikArt.captionText.text == toolTips[i, 0])
            {
                infoTextpl3.text = toolTips[i, 1];
            }

            //if (weakness.captionText.text == toolTips[i, 0])
            //{
            //    infoTextpl4.text = toolTips[i, 1];
            //}

            //if (volkstrength.captionText.text == toolTips[i, 0])
            //{
            //    infoTextpl5.text = toolTips[i, 1];
            //}
        }
    }
}