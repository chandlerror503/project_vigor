﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DoneButton : MonoBehaviour
{
    public GameObject pathMessage;
    public Text messageTxt;
    public Object saveSheet;

    public void OnClick()
    {
        FindObjectOfType<GameManager>().saveID += 1;
        //charactercount +1 when button clicked (substrakt when char delete)
        //show info output path
        pathMessage.SetActive(true);
        messageTxt.text = "Charakterblatt.txt wurde gespeichert. \n\n Path: " + Application.dataPath + "/CharacterSheet.txt";
        FindObjectOfType<CharacterSheet>().Output();

        //Save Data
        //Find CharacterSheet
        //FindObjectOfType<CharacterSheet>().AddSheet(FindObjectOfType<CharacterSheet>());

        //Instantiate(saveSheet,transform.position,Quaternion.identity);

        //neues CharacterSheetSave
        //Daten Übertragen
        //CharSave ID
        PlayerPrefs.SetInt(FindObjectOfType<CharacterSheet>().charName, FindObjectOfType<GameManager>().saveID);

        PlayerPrefs.SetString(("charName_" + FindObjectOfType<GameManager>().saveID) , FindObjectOfType<CharacterSheet>().charName);
        PlayerPrefs.SetString(("volk_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().volk);
        PlayerPrefs.SetString(("volkStaerke_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().volkStaerke);
        PlayerPrefs.SetString(("volkStaerkeInfo_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().volkStaerkeInfo);
        PlayerPrefs.SetString(("modul_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().modul);
        PlayerPrefs.SetString(("stil_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stil);
        PlayerPrefs.Save();

        PlayerPrefs.SetString(("basisf1_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf1);
        PlayerPrefs.SetString(("basisf1Info_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf1Info);
        PlayerPrefs.SetString(("basisf2_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf2);
        PlayerPrefs.SetString(("basisf2Info_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf2Info);
        PlayerPrefs.SetString(("basisf3_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf3);
        PlayerPrefs.SetString(("basisf3Info_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisf3Info);
        PlayerPrefs.SetString(("basisStaerke_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisStaerke);
        PlayerPrefs.SetString(("basisStaerkeInfo_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().basisStaerkeInfo);
        PlayerPrefs.SetString(("stilf1_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilf1);
        PlayerPrefs.SetString(("stilf1Info_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilf1Info);
        PlayerPrefs.SetString(("stilf2_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilf2);
        PlayerPrefs.SetString(("stilf2Info_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilf2Info);
        PlayerPrefs.SetString(("stilStaerke_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilStaerke);
        PlayerPrefs.SetString(("stilStaerkeInfo_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().stilStaerkeInfo);
        PlayerPrefs.Save();
                
        PlayerPrefs.SetString(("profession_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().profession);
        for(int i =0; i< FindObjectOfType<CharacterSheet>().professionsStaerken.Count; i++)
        {
            PlayerPrefs.SetString(("professionsStaerke" + i + "_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().professionsStaerken[i]);
        }
        for (int i = 0; i < FindObjectOfType<CharacterSheet>().professionsInfo.Count; i++)
        {
            PlayerPrefs.SetString(("professionsInfo" + i + "_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().professionsInfo[i]);
        }
        PlayerPrefs.SetString(("ausstattungTyp_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().ausstattungTyp);
        PlayerPrefs.SetString(("schild_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().schild);
        PlayerPrefs.Save();

        PlayerPrefs.SetString(("charakterAussehen_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().charakterAussehen);
        PlayerPrefs.SetString(("charakterHintergrundgeschichte_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().charakterHintergrundgeschichte);
        PlayerPrefs.SetString(("charakterNotizen_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().charakterNotizen);
        for(int i =0; i< FindObjectOfType<CharacterSheet>().charakterZug.Length; i++)
        {
            PlayerPrefs.SetString(("charakterZug" + i + "_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().charakterZug[i]);
        }

        PlayerPrefs.SetInt(("level_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().level);
        PlayerPrefs.Save();

        PlayerPrefs.SetInt(("kk_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().kk);
        PlayerPrefs.SetInt(("ge_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().ge);
        PlayerPrefs.SetInt(("wn_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().wn);
        PlayerPrefs.SetInt(("em_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().em);
        PlayerPrefs.SetInt(("in_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().in_);
        PlayerPrefs.SetInt(("wk_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().wk);
        PlayerPrefs.Save();

        PlayerPrefs.SetInt(("k_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().k);
        PlayerPrefs.SetInt(("g_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().g);
        PlayerPrefs.SetInt(("v_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().v);
        PlayerPrefs.SetInt(("t_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().t);
        PlayerPrefs.SetInt(("f_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().f);
        PlayerPrefs.Save();

        PlayerPrefs.SetInt(("lp_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().lp);
        PlayerPrefs.SetInt(("gg_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().gg);
        PlayerPrefs.SetInt(("sl_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().sl);
        PlayerPrefs.SetInt(("ve_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().ve);

        for (int i = 0; i < FindObjectOfType<CharacterSheet>().schwaechen.Count; i++)
        {
            PlayerPrefs.SetString(("schwaechen" + i + "_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().schwaechen[i]);
        }

        for (int i = 0; i < FindObjectOfType<CharacterSheet>().schwaechenInfo.Count; i++)
        {
            PlayerPrefs.SetString(("schwaechenInfo" + i + "_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().schwaechenInfo[i]);
        }

        PlayerPrefs.SetInt(("xp_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().xp);
        PlayerPrefs.SetInt(("hp_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().hp);

        PlayerPrefs.SetInt(("leonen_" + FindObjectOfType<GameManager>().saveID), FindObjectOfType<CharacterSheet>().leonen);
        //inventar


        PlayerPrefs.SetInt("saveID", FindObjectOfType<GameManager>().saveID);
        PlayerPrefs.Save();


        //save playerprefs alle einzelnen daten aus CharakterSheet
        //dann beim laden neues charaktersheet und daten überschreiben
        //gameManager raus nur über playerprefs
        //new character dann zurück zu home und dann laden
        SceneManager.LoadScene(0);

    }
}
