﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchwaechenButton : MonoBehaviour
{
    public GameObject charakterlich;
    public GameObject koerperlich;
    public GameObject sonstige;

    // Start is called before the first frame update
   public void OnClick()
    {
        switch (tag)
        {
            case "charButton":
                charakterlich.SetActive(true);
                koerperlich.SetActive(false);
                sonstige.SetActive(false);
                break;

            case "koerButton":
                charakterlich.SetActive(false);
                koerperlich.SetActive(true);
                sonstige.SetActive(false);
                break;

            case "etcButton":
                charakterlich.SetActive(false);
                koerperlich.SetActive(false);
                sonstige.SetActive(true);
                break;
        }
    }

    
}
