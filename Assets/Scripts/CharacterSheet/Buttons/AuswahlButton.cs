﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuswahlButton : MonoBehaviour
{

    public string selection;
    public bool gewaehlt;


    public void Start()
    {
        gewaehlt = false;
    }

    // Use this for initialization
    public void OnClick()
    {
        switch (tag)
        {
            case "Volk":
                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().volk = selection;
                break;

            case "VolkStärke":

                string[] selec = null;
                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().volkStaerkeInfo = selection;
                selec = selection.Split('\n');
                selec[0] = selec[0].Remove(0, 6);
                selec[0] = selec[0].Substring(0, selec[0].Length - 8);

                FindObjectOfType<CharacterSheet>().volkStaerke = selec[0];

                break;

            case "Modul":

                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().modul = selection;

                break;

            case "Stilstärke":

                if (!gewaehlt)
                {
                    selection = GetComponentInChildren<Text>().text;
                    FindObjectOfType<CharacterSheet>().stilStaerke = selection;
                    FindObjectOfType<CharacterSheet>().stilSPicked = true;
                    GameObject.Find("BasisStärke").GetComponent<Button>().interactable = false;
                    gewaehlt = true;
                }
                else
                {
                    GameObject.Find("BasisStärke").GetComponent<Button>().interactable = true;
                    FindObjectOfType<CharacterSheet>().stilStaerke = "";
                    gewaehlt = false;
                    FindObjectOfType<CharacterSheet>().stilSPicked = false;
                }
                break;

            case "Modulstärke":

                if (!gewaehlt)
                {

                    selection = GetComponentInChildren<Text>().text;
                    FindObjectOfType<CharacterSheet>().basisStaerke = selection;
                    GameObject.Find("StilStärke").GetComponent<Button>().interactable = false;
                    gewaehlt = true;
                    FindObjectOfType<CharacterSheet>().modulSPicked = true;
                }
                else
                {
                    GameObject.Find("StilStärke").GetComponent<Button>().interactable = true;
                    gewaehlt = false;
                    FindObjectOfType<CharacterSheet>().basisStaerke = "";
                    FindObjectOfType<CharacterSheet>().modulSPicked = false;
                }


                break;

            case "Stil":

                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().stil = selection;

                gewaehlt = false;
                GameObject.Find("StilStärke").GetComponent<Button>().interactable = true;
                GameObject.Find("BasisStärke").GetComponent<Button>().interactable = true;

                FindObjectOfType<CharacterSheet>().basisStaerke = "";
                FindObjectOfType<CharacterSheet>().basisStaerkeInfo = "";

                FindObjectOfType<CharacterSheet>().stilStaerke = "";
                FindObjectOfType<CharacterSheet>().stilStaerkeInfo = "";
                break;

            case "Profession":

                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().profession = selection;
                for (int i = 0; i < FindObjectOfType<Profession>().togs.Length; i++)
                {
                    FindObjectOfType<Profession>().togs[i].isOn = false;
                }
                break;

            case "Ausstattung":

                selection = GetComponentInChildren<Text>().text;
                FindObjectOfType<CharacterSheet>().ausstattungTyp = selection;
                break;
        }
    }
}
