﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toggles : MonoBehaviour
{

    public Toggle tog;
    // Use this for initialization
    void Start()
    {
        tog = GetComponent<Toggle>();
        tog.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(tog);

        });

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToggleValueChanged(Toggle change)
    {
        if (change.isOn)
        {
            FindObjectOfType<Profession>().toggleCount++;
            FindObjectOfType<CharacterSheet>().professionsStaerken.Add(change.GetComponentInChildren<Text>().text);
            FindObjectOfType<CharacterSheet>().AssignProfInfo();

        }
        if (!change.isOn)
        {
            FindObjectOfType<Profession>().toggleCount--;
            FindObjectOfType<CharacterSheet>().professionsStaerken.Remove(change.GetComponentInChildren<Text>().text);
            FindObjectOfType<CharacterSheet>().AssignProfInfo();

        }
    }
}
