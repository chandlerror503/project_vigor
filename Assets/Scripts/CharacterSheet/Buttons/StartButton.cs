﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class StartButton : MonoBehaviour
{

    public bool clicked;
    GameObject player;
    public GameObject volk;
    public GameObject register;

    void Start()
    {
        clicked = false;
    }

    public void OnClick()
    {
        GameObject.Find("Start").SetActive(false);
        clicked = true;

        player = new GameObject("Charakter");
        player.AddComponent<CharacterSheet>();
        volk.SetActive(true);

        register.SetActive(true);

    }
}
