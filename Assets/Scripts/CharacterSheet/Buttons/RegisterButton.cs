﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class RegisterButton : MonoBehaviour
{

    public GameObject volk;
    public GameObject module;
    public GameObject faehigkeiten;
    public GameObject professionen;
    public GameObject ausruestung;
    public GameObject attribute;
    public GameObject schwaechen;
    public GameObject charakter;

    public string buttonName;

    public Button firstEnabled;

    void Start()
    {
        buttonName = GetComponentInChildren<Text>().text;

        firstEnabled.Select();
        ExecuteEvents.Execute(firstEnabled.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

    }

    void Awake()
    {
        firstEnabled.Select();

    }

    public void OnClick()
    {
        switch (buttonName)
        {
            case "Volk":
                volk.SetActive(true);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(false);

                FindObjectOfType<ButtonVoelker>().firstEnabled.Select();
                ExecuteEvents.Execute(FindObjectOfType<ButtonVoelker>().firstEnabled.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
                break;

            case "Modifikation":
                volk.SetActive(false);
                module.SetActive(true);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(false);
                break;

            case "Fähigkeiten":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(true);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(false);
                break;

            case "Professionen":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(true);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(false);

                FindObjectOfType<Profession>().firstEnabled.Select();
                ExecuteEvents.Execute(FindObjectOfType<Profession>().firstEnabled.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

                break;

            case "Ausrüstung":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(true);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(false);
                break;

            case "Attribute":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(true);
                schwaechen.SetActive(false);
                charakter.SetActive(false);
                break;

            case "Schwächen":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(true);
                charakter.SetActive(false);

                FindObjectOfType<Schwaechen>().firstEnabled.Select();
                ExecuteEvents.Execute(FindObjectOfType<Schwaechen>().firstEnabled.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);

                break;

            case "Charakter":
                volk.SetActive(false);
                module.SetActive(false);
                faehigkeiten.SetActive(false);
                professionen.SetActive(false);
                ausruestung.SetActive(false);
                attribute.SetActive(false);
                schwaechen.SetActive(false);
                charakter.SetActive(true);
                break;

        }
    }
}
