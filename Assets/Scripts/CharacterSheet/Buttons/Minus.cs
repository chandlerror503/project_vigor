﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minus : MonoBehaviour
{
    public Text value;
    public int valueInt;

    public void OnClick()
    {
        int.TryParse(value.text, out valueInt);

        if (value.tag == "WW")
        {
            if (valueInt > 5)
            {
                valueInt--;
                value.text = valueInt.ToString();
            }
        }

        if(value.tag == "AW")
        {
            if (valueInt > 1)
            {
                valueInt--;
                value.text = valueInt.ToString();
            }
        }
    }
}
