﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfToggle : MonoBehaviour
{
    public Toggle[] togs;
    public int toggleCount;

    // Use this for initialization
    void Start()
    {
        togs = new Toggle[3];
        togs = GetComponentsInChildren<Toggle>();


        /* for (int i = 0; i <= 3; i++)
         {
             togs[i].onValueChanged.AddListener(delegate
             {
                 ToggleValueChanged(togs[i]);

             });
         }*/

    }

    // Update is called once per frame
    void Update()
    {

        if (toggleCount >= 2)
        {
            for (int j = 0; j <= 3; j++)
            {
                if (!togs[j].isOn)
                {
                    togs[j].interactable = false;
                }
            }
        }
        else
        {
            for (int j = 0; j <= 3; j++)
            {
                if (!togs[j].isOn)
                {
                    togs[j].interactable = true;
                }
            }
        }
    }

    public void ToggleValueChanged(Toggle change)
    {
        if (change.isOn)
        {
            toggleCount++;
            FindObjectOfType<CharacterSheet>().professionsStaerken.Add(change.GetComponentInChildren<Text>().text);

        }
        if (!change.isOn)
        {
            toggleCount--;
            FindObjectOfType<CharacterSheet>().professionsStaerken.Remove(change.GetComponentInChildren<Text>().text);
        }
    }
}
