﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaehigkeitenInfo : MonoBehaviour
{
    public Text buttonName;
    public Text info;

    public void OnClick()
    {
        FindObjectOfType<InfoSwitch>().SwitchBy(buttonName.text,info);
    }
}
