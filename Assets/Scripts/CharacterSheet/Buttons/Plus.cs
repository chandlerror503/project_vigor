﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plus : MonoBehaviour
{
    public Text value;
    public int valueInt;
    public Text max_AW;
    public int maxAW;
    public Text max_WW;
    public int maxWW;

    public void Update()
    {
        int.TryParse(max_AW.text, out maxAW);
        int.TryParse(max_WW.text, out maxWW);
    }
    public void OnClick()
    {
        int.TryParse(value.text, out valueInt);


        if (value.tag == "WW")
        {
            if (valueInt < 14 && maxWW > 0)
            {
                valueInt++;
                value.text = valueInt.ToString();
            }
        }

        if(value.tag == "AW")
        {
            if (valueInt < 6 && maxAW > 0)
            {
                valueInt++;
                value.text = valueInt.ToString();
            }

        }
    }
}
