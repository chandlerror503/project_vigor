﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateCharacterManager : MonoBehaviour
{
    /* public Text infoText;
     public Dropdown dropdownVolk;
     public InputField kkText;
     public InputField inText;
     public InputField geText;
     public InputField wnText;
     public InputField emText;
     public InputField wkText;
     public InputField kText;
     public InputField gText;
     public InputField vText;
     public InputField tText;
     public InputField fText;
     public Dropdown weak1;
     public Dropdown weak2;
     public Dropdown weak3;
     public Dropdown weak4;
     public Dropdown strengthErstmensch;
     public Dropdown strengthMesossi;
     public Dropdown strengthOmegon;
     public GameObject mech;
     public GameObject science;
     public GameObject mili;
     public GameObject diplo;
     public GameObject spy;
     public GameObject vigo;
     public Dropdown techstyle;
     public List<string> dropdownoptions = new List<string>();
     public bool optionsSet;


     // Use this for initialization
     void Start()
     {
         optionsSet = false;
     }

     // Update is called once per frame
     void Update()
     {
         if (GameObject.Find("KKText").GetComponent<Text>().text == "")
         {
             kkText.text = "0";

         }
         if (GameObject.Find("GEText").GetComponent<Text>().text == "")
         {
             geText.text = "0";

         }
         if (GameObject.Find("WNText").GetComponent<Text>().text == "")
         {
             wnText.text = "0";
         }
         if (GameObject.Find("EMText").GetComponent<Text>().text == "")
         {
             emText.text = "0";
         }
         if (GameObject.Find("INText").GetComponent<Text>().text == "")
         {
             inText.text = "0";
         }
         if (GameObject.Find("WKText").GetComponent<Text>().text == "")
         {
             wkText.text = "0";
         }

         //set Ausgleichswerte
         if (GameObject.Find("KText").GetComponent<Text>().text == "")
         {
             kText.text = "0";
         }
         if (GameObject.Find("GText").GetComponent<Text>().text == "")
         {
             gText.text = "0";

         }
         if (GameObject.Find("VText").GetComponent<Text>().text == "")
         {
             vText.text = "0";

         }
         if (GameObject.Find("TText").GetComponent<Text>().text == "")
         {
             tText.text = "0";

         }
         if (GameObject.Find("FText").GetComponent<Text>().text == "")
         {
             fText.text = "0";

         }

         //Volksstärke
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().volk == "Erstmensch")
         {
             strengthOmegon.gameObject.SetActive(false);
             strengthMesossi.gameObject.SetActive(false);
             strengthErstmensch.gameObject.SetActive(true);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().volk == "Mesossi")
         {
             strengthOmegon.gameObject.SetActive(false);
             strengthMesossi.gameObject.SetActive(true);
             strengthErstmensch.gameObject.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().volk == "Omegon")
         {
             strengthOmegon.gameObject.SetActive(true);
             strengthMesossi.gameObject.SetActive(false);
             strengthErstmensch.gameObject.SetActive(false);
         }

         //Profession
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Mechanik")
         {
             mech.SetActive(true);
             science.SetActive(false);
             mili.SetActive(false);
             diplo.SetActive(false);
             spy.SetActive(false);
             vigo.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Wissenschaft")
         {
             mech.SetActive(false);
             science.SetActive(true);
             mili.SetActive(false);
             diplo.SetActive(false);
             spy.SetActive(false);
             vigo.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Militär")
         {
             mech.SetActive(false);
             science.SetActive(false);
             mili.SetActive(true);
             diplo.SetActive(false);
             spy.SetActive(false);
             vigo.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Diplomatie")
         {
             mech.SetActive(false);
             science.SetActive(false);
             mili.SetActive(false);
             diplo.SetActive(true);
             spy.SetActive(false);
             vigo.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Spionage")
         {
             mech.SetActive(false);
             science.SetActive(false);
             mili.SetActive(false);
             diplo.SetActive(false);
             spy.SetActive(true);
             vigo.SetActive(false);
         }
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().profession == "Vigorismus")
         {
             mech.SetActive(false);
             science.SetActive(false);
             mili.SetActive(false);
             diplo.SetActive(false);
             spy.SetActive(false);
             vigo.SetActive(true);
         }

         //Technikart + Stil

         if (!optionsSet && GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().technik != "" && GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style != "")
         {
             SetOptions();
         }

         //Textfield
     }

     public void SetOptions()
     {
         //Robotik
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().technik == "Robotik")
         {
             dropdownoptions.Add("Roboterarmee");

             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Kontrolle")
             {
                 dropdownoptions.Add("Künstliche Intelligenz");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Zerstörung")
             {
                 dropdownoptions.Add("Letzter Funke");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Risiko")
             {
                 dropdownoptions.Add("Ein-Mann-Armee");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Unterstützung")
             {
                 dropdownoptions.Add("Helfende Hand");
             }
         }
         //Biochemie
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().technik == "Biochemie")
         {
             dropdownoptions.Add("Seuchenschleuder");

             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Kontrolle")
             {
                 dropdownoptions.Add("Steigernde Symptomatik");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Zerstörung")
             {
                 dropdownoptions.Add("Ausbruch");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Risiko")
             {
                 dropdownoptions.Add("Übertragbarkeit");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Unterstützung")
             {
                 dropdownoptions.Add("Symbiose");
             }
         }
         //Psychokinese
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().technik == "Psychokinese")
         {
             dropdownoptions.Add("Geistige Beherrschung");

             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Kontrolle")
             {
                 dropdownoptions.Add("Unbrechbarer Wille");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Zerstörung")
             {
                 dropdownoptions.Add("Psychischer Schrei");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Risiko")
             {
                 dropdownoptions.Add("Dem Wahnsinn verfallen");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Unterstützung")
             {
                 dropdownoptions.Add("Neurologische Stimulierung");
             }
         }
         //Strahlung
         if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().technik == "Strahlung")
         {
             dropdownoptions.Add("Schnellschuss");

             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Kontrolle")
             {
                 dropdownoptions.Add("Blendung");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Zerstörung")
             {
                 dropdownoptions.Add("Die Strahlen kreuzen");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Risiko")
             {
                 dropdownoptions.Add("Instabile Kraft");
             }
             if (GameObject.Find("CharacterSheet").GetComponent<CharacterSheet>().style == "Unterstützung")
             {
                 dropdownoptions.Add("Starthilfe");
             }
         }


         techstyle.AddOptions(dropdownoptions);
         optionsSet = true;
     }
 */
}
