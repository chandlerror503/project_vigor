﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonVoelker : MonoBehaviour
{

    public string buttonName;
    public Text staerke1;
    public Text staerke2;
    public Text staerke3;
    public Text textBackground1;

    public Button firstEnabled;


    // Use this for initialization
    void Start()
    {
        buttonName = GetComponentInChildren<Text>().text;

    }

    public void OnClick()
    {
        switch (buttonName)
        {

            case "Scherber":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Hartnäckig</i></b>\n  Der Held kann nicht durch Schaden allein in Ohnmacht fallen.";
                staerke2.text = "<b><i>Schneller als sein Schatten</i></b>\n Der Held hat 2 SL- Punkte mehr, als er sonst durch seine Werte hätte.";
                staerke3.text = "<b><i>Never Tell Me The Odds</i></b>\n Der Held muss bei Proben den WW nicht unterwürfeln sondern darf ihn nur nicht überwürfeln.";
                textBackground1.text = "<b><i>Scherber</i></b>\nScherber sind eine humanoide Spezies, sie haben einen drahtigen Oberkörper und sehr muskulöse Beine. Typisch sind hervortretende" +
                    " Verknöcherungen an Schädel und Rücken. Der Name Scherber ist eine von den Utopianern eingeführte Fremdbezeichnung, die darauf anspielt, dass die so " +
                    "Bezeichneten auf Planeten - Bruchstücken durch die Galaxie treiben. Von den meisten Utopianern werden Scherber als unzivilisiert und primitiv wahrgenommen." +
                    "Alte Quellen der ersten Utopianer deuten jedoch darauf hin, dass Scherber die Nachkommen der ersten Hochkultur der Delta - Galaxie sind.Weitere antike " +
                    "Quellen der Subterras sprechen dafür, dass besagte Hochkultur ihren eigenen Planeten durch die Explosion eines neuartigen Energiereaktors zerstörte." +
                    "Für diese Theorie sprechen auch Zivilisationsüberreste auf manchen der Planetensplitter, aus denen die kreativen Bewohner Sonnensegel bauen um nicht nur " +
                    "Ziellos durch den Kosmos zu treiben.Diese überdurchschnittliche Kreativität und die körperliche und mentale Hartnäckigkeit sichern den Fortbestand dieser " +
                    "unfreiwilligen Nomaden unter den widrigsten Lebensumständen.";

                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;

            case "Subterras":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Strahlungsresistenz</i></b>\n Der Held nimmt 3 Schaden weniger von jeder Art von Strahlung.";
                staerke2.text = "<b><i>Kristallkontakt</i></b>\n Der Held hat 1 VE-Punkt mehr, als er sonst durch seine Werte hätte";
                staerke3.text = "<b><i>Tunnelblick</i></b>\n Der Held kann Wärme durch Wände hindurch sehen.";
                textBackground1.text = "<b><i>Kr’şs/Subterras (⍾♌︎)</i></b>\n" +
                    "Kr’şs sind eine repteloide Spezies, sie sind dementsprechend Kaltblüter. Aufgrund ihrer unterirdischen Lebensweise werden " +
                    "sie von den Utopianern Subterras genannt.Der Körper dieser Echsenmenschen ist von grünlichen Schuppen bedeckt, der sie gegen " +
                    "die hohe Strahlung ihres Planeten Kr’kstšs(⍾⏄⏇⏁♌︎) schützt. Der Schädel weist einen gebogenen Knochenkamm auf, der traditionell" +
                    " zum Graben von Tunneln und im Kampf Verwendung findet. Die schlangenartigen Augen dieser Repteloiden ermöglichen ihnen einen " +
                    "thermografischen Blick durch dünne Gesteinsschichten und Wände.Die Kr’şs, deren Sprache aus Klick-und S - Lauten besteht, sind " +
                    "eine der ältesten noch bestehenden Kulturen der Delta - Galaxie.Die ersten Raumfahrt-Versuche dieser Spezies lassen sich bis in " +
                    "das erste Zeitalter der antiken Utopianer zurückdatieren.Die Vorfahren der heutigen heutigen Bewohner Utopias verehrten diese für " +
                    "sie fremden Wesen als mythologische Gestalten oder gar Gottheiten.Die heutige Staatsdoktrin der Utopianer leugnet allerdings, dass " +
                    "die ersten Impulse zu Raumfahrt auf Besucher von Kr’kstšs zurückzuführen seien.";
                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;

            case "Utopianer":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Erholsamer Schlaf</i></b>\n Der Held darf 2W6 LP bei Schlaf regenerieren.";
                staerke2.text = "<b><i>Heile Welt</i></b>\n Der Held hat 3 GG-\nPunkte mehr, als er sonst durch seine Werte hätte.\n";
                staerke3.text = "<b><i>Schiffgeboren</i></b>\n Der Held erhält Motivation wann immer Raumschiffkämpfe anfangen.";
                textBackground1.text = "";
                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;

            case "Mesossi":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Entschlossenheit</i></b>\n Der Held erhält immer  1 GG-Schaden weniger.";
                staerke2.text = "<b><i>Kraft von Mesos</i></b>\n Der Held hat 3 Lebenspunkte mehr, als er sonst durch seine Werte hätte.";
                staerke3.text = "<b><i>Hochkultur</i></b>\n Der Held erhält 1W4 mehr Lernfortschritt pro Lernsession.";
                textBackground1.text = "<b><i>Mesossi</i></b>\n Messosi sind eine humanoide Spezies, die sich durch ihren starken Muskeltonus und ihre " +
                    "durchschnittliche Körpergröße von über 2m kennzeichnet. Sie bilden neben den Kr’şs eine der beiden ältesten noch bestehenden Hochkulturen " +
                    "der Delta-Galaxie. Ihr Heimatplanet Mesos wird von seinen Bewohnern als Göttin verehrt und gilt unter diesen als Ursprung aller Kraft." +
                    " Mesos bildete bis ins dritte Zeitalter den Mittelpunkt der Delta-Galaxie. Durch eine künstlich herbeigeführte Supernova(siehe Scherber) " +
                    "verrückte sich jedoch die Sternkonstellation. Bis zu diesem Zeitpunkt verbanden Energieströme die gesamte Galaxie und liefen auf Portalen" +
                    " in Mesos zusammen und ermöglichten den Mesossi das interstellare Reisen ohne das RIsiko der Raumfahrt. Die Ruinen dieser Energieportale" +
                    " sind bis heute Quelle der guten Lebensbedingungen auf dem Planeten. Die Mesossi-Gesellschaft ist ein Matriachat, dass vom Rat der" +
                    " Heldinnen geführt wird.Der Heldinnenkult basiert nicht auf Geburtsrecht sondern den Taten jeder Person.Die drei Grundwerte von Mesos" +
                    " sind Integrität, Loyalität und Entschlossenheit, sie machen eine Person zur Heldin und in manchen Fällen zum Helden. Neben diesen drei " +
                    "Säulen basiert die Kultur der Mesossi auf der Annahme, dass man nicht zwischen Magie und Wissenschaft trennen kann, weil alle Energien " +
                    "heilig sind.";
                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;

            case "Omegon":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Splyce Of Life</i></b>\n Der Held regeneriert sofort 1W4 Leben bei dem ersten Schaden erhält. Dies geht nur einmal am Tag.";
                staerke2.text = "<b><i>Genetisches Design</i></b>\n Der Held darf einen seiner WW auf 15, statt 14 erhöhen, wenn er dafür mindestens einen WW auf 5 hat.";
                staerke3.text = "<b><i>Unmenschlicher Instinkt</i></b>\n Der Held darf Proben im Schleichen und Fährtenlesen für Teamkollegen in der Nähe nachwürfeln, um sie zu korrigieren.";
                textBackground1.text = "";
                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;

            case "Monoloten":
                FindObjectOfType<CharacterSheet>().volkStaerke = "";
                staerke1.text = "<b><i>Zusammenhalt</i></b>\n Der Held erhält 1 Schaden aus allen Quellen weniger, wenn er gemeinsam mit seiner Gruppe kämpft.";
                staerke2.text = "<b><i>Gefasst</i></b>\n Der Held erhält +1 in seinem Kampfwert.";
                staerke3.text = "<b><i>Heilige Suche</i></b>\n Der Held erhält 100XP wann immer er einen neuen Monoloten findet.";
                textBackground1.text = "<b><i>Monoloten</i></b>\n Monoloten sind eine künstlich geschaffene Spezies. Die ersten Monoloten waren Mitglieder " +
                    "einer technokratische Sekte, die sich im dritten Zeitalter auf Mesos gründete.Nach dem die große Verrückung den Planeten aus seiner " +
                    "zentralen Position verschoben hatte, gelang es Wissenschaftlern Mesos-Energie synthetisch zu replizieren.Dies führte dazu, dass einige " +
                    "von ihnen die Göttlichkeit von Mesos und das daran gebundene heilige Matriarchat in Frage stellten.Sie stellten ein neues Dogma auf, dass" +
                    " stattdessen das Kollektiv und Wissenschaft in den Mittelpunkt setzte.Um ihre Vorstellung von einer perfekten Gesellschaft umzusetzen" +
                    " schufen sie aus dieser neugewonnen Energie ein künstliches Schwarmbewusstein.Neue Mitglieder der Sekte werden bei der Aufnahme daran" +
                    " gebunden, zudem lassen sie bei dieser „Taufe“ ihr Geschlecht mechanisch auflösen um zu einem geschlechtlosen Monoloten zu werden. Für den" +
                    " Versuch die Mesos - Kultur abzuschaffen wurden die ersten Mitglieder als Häretiker verurteilt und vom Planeten verbannt. Die ersten Monoloten" +
                    " verstreuten sich in einzelnen Sonden über die gesamte Galaxis um, auf dieser „heiligen Reise“, neues Wissen für das hive mind zu sammeln " +
                    "und weitere Mitglieder zu bekehren.";
                FindObjectOfType<CharacterSheet>().volk = buttonName;
                break;
        }

    }
}
