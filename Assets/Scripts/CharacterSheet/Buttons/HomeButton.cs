﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HomeButton : MonoBehaviour
{
    public bool sure;

    public Text warningMessage;

    public Button y;
    public Button n;

    // Start is called before the first frame update
    void Start()
    {
        warningMessage.text = "Zum Startbildschirm zurückkehren und bisherige Auswahl verwerfen?";
        warningMessage.enabled = false;
    }

    // Update is called once per frame
    void OnClick()
    {
        warningMessage.enabled = true;

    }

    void Update()
    {
        if (y.IsInvoking())
        {
            sure = true;
        }

        if (n.IsInvoking())
        {
            sure = false;
        }

        if (sure)
        {
            warningMessage.enabled = false;
            SceneManager.LoadScene(0);
        }
    }
}
