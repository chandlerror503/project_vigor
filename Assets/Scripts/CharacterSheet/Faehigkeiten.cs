﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Faehigkeiten : MonoBehaviour
{

    public Text basisf1;
    public Text basisf2;
    public Text basisf3;

    public Text stilf1;
    public Text stilf2;

    public Text basisS;
    public Text stilS;

    public int stilToggleCount;
    public int strengthToggleCount;
    public Toggle[] stilTogs;
    public Toggle[] strengthTogs;

    // Use this for initialization
    void Start()
    {
        /*  stilTogs = new Toggle[3];
          stilTogs = GameObject.FindWithTag("Stil").GetComponents<Toggle>();

          strengthTogs = new Toggle[1];
          strengthTogs = GameObject.FindWithTag("StrengthToggle").GetComponents<Toggle>(); */
    }

    // Update is called once per frame
    void Update()
    {

        switch (FindObjectOfType<CharacterSheet>().modul)
        {

            case "Robotik":

                basisf1.text = "Vervielfältigung";
                basisf2.text = "Mecha-Modus";
                basisf3.text = "Attillerieangriff";

                basisS.text = "Roboterarmee";
                //basisStärke (Stil frei) Text

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Festungsbefehl";
                        stilf2.text = "Statischer Stoß";
                        stilS.text = "Künstliche Intelligenz";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Angriffs-Modus";
                        stilf2.text = "Satellitenschlag";
                        stilS.text = "Letzter Funke";
                        break;

                    case "Risiko":
                        stilf1.text = "Mechafusion";
                        stilf2.text = "Super-Mecha-Modus";
                        stilS.text = "Mecha aktiviert!";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Schutz-Modus";
                        stilf2.text = "Schildwall";
                        stilS.text = "Schutzanzug";
                        break;
                }


                break;

            case "Gravitation":

                basisf1.text = "Kraftzug";
                basisf2.text = "Seismischer Schlag";
                basisf3.text = "Schwerelosigkeit";
                basisS.text = "Ereignishorizont";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Schwergewicht";
                        stilf2.text = "Sogzentrum";
                        stilS.text = "Bis zur Unendlichkeit";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Schockstoß";
                        stilf2.text = "Erdbeben";
                        stilS.text = "Nachbeben";
                        break;

                    case "Risiko":
                        stilf1.text = "Konterschlag";
                        stilf2.text = "Kometenhieb";
                        stilS.text = "Schwerkraft";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Beschleunigung";
                        stilf2.text = "Raumkrümmung";
                        stilS.text = "Defying Gravity";
                        break;
                }


                break;

            case "Mikrobiologie":

                basisf1.text = "Muskelschlaffheit";
                basisf2.text = "Regenerator";
                basisf3.text = "Blutplage";
                basisS.text = "Seuchenschleuder";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Schwarzmagenseuche";
                        stilf2.text = "Fieberwelle";
                        stilS.text = "Steigernde Symptomatik";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Ausbreitung";
                        stilf2.text = "Nekrotische Seuche";
                        stilS.text = "Ausbruch";
                        break;

                    case "Risiko":
                        stilf1.text = "Transfusion";
                        stilf2.text = "Viraler Schock";
                        stilS.text = "Übertragbarkeit";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Synthetischer Parasit";
                        stilf2.text = "Biotische Gaswolke";
                        stilS.text = "Symbiose";
                        break;
                }


                break;

            case "Mutation":

                basisf1.text = "Animalischer Furor";
                basisf2.text = "Mutation: Monsterkralle";
                basisf3.text = "Genverzeichnis";
                basisS.text = "Adaptabilität";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Mutation: Giftzunge";
                        stilf2.text = "Monstergebrüll";
                        stilS.text = "Furchteinflößend";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Raubtierinstinkt";
                        stilf2.text = "Mutation: Stachelregen";
                        stilS.text = "Blutrausch";
                        break;

                    case "Risiko":
                        stilf1.text = "Adrenalinpumpe";
                        stilf2.text = "Mutation: Ultimativer Jäger";
                        stilS.text = "Blut für Blut";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Mutation: Plattenpanzer";
                        stilf2.text = "Genvermittlung";
                        stilS.text = "Lebensquell";
                        break;
                }


                break;

            case "Photonik":

                basisf1.text = "Aufladung";
                basisf2.text = "Energiekonvertierung";
                basisf3.text = "Instabiler Quantenball";
                basisS.text = "Umleitung";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Blitzbetäubung";
                        stilf2.text = "Statikfeld";
                        stilS.text = "Blendung";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Detonieren";
                        stilf2.text = "Todesstrahl";
                        stilS.text = "Die Strahlen kreuzen";
                        break;

                    case "Risiko":
                        stilf1.text = "Dematerialisierungsblast";
                        stilf2.text = "Photonenroulette";
                        stilS.text = "Instabile Kraft";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Rückstoßschild";
                        stilf2.text = "Schildstrahl";
                        stilS.text = "Starthilfe";
                        break;
                }

                break;

            case "Holotech":

                basisf1.text = "Hologramm: Holo-Kopie";
                basisf2.text = "Hologramm: Augmentierung";
                basisf3.text = "Hologramm: Reflektorwand";
                basisS.text = "Intelligentes Design";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Verzerrtes Bild";
                        stilf2.text = "Reflektionsmaximum";
                        stilS.text = "Sensorisches Erlebnis";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Hologramm: Lichtwaffe";
                        stilf2.text = "Lichtspaltung";
                        stilS.text = "Durchdringung";
                        break;

                    case "Risiko":
                        stilf1.text = "Lichtschlucker";
                        stilf2.text = "Lichtkaskade";
                        stilS.text = "Schildkonverter";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Hologramm: Holographische Textur";
                        stilf2.text = "Holographische Kuppel";
                        stilS.text = "Kompatibel";
                        break;
                }


                break;

            case "Psyonik":

                basisf1.text = "Glücksstrahl";
                basisf2.text = "Gedankenschinden";
                basisf3.text = "Psychokinese";
                basisS.text = "Geistige Beherrschung";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Innere Schockwelle";
                        stilf2.text = "Geistesergriff";
                        stilS.text = "Unbrechbarer Wille";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Psychischer Schrei";
                        stilf2.text = "Neuronaler Zusammenbruch";
                        stilS.text = "Gebrochener Geist";
                        break;

                    case "Risiko":
                        stilf1.text = "Erinnerungsfresser";
                        stilf2.text = "Strahlender Wahnsinn";
                        stilS.text = "Dem Wahnsinn verfallen";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Ruhiger Geist";
                        stilf2.text = "Körper und Geist";
                        stilS.text = "Neurologische Stimulierung";
                        break;
                }


                break;

            case "Thermochemie":

                basisf1.text = "Thermische Einstellung";
                basisf2.text = "Thermischer Schock";
                basisf3.text = "Wetterumschwung";
                basisS.text = "Sturmwarnung";

                switch (FindObjectOfType<CharacterSheet>().stil)
                {

                    case "Kontrolle":
                        stilf1.text = "Verfestigung";
                        stilf2.text = "Eisige Adern/ Brodelndes Blut";
                        stilS.text = "Gefrierbrand";
                        break;

                    case "Zerstörung":
                        stilf1.text = "Froststrahl/Feuerwelle";
                        stilf2.text = "Blizzard/Sengende Luft";
                        stilS.text = "Inferno";
                        break;

                    case "Risiko":
                        stilf1.text = "Eisspeere/Feuersalve";
                        stilf2.text = "Lebende Bombe";
                        stilS.text = "Wechselwirkung";
                        break;

                    case "Unterstützung":
                        stilf1.text = "Eisige Berührung/ Feuerteufel";
                        stilf2.text = "Eisstruktur/ Kauterisierung";
                        stilS.text = "Windschatten";
                        break;
                }


                break;

        }

        SetValues();
    }

    void SetValues()
    {
        FindObjectOfType<CharacterSheet>().basisf1 = basisf1.text;
        FindObjectOfType<CharacterSheet>().basisf2 = basisf2.text;
        FindObjectOfType<CharacterSheet>().basisf3 = basisf3.text;

        FindObjectOfType<CharacterSheet>().stilf1 = stilf1.text;
        FindObjectOfType<CharacterSheet>().stilf2 = stilf2.text;
    }
}
