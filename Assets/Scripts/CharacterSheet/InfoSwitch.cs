﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoSwitch : MonoBehaviour
{
    public Text infoText;
    public VerticalLayoutGroup vlGroup;

    // Start is called before the first frame update
    public void Update()
    {
        FindObjectOfType<CharacterSheet>().basisf1Info = SwitchBy(FindObjectOfType<CharacterSheet>().basisf1);
        FindObjectOfType<CharacterSheet>().basisf2Info = SwitchBy(FindObjectOfType<CharacterSheet>().basisf2);
        FindObjectOfType<CharacterSheet>().basisf3Info = SwitchBy(FindObjectOfType<CharacterSheet>().basisf3);

        FindObjectOfType<CharacterSheet>().stilf1Info = SwitchBy(FindObjectOfType<CharacterSheet>().stilf1);
        FindObjectOfType<CharacterSheet>().stilf2Info = SwitchBy(FindObjectOfType<CharacterSheet>().stilf2);

        FindObjectOfType<CharacterSheet>().basisStaerkeInfo = SwitchBy(FindObjectOfType<CharacterSheet>().basisStaerke);
        FindObjectOfType<CharacterSheet>().stilStaerkeInfo = SwitchBy(FindObjectOfType<CharacterSheet>().stilStaerke);

        Invoke("UpdateParentLayoutGroup", 0.1f);
    }

    void UpdateParentLayoutGroup()
    {
        infoText.gameObject.SetActive(false);
        infoText.gameObject.SetActive(true);

        Canvas.ForceUpdateCanvases();
        vlGroup.enabled = false;
        vlGroup.enabled = false;
    }


    public void SwitchBy(string s, Text c)
    {
        switch (s)
        {
            //ROBOTIK
            //Basisfähigkeiten

            case "Vervielfältigung":
                c.text = "<b><i>Vervielfältigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held gibt ein Signal an den Roboterbegleiter und Dieser vervielfältigt sich. Der Roboter erschafft Kopien von sich, die für 4 Runden halten und sich danach wieder mit dem Begleiter zusammensetzen. Alle Kopien haben die Hälfte der aktuellen Lebenspunkte des Originals.\n" +
                    "Der Roboter kann maximal 2+KF Kopien von sich herstellen.\n" +
                    "Sobald der Roboterbegleiter zerstört ist, sind alle weiteren Kopien ebenfalls zerstört.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Mecha-Modus gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                break;

            case "Mecha-Modus":
                c.text = "<b><i>Mecha-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Roboterbegleiter funktioniert sich in eine Mecha-Rüstung um und verbindet sich mit einer Person. Diese Person erhält 5 Schild solange der Mecha-Modus aktiv ist.\n" +
                    "Jeder Schaden über dem Schild kostet den Roboter SS solange er im Mecha-Modus ist.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Vervielfältigung gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Attillerieangriff":
                c.text = "<b><i>Attillerieangriff</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Roboter\n\n" +
                    "<b>Beschreibung:</b>\nJeder aktive Roboter eröffnet ein Bombardement von Plasmaraketen, welche in einem kleinen Radius 1W6+KF Schaden pro Roboter anrichten.\n" +
                    "Der Roboterbegleiter richtet 1W6 Schaden mehr an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //stil-frei stärke

            case "Roboterarmee":
                c.text = "<b><i>Roboterarmee</i></b>\n\n" +
                    "Wenn der Held 2 oder mehr Roboterkopien gleichzeitig kontrolliert, kann neben Vervielfältigung eine weitere Fähigkeit pro Runde gewirkt werden.";



                break;


            //Kontrolle

            case "Festungsbefehl":
                c.text = "<b><i>Festungsbefehl</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle aktiven Roboter formen sich zusammen zu einem bodenlosen Raum mit 2 Meter hohen Wänden. Dieser Raum hält 24h lang und besitzt 12+KF LP pro Roboter.\n" +
                    "2 Roboter ergeben einen kleinen Raum, 4 Roboter ergeben einen mittleren Raum, 6 Roboter ergeben einen großen Raum.\n" +
                    "Die Roboter verbrauchen keine Energie mehr, können aber mit Ausnahme des Roboterbegleiters nicht aus der Festung entfernt werden.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Mecha-Modus gewirkt werden.Die Struktur kann für Deckung genutzt werden.\n\n " +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Statischer Stoß":
                c.text = "<b><i>Statischer Stoß</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle aktiven Roboter entfesseln einen statischen Stoß um sich herum, der Allen innerhalb der Reichweite einen Stromstoß gibt und sie für eine Runde betäubt, sofern sie nicht widerstehen, und ihnen 2W4+KF Schaden anrichtet.\n" +
                    "2 Roboter ergeben einen kleinen Raum, 4 Roboter ergeben einen mittleren Raum, 6 Roboter ergeben einen großen Raum.Schaden und Betäubung sind bei Maschinen verdoppelt.\n" +
                    "Um zu widerstehen muss das Ziel eine 2er-Würfel KK-Körper-Probe würfeln.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Kontrolle Stärke

            case "Künstliche Intelligenz":
                c.text = "<b><i>Künstliche Intelligenz</i></b>\n\n" +
                    "Wenn der Held einen Kontrolleffekt einsetzt und mehr als 1 Roboter besitzt, wird der Effekt von einem Roboter in der nächsten Runde wiederholt.";

                //

                break;

            //Zerstörung

            case "Angriffs-Modus":
                c.text = "<b><i>Angriffs-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDiese Fähigkeit kann zusammen mit Vervielfältigung gewirkt werden.1-3 Roboter funktionieren sich in den Angriffs-Modus um. Hierbei halten sie eine Runde weniger, allerdings richten sie nach jedem Angriff noch 1W4+KF zusätzlichen Schaden an, solange sie noch aktiv sind.\n" +
                    "Diese Fähigkeit kann zusammen mit Vervielfältigung gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Satellitenschlag":
                c.text = "<b><i>Satellitenschlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter heben ab und sammeln Energie von der Sonne für eine Runde. Die Runde darauf schießen sie von oben einen gebündelten Energiestrahl, der 2W6+KF Schaden pro Roboter anrichtet. Dies ist auch in Raumschiff-Schlachten möglich\n" +
                    "Das Ziel muss sich unter freiem Himmel befinden und eine Sonne muss für die Roboter erreichbar sein\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Letzter Funke":
                c.text = "<b><i>Letzter Funke</i></b>\n\n" +
                    "Wann immer ein Roboter zerstört oder abgeschaltet werden würde, können sie sich nach Wunsch des Spielers für 1W6 Schaden pro Roboter am nächsten Gegner entladen.";

                //

                break;

            //Risiko

            case "Mechafusion":
                c.text = "<b><i>Mechafusion</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Roboter fusioniert sich mit der Waffe des Helden und fügt sie zum Mecha-Modus hinzu solange der Mecha-Modus aktiv ist. Die Waffe richtet KF als Zusatzschaden an und darf neben normalen Kampf-Aktionen gefeuert werden.\n" +
                    "Mecha-Modus muss dafür aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Super-Mecha-Modus":
                c.text = "<b><i>Super-Mecha-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Mecha transformiert sich in einen 5 Meter großen Roboter, dabei wird der KF verdoppelt.\n" +
                    "Der Held innerhalb des Mechas erhält 10+KF Schild während die Fähigkeit aktiv ist.\n" +
                    "Jeder Schaden über dem Schild kostet den Roboter SS solange er im Mecha-Modus ist.\n" +
                    "Diese Fähigkeit geht nur innerhalb des Mecha-Moduses und zählt selbst als Mecha-Modus für alle weiteren Fähigkeiten und Stärken.\n" +
                    "Der Schild des vorherigen Mecha-Moduses wird nicht übernommen.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            //Risiko Stärke

            case "Mecha aktiviert!":
                c.text = "<b><i>Mecha aktiviert!</i></b>\n\n" +
                    "Solange der Held keine aktiven Roboter hat, kosten alle “Mecha-Modus”- Fähigkeiten 1 VE weniger.";

                //

                break;

            //Unterstützung

            case "Schutz-Modus":
                c.text = "<b><i>Schutz-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter heben ab und sammeln Energie von der Sonne für eine Runde. Die Runde darauf schießen sie von oben einen gebündelten Energiestrahl, der 2W6+KF Schaden pro Roboter anrichtet. Dies ist auch in Raumschiff-Schlachten möglich1-3 Roboter bauen sich um und erreichen Schutz-Modus, wobei sie, pro Roboter, 1W4+KF Schaden von Verbündeten in kleiner Reichweite abfangen und selbst erhalten. Wann immer ein Gegner einen Roboter im Schutz-Modus angreift erhält der Gegner 1W4+KF Schaden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Schildwall":
                c.text = "<b><i>Schildwall</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter funktionieren sich zu einem Wall um, der beliebig eingesetzt werden kann. Gegner können nicht ungewollt an den Robotern passieren, bis sie nicht mindestens 10+KF Schaden genommen haben. Verbündete hinter dem Wall erhalten Deckung und können Ausweichen-Proben um 1 Würfel erleichtert würfeln.\n" +
                    "Dies kann gewirkt werden ohne eine Aktion zu benutzen und die Roboter können beliebig viele Ziele schützen, solange nicht mehr als ein Roboter pro Ziel eingesetzt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Unterstützung Stärke

            case "Schutzanzug":
                c.text = "<b><i>Schutzanzug</i></b>\n\n" +
                    "Der Held kann Verbündete ebenfalls mit Mecha-Modus mit seinem Roboter rüsten, allerdings können sie weiterhin nur ihre eigenen Fähigkeiten benutzen.";

                //

                break;

            //GRAVITATION   
            //Basisfähigkeiten

            case "Kraftzug":
                c.text = "<b><i>Kraftzug</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann einen Gegenstand oder eine Person bis zu 100kg an sich heranziehen. Bei schwereren Zielen, zieht er sich heran.\n" +
                    "Diese Fähigkeit darf statt einer Bewegungsaktion gewirkt werden\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Seismischer Schlag":
                c.text = "<b><i>Seismischer Schlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lädt seine Faust mit Gravitationskraft auf und schlägt das Ziel mit voller Wucht. Hierbei richtet er 2W4+KF Schaden an. Der Schaden wird verdoppeln, wenn das Ziel oder der Held in der Runde durch Gravitation bewegt wurde.\n\n" +
                    "<b>Reichweite:</b>\nKlein";



                break;


            case "Schwerelosigkeit":
                c.text = "<b><i>Schwerelosigkeit</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held  oder ein willentliches Ziel wird schwerelos und kann für eine Stunde langsam durch die Luft schweben. Dies wird ab dem ersten Bodenkontakt abgebrochen.\n\n" +
                    "<b>Reichweite:</b>\n---";



                break;

            //Stil-frei Stärke

            case "Ereignishorizont":
                c.text = "<b><i>Ereignishorizont</i></b>\n\n" +
                    "Der KF aller Fähigkeiten wird um 3 erhöht, wenn sie aus kleiner Distanz angewendet werden.";



                break;

            //Kontrolle

            case "Schwergewicht":
                c.text = "<b><i>Schwergewicht</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann das Gewicht eines Gegenstands für 2+KF Runden verdoppeln, allerdings geht dies nur für Gegenstände, die maximal 500kg schwer sind.\n" +
                    "Lebewesen verlieren stattdessen bei diesem Effekt 5 SL.\n" +
                    "Der Held muss das Ziel für diesen Effekt berühren.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Sogzentrum":
                c.text = "<b><i>Sogzentrum</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieball zu dem alles in einem mittleren Radius hingezogen wird. Um sich zu befreien müssen, abhängig von der Distanz zum Zentrum, 1-3 3er Würfel KK-Körper-Proben gemacht werden. Zum Ende des Sogs erhalten alle Ziele im Zentrum 2W6+KF Schaden.\n" +
                    "Der Sog hält 5 Runden lang an, kann aber früher beendet werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Bis zur Unendlichkeit":
                c.text = "<b><i>Bis zur Unendlichkeit</i></b>\n\n" +
                    "Die Reichweite aller Fähigkeiten, die Ziele  bewegen ist um eine Distanzklasse erhöht.";

                //

                break;

            //Zerstörung

            case "Schockstoß":
                c.text = "<b><i>Schockstoß</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held rammt seine Fäuste in die Richtung des Ziels und schickt einen Schock aus, der allen in einer geraden Linie 3W4+KF Schaden zufügt.\n" +
                    "Bei vollem Schaden werden alle Ziele umgeworfen und für eine Runde betäubt.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Erdbeben":
                c.text = "<b><i>Erdbeben</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieimpuls in die Erde. Kurze Zeit später bebt die Erde und richtet 3W4+KF Schaden an Allem innerhalb eines großen Radius pro Runde für 3 Runden an. Alles im kleinen Radius zum Helden nimmt keinen Schaden. Alle innerhalb des Bebens haben halbe Bewegungsreichweite.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Zerstörung Stärke

            case "Nachbeben":
                c.text = "<b><i>Nachbeben</i></b>\n\n" +
                    "Alle Fähigkeiten, die Schaden an nur einem Ziel anrichten, richten zusätzlich halben Schaden an Zielen im kleinen Radius hinter dem Ziel an.";

                //

                break;

            //Risiko

            case "Konterschlag":
                c.text = "<b><i>Konterschlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held nutzt seine Gravitationskräfte, um das Ziel zu beschleunigen, während es ihn angreift.\n" +
                    "Wenn die Fähigkeit gelingt, richtet sie 3W4+KF Schaden an.\n" +
                    "Wenn die Fähigkeit misslingt, richtet der Gegner zusätzlich 1W4 Schaden an.\n" +
                    "Diese Fähigkeit kann nur innerhalb eines gegnerischen Angriffs gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Kometenhieb":
                c.text = "<b><i>Kometenhieb</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schmettert ein Ziel von sich weg und richtet 3W4+KF Schaden an.\n" +
                    "< 100kg = Ziel wird um eine große Distanz bewegt\n" +
                    "< 200kg = Ziel wird um eine mittlere Distanz bewegt\n" +
                    "> 200kg = Der Held nimmt stattdessen den Schaden und wird eine mittlere Distanz wegbewegt.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Risiko Stärke

            case "Schwerkraft":
                c.text = "<b><i>Schwerkraft</i></b>\n\n" +
                    "Ziele, die vom Helden angegriffen wurden verlieren einmalig 1W4 SL bis zum Ende des Kampfs, allerdings verliert der Held ebenfalls 2 SL.";

                //

                break;

            //Unterstützung

            case "Beschleunigung":
                c.text = "<b><i>Beschleunigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nEin Ziel der Wahl erhält +3 SL für 1+KF Runden und kann einen Waffenangriff zusätzlich pro Runde machen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Raumkrümmung":
                c.text = "<b><i>Raumkrümmung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held krümmt für eine Runde das Raumgefüge und kann damit dafür sorgen, dass eine Distanz innerhalb des Radiuses für bis zu 5 Runden verdoppelt oder halbiert ist. Er darf die Verhältnisse der Distanz innerhalb der 5 Runden einmal pro Runde ändern.\n" +
                    "Während Raumkrümmung aufrecht erhalten wird, darf der Held weitere Aktionen ausführen.\n\n" +
                    "<b>Reichweite:</b>\nGroßer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Defying Gravity":
                c.text = "<b><i>Defying Gravity</i></b>\n\n" +
                    "Verbündete, die von einer Fähigkeit des Helden getroffen werden, können für eine Runde die doppelte Distanz an Bewegung zurücklegen.";

                //

                break;


            //MIKROBIOLOGIE
            //Basisfähigkeiten

            case "Muskelschlaffheit":
                c.text = "<b><i>Muskelschlaffheit</i></b>\n\n" +
                    "<b>Wurf:</b>\n Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt eine Reihe an Pilzsporen gezielt frei, die ihren Weg zum Ziel finden. Die Sporen sorgen dafür, dass die Muskeln des Ziels sich entspannen und es träge wird.\n" +
                    "Ein waches Ziel verliert 1 SL und 1 KK pro Runde und ab der dritten Runde 4 KK. Ein schlafendes Ziel verfällt nach 10 Sekunden in Tiefschlaf für die nächste Stunde und kann nur durch Schmerz, Kälte, Fallgefühl oder Lebensgefahr geweckt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Regenerator":
                c.text = "<b><i>Regenerator</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erhöht die regenerativen Fähigkeiten des Ziels.\n" +
                    "Das Ziel heilt sich um 2+KF LP pro Runde. Diese Fähigkeit hält 3 Runden lang an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Blutplage":
                c.text = "<b><i>Blutplage</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDas Ziel wird mit Viren infiziert, die das Ziel innerlich bluten lassen und ihm jede Runde  1W6+KF Schaden zufügen. Dies hält für 6 Runden an.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            //Stil-frei Stärke

            case "Seuchenschleuder":
                c.text = "<b><i>Seuchen- schleuder</i></b>\n\n" +
                    "Für jede neue Fähigkeit richtet jede vorherige Fähigkeit auf dem selben Ziel 1 Schaden mehr pro Runde an.";



                break;

            //Kontrolle

            case "Schwarzmagenseuche":
                c.text = "<b><i>Schwarzmagenseuche</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt Bakterien los, die das Ziel infizieren. Die 1. Runde bemerkt das Ziel nichts, doch in der 2. Runde erhält es 1W6+KF Schaden und in der dritten Runde erhält es 6+KF Schaden und übergibt sich, was es für eine Runde betäubt, sofern es nicht widersteht.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er Probe von KK-Körper gelingen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Fieberwelle":
                c.text = "<b><i>Fieberwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Feinde im Radius erkranken plötzlich an einem Fieber. In der 1. Runde sind alle Proben der Feinde um 1 Würfel erschwert, in der 2. Runde sind alle Proben um 2 Würfel erschwert und in der dritten Runde besteht eine 1/6 Chance, dass das Ziel für 3 Runden erblindet, was seinen WW aller Proben um 3+KF senkt und die Erschwernis beibehält. Falls dies nicht eintritt, bleibt der Effekt von der 2. Runde.\n\n" +
                    "<b>Reichweite:</b>\nGroßer Radius";

                //

                break;

            //Kontrolle Stärke

            case "Steigernde Symptomatik":
                c.text = "<b><i>Steigernde Symptomatik</i></b>\n\n" +
                    "Schadenslose Zusatzeffekte treten eine Runde früher auf.";

                //

                break;

            //Zerstörung

            case "Ausbreitung":
                c.text = "<b><i>Ausbreitung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann alle existierenden Fähigkeiten eines Ziels, die er gewirkt hat, auf ein weiteres Ziel übertragen. Sie beginnen beim neuen Ziel, als wären sie frisch gewirkt worden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Nekrotische Seuche":
                c.text = "<b><i>Nekrotische Seuche</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entfesselt eine fleischfressende Seuche, die bis zu 3 Gegner von innen verrotten lässt. Diese hält für 4+KF Runden an.\n" +
                    "Der Held würfelt in der ersten Runde 2W4+KF. Die Seuche richtet in der ersten Runde das gewürfelte Ergebnis an Schaden an, der Schaden verdoppelt sich allerdings jede Runde. Leblose Körper, die von nekrotischer Seuche befallen sind, lösen sich restlos auf.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Zerstörung Stärke

            case "Ausbruch":
                c.text = "<b><i>Ausbruch</i></b>\n\n" +
                    "Fähigkeiten des Helden halten nur 3 Runden lang, richten aber in der ersten Runde doppelten Schaden an.";

                //

                break;

            //Risiko

            case "Transfusion":
                c.text = "<b><i>Transfusion</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held heilt ein Ziel sofort  um 3W6+KF LP, allerdings nimmt der Held die Hälfte der Heilung als Schaden.\n" +
                    "Der Held kann sich nicht selbst mit dieser Fähigkeit heilen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Viraler Schock":
                c.text = "<b><i>Viraler Schock</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entlädt eine gigantische Welle an Krankheitserregern auf den Gegner, die 4W6+KF auf dem Ziel an Schaden anrichtet. Sollte das Ziel den Viralen Schock jedoch überleben, richten alle mikrobiologischen Fähigkeiten daraufhin nur noch halben Schaden am Ziel an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Risiko Stärke

            case "Übertragbarkeit":
                c.text = "<b><i>Übertragbarkeit</i></b>\n\n" +
                    "Sollten die Fähigkeiten des Helden ein Ziel töten, besteht eine 5/20 Chance, dass sie auf ein weiteres Ziel im mittleren Radius überspringt. Bei einer 20 ist dieses Ziel ein Verbündeter.";

                //

                break;

            //Unterstützung

            case "Synthetischer Parasit":
                c.text = "<b><i>Synthetischer Parasit</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann zwei Ziele mit einem verbundenen Organismus belegen, welcher dem ersten Ziel Lebenskraft entzieht und jede Runde 2W4+KF Schaden zufügt und das zweite Ziel um den Schaden am ersten Ziel heilt.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Biotische Gaswolke":
                c.text = "<b><i>Biotische Gaswolke</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt eine Gaswolke um sich herum erscheinen, die alle Lebewesen innerhalb der Wolke um 2W4+KF pro Runde für 3 Runden heilt.\n" +
                    "Der Held kann bis zu 3 Ziele bestimmen, die nicht geheilt werden sollen.\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Symbiose":
                c.text = "<b><i>Symbiose</i></b>\n\n" +
                    "Heilungsfähigkeiten des Helden heilen einen weiteren Verbündeten in mittlerer Distanz.";

                //

                break;

            //MUTATION
            //Basisfähigkeiten 

            case "Animalischer Furor":
                c.text = "<b><i>Animalischer Furor</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann einen weiteren Angriff zusätzlich zu seiner normalen Angriffsaktion durchführen.\n" +
                    "Dies erfordert keine Kampfaktion.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Monsterkralle":
                c.text = "<b><i>Mutation: Monsterkralle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held transformiert eine seiner Hände zu einer gigantischen Pranke mit messerscharfen Klauen und schlägt einen Feind damit. Er richtet 3W4+KF Schaden an. \n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Monsterkralle” keine VE\n" +
                    "Monsterkralle erhöht ebenfalls die KK um 2\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nKlein";



                break;

            case "Genverzeichnis":
                c.text = "<b><i>Genverzeichnis</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (Wn)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nWenn der Held einen Feind getötet hat, kann er diesen fressen und erhält einen passiven Vorteil gegen die Spezies. Dadurch erhält er permanent 3+KF weniger Schaden von dieser Spezies.\n" +
                    "Dies ist nicht stapelbar.\n" +
                    "Die Schadensreduktion schließt unnatürlichen Schaden, wie Maschinen und Waffen aus.Der Held kann sich nicht selbst mit dieser Fähigkeit heilen.\n\n" +
                    "<b>Reichweite:</b>\n---";



                break;

            //Stil-frei Stärke

            case "Adaptabilität":
                c.text = "<b><i>Adaptabilität</i></b>\n\n" +
                    "Der Held kann 3 aktive Mutationen gleichzeitig auf seinem Körper halten.";



                break;

            //Kontrolle

            case "Mutation: Giftzunge":
                c.text = "<b><i>Mutation: Giftzunge</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert, so dass seine Zunge sich stark verlängert und mit einem Nervengift getränkt ist.\n" +
                    "Er greift das Ziel daraufhin mit einem peitschenden Zungenschlag an. Das Ziel erhält 2W4+KF Schaden. Bei jedem Angriff mit der Giftzunge besteht eine 5/20 Chance, dass das Ziel betäubt wird.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden.\n" +
                    "Solange die Mutation aktiv ist, kostet die nächste 'Giftzunge' keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Monstergebrüll":
                c.text = "<b><i>Monstergebrüll</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held adaptiert für eine kurze Zeit die Stimmen mehrerer Raubtiere und lässt ein furchteinflößendes Gebrüll erschallen.\n" +
                    "Alle Feinde in Reichweite verlieren 2W6+KF GG und werden vor Furcht betäubt, sofern eine 3er WK-Geist-Probe misslingt.\n" +
                    "Der Held kann das Gebrüll auch nutzen, um in einer furchteinflößenden Monsterstimme bis zu 10 Sekunden lang zu sprechen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Furchteinflößend":
                c.text = "<b><i>Furchteinflößend</i></b>\n\n" +
                    "Während der Held eine Mutation aktiv hat ist das Einschüchtern von Zielen um 1 Würfel erleichtert.";

                //

                break;

            //Zerstörung

            case "Raubtierinstinkt":
                c.text = "<b><i>Raubtierinstinkt</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erhält die Jagdinstinkte eines Raubtiers und erhält eine 1W4+KF Chance auf kritische Treffer mit seinen Fähigkeiten gegenüber ein vorher bestimmtes Ziel, sofern Raubtierinstinkt aktiv ist.\n" +
                    "Dies benötigt keine Kampfaktion.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Stachelregen":
                c.text = "<b><i>Mutation: Stachelregen</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDem Helden wachsen messerscharfe Stacheln aus dem Körper. Diese kann er nach Belieben herausschießen lassen und auf bis zu 3 Ziele niederregnen lassen kann. Jedes Ziel erhält 3W6+KF Schaden.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Stachelregen” keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Zerstörung Stärke

            case "Blutrausch":
                c.text = "<b><i>Blutrausch</i></b>\n\n" +
                    "Ziele, die den Helden bereits angegriffen haben, nehmen +3 Schaden vom Helden pro Fähigkeit.";

                //

                break;

            //Risiko

            case "Adrenalinpumpe":
                c.text = "<b><i>Adrenalinpumpe</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held pumpt seinen Körper voll mit Adrenalin. Sein KF steigt um 1W6 und erhält seinen KF an weniger Schaden während Adrenalinpumpe aktiv ist. \n" +
                    "Sobald Adrenalinpumpe nicht länger gewirkt wird, verliert er 2W6 Lebenspunkte.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Ultimativer Jäger":
                c.text = "<b><i>Mutation: Ultimativer Jäger</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert seinen Körper so, dass er den letzten Angriff reproduzieren kann, der ihn in diesem Kampf getroffen hat und verwendet ihn. Dies ist auch bei nicht-natürlichen Angriffen möglich.\n" +
                    "Hierbei richtet der Angriff exakt den gleichen Schaden an, den er am Helden angerichtet hat +KF. Die Fähigkeit übernimmt nur den Schaden und keine Zusatzeffekte.\n" +
                    "Die Mutation selbst hält 2 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Ultimativer Jäger” keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nAbhängig vom Angriff";

                //

                break;

            //Risiko Stärke

            case "Blut für Blut":
                c.text = "<b><i>Blut für Blut</i></b>\n\n" +
                    "Der Held kann 5 LP opfern, um seine VE-Kosten für die Runde um 1 zu verringern.";

                //

                break;

            //Unterstützung

            case "Mutation: Plattenpanzer":
                c.text = "<b><i>Mutation: Plattenpanzer</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert seinen Körper so, dass sich robuste Panzerplatten an seinem Körper bilden. Dadurch erhält er 7+KF Schild.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden.\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Genvermittlung":
                c.text = "<b><i>Genvermittlung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann all seine aktiven Mutationen auf einen Verbündeten übertragen, wenn er ihn berührt. Der Verbündete muss dies wollen.\n" +
                    "Ihre Laufzeiten gehen regulär weiter, bis sie ablaufen und alle Fähigkeiten der Mutationen können genutzt werden. Die Mutation selbst kann allerdings nicht durch den Verbündeten erneuert werden.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Unterstützung Stärke

            case "Lebensquell":
                c.text = "<b><i>Lebensquell</i></b>\n\n" +
                    "Der Held kann mit jeder Mutation, bis zu 10 LP von sich an eine weitere Person übertragen, sofern er sie berührt.";

                //

                break;

            //PHOTONIK
            //Basisfähigkeiten

            case "Aufladung":
                c.text = "<b><i>Aufladung</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1-3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDDer Held lädt sein Modul mit Energie auf. Für jeden investierten VE-Punkt erhält die nächste Fähigkeit +4 KF. Dies gilt nur für die erste Runde jeder Fähigkeit.\n" +
                    "Aufladen wird nicht an Fähigkeiten verbraucht, die keine KF benutzen, kann aber nur für maximal 5 Runden aufgehoben werden.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Energiekonvertierung":
                c.text = "<b><i>Energiekonvertierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held greift ein Ziel mit einer Energiewelle an und richtet 1W4+KF Schaden an. Die nächste Fähigkeit des Helden kostet ihn 1 VE weniger für jeden 2. Schadenspunkt, den Energiekonvertierung angerichtet hat.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            case "Instabiler Quantenball":
                c.text = "<b><i>Instabiler Quantenball</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieball heraus, der bei erstem Impakt explodiert und in einem kleinen Radius 2W4+KF Schaden anrichtet.\n" +
                    "Bei mehr als 14 Schaden wird der Radius verdoppelt.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            //Stil-frei Stärke

            case "Umleitung":
                c.text = "<b><i>Umleitung</i></b>\n\n" +
                    "Der Held kann seine Fähigkeiten in seine Waffe umleiten und die Effekte, sowie den halben Schaden der Waffe zusätzlich wirken.Der Schaden zählt nicht als KF.";



                break;

            //Kontrolle

            case "Blitzbetäubung":
                c.text = "<b><i>Blitzbetäubung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erzeugt einen nicht-tödlichen Energiestrahl, der 1W6+KF Schaden anrichtet und ein Ziel lähmen kann. Das Ziel ist für 1 Runde betäubt, allerdings funktioniert dies nur einmal pro Ziel. Dies ist nur möglich, wenn das Ziel den Helden ansieht.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Statikfeld":
                c.text = "<b><i>Statikfeld</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erzeugt ein hochenergetisches Feld um sich herum. Alle Gegner in dem Feld erhalten jede Runde 1W6+KF Schaden und verlieren einmalig 1W6 an SL.\n" +
                    "Während das Feld aktiv ist, kann der Held weitere Fähigkeiten wirken.\n" +
                    "“Umleitung” betrifft diese Fähigkeit nicht.\n" +
                    "Bei mehr als 5 SL-Verlust, werden die Ziele die erste Runde betäubt, sofern sie nicht widerstehen können.\n" +
                    "Um zu widerstehen muss eine 3er KK-Körper-Probe gewürfelt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //kontrolle Stärke

            case "Blendung":
                c.text = "<b><i>Blendung</i></b>\n\n" +
                    "Die Fähigkeiten des Helden blenden alle erfassten Ziele zusätzlich für eine Runde, wenn sie mit einem kritischen Erfolg geschafft werden. Aktionen von geblendeten Zielen sind um 2 Würfel erschwert.";

                //

                break;

            //Zerstörung

            case "Detonieren":
                c.text = "<b><i>Detonieren</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDiese Fähigkeit kann gewirkt werden,ohne eine Aktion zu erfordern, nachdem eine andere Fähigkeit ein Ziel getroffen hat. Um das Ziel herum explodiert die Energie der vorherigen Fähigkeit und richtet in einem kleinen Radius 2W4+KF Schaden an.\n" +
                    "Sollte Detonieren mehr als 12 Schaden anrichten, darf der Spieler.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            case "Todessstrahl":
                c.text = "<b><i>Todessstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held bündelt einen hochenergetischen Strahl, der Alles in einer Linie trifft und nicht aufgehalten oder reflektiert werden kann. Er richtet 3W6+KF Schaden an.\n" +
                    "Bei mehr als 18 Schaden, wird er in der nächsten Runde weiterhin gewirkt, ohne VE zu kosten. Dieser Effekt tritt nur einmal pro Wurf auf. Der Held kann die Richtung des Strahls nicht ändern.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Die Strahlen kreuzen":
                c.text = "<b><i>Die Strahlen kreuzen</i></b>\n\n" +
                    "Der Held kann in einer Runde zwei Fähigkeiten gleichzeitig einsetzen, muss aber in der Runde danach aussetzen.";

                //

                break;

            //Risiko

            case "Dematerialisierungskanone":
                c.text = "<b><i>Dematerialisierungskanone</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt ein Energieprojektil in die Luft, welches in einem hohen Bogen fliegt und in einem kleinen Radius landet. Dort richtet es 2W6+KF Schaden an Allen an. Getötete Lebewesen dematerialisieren sich komplett und stellen 1 VE beim Helden wieder her.\n" +
                    "Bei mehr als 15 Schaden an einem Ziel stellt sich auch ohne Tod eines Lebewesens 1 VE beim Helden wieder her.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            case "Photonenroulette":
                c.text = "<b><i>Photonenroulette</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erstellt einen extrem instabilen Ball an Energie und schießt ihn auf ein Ziel. Der Ball richtet 2W6+KF Schaden an dem Ziel an.\n" +
                    "Der Held würfelt 1W6, bei einer 6 implodiert der Ball und richtet zusätzliche 2W6+KF Schaden an. \n" +
                    "Bei einer 2-5 springt er weiter. Falls es weitere feindliche Ziele in Reichweite gibt, springt er auf sie über und implodiert dort. Falls es keine weiteren Feinde gibt, implodiert er in der Luft.\n" +
                    "Bei einer 1 springt der Ball auf ein Verbündetes Ziel in Reichweite oder den Helden über und implodiert dort.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke

            case "Instabile Kraft":
                c.text = "<b><i>Instabile Kraft</i></b>\n\n" +
                    "Die kritische Erfolgschance und die kritische Misserfolgs- chance aller Fähigkeiten des Helden sind um 1 erhöht.";

                //

                break;

            //Unterstützung

            case "Rückstoßschild":
                c.text = "<b><i>Rückstoßschild</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt eine Energieladung auf einen Verbündeten. Das Schild des Verbündeten wird mit Energie aufgeladen und erhält 1W6+KF Schild für 3 Runden. Jeder Nahkampfangriff auf den Verbündeten, der das Schild trifft, während es verstärkt ist, schickt einen Kraftimpuls durch den Feind, der 1W4+KF Schaden anrichtet und ihn leicht zurückschleudert. \n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Schildstrahl":
                c.text = "<b><i>Schildstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held stellt einen beschädigten/ zerstörten Schild um 2W6+KF Punkte wieder her.\n" +
                    "Falls dabei Punkte übrig bleiben sollten, stellt Schildstrahl 1VE beim Ziel für eine Runde wieder her.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Unterstützung Stärke

            case "Starthilfe":
                c.text = "<b><i>Starthilfe</i></b>\n\n" +
                    "Fähigkeiten, die auf Verbündete gewirkt werden, stellen 1 VE bei dem Verbündeten wieder her. (Dies gilt nur einmal pro Verbündeter)";

                //

                break;

            //HOLOTECH
            //Basisfähigkeiten

            case "Hologramm: Holo-Kopie":
                c.text = "<b><i>Hologramm: Holo-Kopie</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE alle 3 Runden\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erstellt eine holographische Kopie von einem Gegenstand oder Lebewesen seiner Wahl, die Gewichtsbelastung von bis zu 1+KF kg und 5+KF Schadenspunkte aushält und sich maximal eine riesige Reichweite weit von dem Helden entfernen kann. Die Kopie ist nur optisch und haptisch wahrnehmbar.\n" +
                    "Der Held kann die Kopie allein per Gedanken befehligen, solange er sie sehen kann.\n" +
                    "Der Held kann bis zu 3 Hologramme gleichzeitig aktiv halten.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Hologramm: Augmentierung":
                c.text = "<b><i>Hologramm: Augmentierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held umhüllt eine Waffe mit energiegeladenen Projektionen und erhöht ihren Schaden um 2W4+KF für 4 Runden.\n" +
                    "Dies nimmt keine Aktion in Anspruch\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            case "Hologramm: Reflektorwand":
                c.text = "<b><i>Hologramm: Reflektorwand</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held baut eine massive Wand aus Licht auf, die eine mittlere Reichweite lang ist. Diese Wand hält für eine Runde und hält Projektile auf, die 8+KF Schaden anrichten würden. Jedes Projektil, dass über diese Grenze geht, durchdringt die Wand, jedes Projektil, dass die Grenze nicht überschreitet, wird zurück auf den Schützen geschossen und richtet seinen Schaden +KF des Helden an.\n" +
                    "Der Held kann wählen Projektile nicht abzufangen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //Stil-frei Stärke

            case "Intelligentes Design":
                c.text = "<b><i>Intelligentes Design</i></b>\n\n" +
                    "Alle Hologramme können auch agieren, wenn der Held sie nicht sehen kann, sofern sie nicht weiter als eine riesige Distanz von ihm entfernt sind.";



                break;

            //Kontrolle

            case "Verzerrtes Bild":
                c.text = "<b><i>Verzerrtes Bild</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held setzt ein Hologramm nahezu exakt an die Stelle eines Ziels. Die Körper der Halluzination überschneiden sich, bewegen sich aber leicht anders, als das Ziel selbst. Dies macht es schwieriger das Ziel zu treffen und erschwert alle Angriffe auf das Ziel so, dass ohne AW gewürfelt werden muss.\n" +
                    "Wann immer der Feind das Ziel verfehlt, greift das verzerrte Bild den Feind an und fügt ihm 1W6+KF Schaden zu. Dies hält 3 Runden lang an. Es kann nur ein “Verzerrtes Bild” gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Reflektionsmaximum":
                c.text = "<b><i>Reflektionsmaximum</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held verändert die Lichtreflektion in einem mittleren Radius  so, dass gewünschte Objekte innerhalb entweder komplett unsichtbar werden oder der Ort zu einem lichtleeren Raum wird. Ziele innerhalb des unsichtbaren Raums können um 3 Würfel erleichtert ausweichen und sie zu bemerken, wenn sie sich nicht erkenntlich geben, ist um 3 Würfel erschwert.\n" +
                    "In den Runden nach der ersten Wirkung von Reflektionsmaximum können weitere Aktionen gewirkt werden, während es aufrecht erhalten wird.\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Kontrolle Stärke

            case "Sensorisches Erlebnis":
                c.text = "<b><i>Sensorisches Erlebnis</i></b>\n\n" +
                    "Alle Illusionen beinhalten auch Geräusche, sofern der Held das nachgebildete Geräusch bereits gehört hat.";

                //

                break;

            //Zerstörung

            case "Hologramm: Lichtwaffe":
                c.text = "<b><i>Hologramm: Lichtwaffe</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erschafft eine holographische Kopie seiner Waffe, die im Raum schwebt und jede Runde  Waffenschaden+KF Schaden anrichtet. Der Held kann die Waffe gedanklich befehligen, die Waffe kann sich nicht weiter als eine große Distanz vom Ziel wegbewegen.\n" +
                    "Die Waffe kann nicht zerstört werden und keine Fähigkeiten, die Waffen betreffen betreffen die Lichtwaffe, mit Ausnahme von “Augmentierung”.\n" +
                    "Die Waffe hält 4 Runden lang, der Held kann maximal 3 Lichtwaffen gleichzeitig aktiv haben.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Lichtspaltung":
                c.text = "<b><i>Lichtspaltung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n4 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Hologramme des Helden explodieren und richten 4W4+KF Schaden pro Hologramm in einem kleinen Radius an.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Durchdringung":
                c.text = "<b><i>Durchdringung</i></b>\n\n" +
                    "Alle Fähigkeiten ignorieren 4 Punkte gegnerischer Schilde.";

                //

                break;

            //Risiko

            case "Lichtschlucker":
                c.text = "<b><i>Lichtschlucker</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann seine gesamte Schildkapazität opfern, um für eine Runde die Hälfte seines Schilds als KF zu erhalten. \n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Lichtkaskade":
                c.text = "<b><i>Lichtkaskade</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schickt drei Lichtwellen mit mittlerem Durchmesser vor sich aus, die jeweils 1W6+KF Schaden an allen Zielen anrichtet. Sollte allerdings eine 6 gewürfelt werden, wird die Welle reflektiert, sobald sie ihr Ende erreicht hat und wird erneut in Richtung des Helden gewirkt, wobei sie ebenfalls den Helden trifft.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke

            case "Schildkonverter":
                c.text = "<b><i>Schildkonverter</i></b>\n\n" +
                    "Der Held erhält 1 KF für jeden 3. aktuellen Schildpunkt, wann immer er sein Schild deaktiviert.";

                //

                break;

            //Unterstützung

            case "Hologramm: Holographische Textur":
                c.text = "<b><i>Hologramm: Holographische Textur</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Stunde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held legt eine holographische Schicht über ein Objekt oder ein Lebewesen und kann dessen Aussehen leicht verändern. Die generelle Form des Objekts bleibt gleich, aber einzelne Details, wie Gesichter oder Rillen können anders dargestellt werden. Die Textur ist rein optisch.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Holographische Kuppel":
                c.text = "<b><i>Holographische Kuppel</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>Der Held erschafft eine Kuppel aus Licht, die Schaden absorbiert. Die Kuppel hält 14+KF Schaden aus und umgibt ihren gesamten Bereich. Der Held kann die Kuppel deaktivieren, um allen außerhalb der Kuppel in einem Ring den Schaden anzurichten, der abgefangen wurde.\n" +
                    "Schadensquellen innerhalb der Kuppel werden ignoriert.\n" +
                    "Der Held kann sich bewegen, während er die Kuppel aktiv hält und kann weitere Aktionen tätigen (außer in der Runde der Aktivierung und der Runde der Deaktivierung)\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Kompatibel":
                c.text = "<b><i>Kompatibel</i></b>\n\n" +
                    "Der Held darf einem Verbündeten in großer Reichweite zu Beginn der Runde einmal pro Runde 2 Schildpunkte geben, die eine Runde lang halten.";

                //

                break;

            //PSYONIK
            //Basisfähigkeiten

            case "Glücksstrahl":
                c.text = "<b><i>Glücksstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held bestrahlt ein Ziel mit einem Glücksstrahler, der für alle sichtbar ist. Sollte das Ziel nicht widerstehen können, wird das Ziel wird beruhigt und dem Helden gegenüber gutgestimmt. Sofern es nicht widersteht, erinnert sich nach 30min jedoch an die Bestrahlung und verliert den Effekt.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Gedankenschinden":
                c.text = "<b><i>Gedankenschinden</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held greift die Psyche des Gegners direkt an, wodurch dieser massive Kopfschmerzen bekommt und richtet 3W4 + KF Schaden und 2W4 GG-Schaden an. \n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Psychokinese":
                c.text = "<b><i>Psychokinese</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann ein Objekt oder ein Lebewesen unter 50 x KF kg mit Hilfe seiner Gedankenkraft  eine kleine Distanz weit bewegen. Sollte das Lebewesen geworfen werden, richtet ein gewöhnlicher Aufprall 3W4+KF Schaden an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //Stil-frei Stärke

            case "Geistige Beherrschung":
                c.text = "<b><i>Geistige Beherrschung</i></b>\n\n" +
                    "Der Held kann bei Fähigkeiten Stress auswürfeln, nachdem der ursprüngliche  Wurf bereits gefallen ist, allerdings muss er hierfür 2 GG zusätzlich verbrauchen.";



                break;

            //Kontrolle

            case "Innere Schockwelle":
                c.text = "<b><i>Innere Schockwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held sendet eine mentale Schockwelle aus, die allen Feinden 2W4+KF Schaden anrichtet und sie für 1 Runde festhält und an Bewegungsaktionen hindert, sofern sie nicht widerstehen.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Geistesergriff":
                c.text = "<b><i>Geistesergriff</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held fesselt die Gedanken seines Gegners und zwingt sie an etwas zu denken, was der Held vorher festlegen kann. Der Feind ist für eine Runde betäubt außer, wenn er alles ausspricht, was ihm zu dem Gedanken in den Kopf kommt.\n" +
                    "Um zu widerstehen muss dem Ziel eine 5er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Kontrolle Stärke

            case "Unbrechbarer Wille":
                c.text = "<b><i>Unbrechbarer Wille</i></b>\n\n" +
                    "Der Held kann seine Kontroll-Effekte um eine Runde verlängern, wenn ihm eine AW-lose 5er WK-Probe gelingt.";

                //

                break;

            //Zerstörung

            case "Psychischer Schrei":
                c.text = "<b><i>Psychischer Schrei</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entfesselt einen markerschütternden Schrei innerhalb des Geistes aller Feinde und richtet dabei  2W4 Schaden und 2W4+KF GG-Schaden an allen an.\n" +
                    "Sollte der zugefügte GG-Schaden höher sein, als der normale Schaden, darf der Held 1W4+KF Schaden zusätzlich verursachen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Neuronaler Zusammenbruch":
                c.text = "<b><i>Neuronaler Zusammenbruch</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt das Gehirn des Gegners zusammenbrechen und richtet dabei 4W4+KF Schaden und GG-Schaden an. Sollte der Feind vor dem Angriff unter der Hälfte seiner GG sein, ist der Schaden verdoppelt.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Zerstörung Stärke

            case "Gebrochener Geist":
                c.text = "<b><i>Gebrochener Geist</i></b>\n\n" +
                    "Für jeden 4. Schadenspunkt der Fähigkeiten des Helden verliert das Ziel 1 GG.";

                //

                break;


            //Risiko

            case "Erinnerungsfresser":
                c.text = "<b><i>Erinnerungsfresser</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held frisst sich durch die Gedanken seines Ziels und richtet 1W10+KF Schaden an. Abhängig vom Wurf geschieht folgender Zusatzeffekt.\n" +
                    "1: Der Held vergisst eine zufällige Fähigkeit bis zum Ende des Tages\n2 - 5: Der Held und das Ziel verlieren beide 2W6 GG\n6 - 9: Das Ziel vergisst eine Erinnerung, die der Held lesen kann\n10: Das Ziel vergisst eine Erinnerung, die der Held lesen kann und das Ziel verliert 3W6 GG.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Strahlender Wahnsinn":
                c.text = "<b><i>Strahlender Wahnsinn</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDDer Held verknüpft den Geist seines Ziels mit seinem eigenen Geist und füllt diesen mit Visionen des Wahnsinns. Dabei fügt er ihm und sich selbst 3W6+KF GG-Schaden zu.\n" +
                    "Sollte der Held bereits dabei unter die Hälfte seiner GG erreichen, richtet die Fähigkeit den doppelten GG Schaden als physischen Schaden am Gegner an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Risiko Stärke 

            case "Dem Wahnsinn verfallen":
                c.text = "<b><i>Dem Wahnsinn verfallen</i></b>\n\n" +
                    "Der Held kann 10 GG opfern um seine VE-Kosten für die Runde um 1 zu verringern.";

                //

                break;

            //Unterstützung

            case "Ruhiger Geist":
                c.text = "<b><i>Ruhiger Geist</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nRuhiger Geist\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Körper und Geist":
                c.text = "<b><i>Körper und Geist</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held nimmt einem Verbündeten jeglichen Schmerz und sorgt dafür, dass er so viele Lebenspunkte wie GG hat (sofern er mehr GG als LP besitzt). Nach Ablaufen der Fähigkeit verliert der Verbündete die Hälfte der Lebenspunkte wieder.\n" +
                    "Diese Fähigkeit kann als Reaktion im gegnerischen Zug gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Unterstützung Stärke

            case "Neurologische Stimulierung":
                c.text = "<b><i>Neurologische Stimulierung</i></b>\n\n" +
                    "Am Ende des Kampfs stellt der Held 1W6GG bei sich und allen Verbündeten wieder her.";

                //

                break;

            //THERMOCHEMIE
            //Basisfähigkeiten

            case "Thermische Einstellung":
                c.text = "<b><i>Thermische Einstellung</i></b>\n\n" +
                    "<b>Wurf:</b>\n---\n\n" +
                    "<b>Kosten:</b>\n---\n\n" +
                    "<b>Beschreibung:</b>\nDer Held stellt sein Modul auf Hitze oder Kälte ein. Dies modifiziert alle weiteren Fähigkeiten, die mit “Hitze” oder “Kälte” versehen wurden.\n" +
                    "Die Einstellung erfordert keine Aktion\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Thermischer Schock":
                c.text = "<b><i>Thermischer Schock</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>Der Held kann Hitze oder Kälte der Umgebung oder eines Objekts umwandeln und in einem Angriff freisetzen. Die Umgebung oder das Objekt verliert dabei ein wenig an Hitze/ Kälte.\n" +
                    "Dampfschock: (Hitze) Kann als Reaktion im gegnerischen Zug gewirkt werden und richtet 2W4+KF Schaden an.\n" +
                    "Frostschock: (Kälte)  Richtet 3W4+KF Schaden an.\n\n" +
                    "<b>Reichweite:</b>\nGroße Reichweite";



                break;

            case "Wetterumschwung":
                c.text = "<b><i>Wetterumschwung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>Das Wetter der Gegend kann, abhängig von der thermischen Einstellung und den existierenden Bedingungen vorübergehend verändert werden. Dafür wird eine Atmosphäre benötigt. Das Wetter hält 5 + KF Runden an.\n" +
                    "<b>Sonnig + <i>Kälte</i></b> = Bewölkt mit Chance auf Regen\n" +
                    "<b>Sonnig + <i>Hitze</i></b> = Dürre (alle Einheiten haben -2 KK, +2 KF für Hitze)\n" +
                    "<b>Dürre + <i>Kälte</i></b> = Sonnig\n" +
                    "<b>Bewölkt + <i>Hitze</i></b> = Gewitter (1/20 Chance pro Ziel 3W4 Schaden zu erhalten)\n" +
                    "<b>Bewölkt + <i>Kälte</i></b> = Schnee\n" +
                    "<b>Gewitter + <i>Kälte</i></b> = Schneesturm  (Alle Einheiten verlieren 3 Lebenspunkte pro Runde. +2 KF für Kälte)\n" +
                    "<b>Schnee + <i>Hitze</i></b> = Regenschauer\n" +
                    "<b>Schneesturm + <i>Hitze</i></b> = Gewitter\n\n" +
                    "<b>Reichweite:</b>\nRiesiger Radius";



                break;

            //Stil-frei Stärke

            case "Sturmwarnung":
                c.text = "<b><i>Sturmwarnung</i></b>\n\n" +
                    "Alle Wetterzustände halten 2 Runden länger an.";



                break;

            //Kontrolle

            case "Verfestigung":
                c.text = "<b><i>Verfestigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann den Aggregatzustand eines flüssigen oder gasförmigen Objekts ändern und es verfestigen, solange er es berührt. Das Objekt wird für eine Stunde verfestigt und nimmt danach seine vorherige Form an.\n" +
                    "Dies funktioniert nicht für Lebewesen.\n" +
                    "Der Held kann dies nicht für Objekte nutzen, die eine größere Masse besitzen, als er. \n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;


            case "Eisige Adern/ Brodelndes Blut":
                c.text = "<b><i>Eisige Adern/ Brodelndes Blut</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisige Adern: <i>(Kälte)</i></b> Der Held durchflutet den Körper des Ziels mit Kälte, wobei es 3W6 Schaden nimmt und für 3+KF Runden 4 SL verliert. Dies ist nicht stapelbar.\n" +
                    "<b>Brodelndes Blut: <i>(Hitze)</i></b> Der Held durchflutet den Körper des Ziels mit Hitze, wobei es 3W6 +KF Schaden nimmt und für eine Runde bewegungsunfähig wird, wenn es keine 3er Körper-Probe schafft.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Gefrierbrand":
                c.text = "<b><i>Gefrierbrand</i></b>\n\n" +
                    "Alle Fähigkeiten mit SL-Verringerungen oder Betäubung erhalten +2 KF";

                //

                break;

            //Zerstörung

            case "Froststrahl/Feuerwelle":
                c.text = "<b><i>Froststrahl/Feuerwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Froststrahl: <i>(Kälte)</i></b> Der Held greift das Ziel mit einem eisigen Projektil an und richtet 3W4+KF Schaden an. Ziele die in den letzten 3 Runden von Kälte-Fähigkeiten getroffen wurden, erhalten doppelten Schaden.\n" +
                    "<b>Feuerwelle: <i>(Hitze)</i></b> Der Held beschießt bis zu 3 Ziele mit eine Welle an konzentrierter Hitze, die 2W6+KF Schaden pro Ziel anrichten. Sollte das Ziel 6 oder mehr Schaden an einem Ziel verursachen, nimmt das Ziel die nächsten 3 Runden 1W6 Brandschaden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Blizzard/Sengende Luft":
                c.text = "<b><i>Blizzard/Sengende Luft</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\n<b>Blizzard: <i>(Kälte)</i></b>  Der Held erlangt Meisterschaft über das Wetter und beschwört einen Blizzard herauf. Dieser richtet 2W6+KF Schaden an allen Zielen innerhalb des Radiuses pro Runde an und verlangsamt sie um 3 SL.  Alle Ziele innerhalb eines kleinen Radiuses vom Helden aus, sind nicht vom Blizzard betroffen. Blizzard zählt als Schneesturm und übernimmt seine Eigenschaften, aber nicht den Schaden.\n" +
                    "<b>Sengende Luft: <i>(Hitze)</i></b> Der Held erlangt Meisterschaft über das Wetter und beschwört eine massive Hitzewelle herauf. Diese richtet 2W6+KF Schaden an allen Zielen innerhalb des Radiuses an. Alle Ziele, die 10 oder mehr Schaden erhalten, nehmen zusätzlich 1W4 Brandschaden pro Runde für 3 Runden. Brennende Luft zählt als Dürre und übernimmt ihre Eigenschaften.\n" +
                    "Während Blizzard/ Sengende Luft wirkt, kann der Held weitere Fähigkeiten wirken.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Inferno":
                c.text = "<b><i>Inferno</i></b>\n\n" +
                    "Jede dritte Hitze-Fähigkeit im Kampf richtet doppelten Schaden an.";

                //

                break;

            //Risiko

            case "Eisspeere/Feuersalve":
                c.text = "<b><i>Eisspeere/Feuersalve</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisspeere: <i>(Kälte)</i></b> Der Held schießt 1W4 Eisspeere, die jeweils 1W4+KF Schaden anrichten, auf bis zu 1W4 Ziele (die selbe Anzahl wie Eisspeere). Jeder Eisspeer richtet +2 Schaden für jeden weiteren Eisspeer der das Ziel in der Runde trifft an.\n" +
                    "<b>Feuersalve: <i>(Hitze)</i></b> Der Held schießt 5 flammende Projektile, die zufällig auf alle Ziele in Reichweite verteilt werden und 1W4+KF Schaden anrichten. Verbündete Ziele erhalten nur den halben Schaden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Lebende Bombe":
                c.text = "<b><i>Lebende Bombe</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n4 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held belegt das Ziel für 2 Runden mit einer Welle an thermischer Energie, die herausbricht, wenn das Ziel in einer Runde mehr als 10-KF Schaden genommen hat. Daraufhin explodiert die Energie aus dem Ziel heraus und richtet 3W6 Schaden an.\n" +
                    "<b><i>Kälte:</i></b> Die Explosion hat eine 10/20 Chance pro Ziel zu betäuben und es festzufrieren.\n" +
                    "<b><i>Hitze:</i></b> Die Explosion verteilt Lebende Bombe auf ein weiteres Ziel der Wahl in der Nähe.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke 

            case "Wechselwirkung":
                c.text = "<b><i>Wechselwirkung</i></b>\n\n" +
                    "Wann immer der Held einen Fähigkeitstypen (Hitze/Kälte) wiederholt, kostet die zweite Fähigkeit 1 VE mehr. Wann immer er einen Typ nicht wiederholt, kostet die 2. Fähigkeit 1 VE weniger.";

                //

                break;

            //Unterstützung

            case "Eisige Berührung/ Feuerteufel":
                c.text = "<b><i>Eisige Berührung/ Feuerteufel</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisige Berührung: <i>(Kälte)</i></b> Der Held verstärkt eine Waffe  mit thermischer Kälteenergie, woraufhin die Waffe für 1+KF Runden lang mit jedem Waffenangriff eine 1/6 Chance hat ein Ziel zu betäuben.\n" +
                    "<b>Feuerteufel: <i>(Hitze)</i></b> Der Held verstärkt eine Waffe mit thermischer Hitzeenergie, woraufhin die Waffe für 1+KF Runden lang mit jedem Waffenangriff eine 1W6 Chance hat zu überhitzen und einen weiteren Angriff abzufeuern.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Eisstruktur/ Kauterisierung":
                c.text = "<b><i>Eisstruktur/ Kauterisierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisstruktur: <i>(Kälte)</i></b> Der Held erzeugt eine gefrorene Struktur, die bis zu 5 Meter groß ist und 10 Runden lang hält, bis sie zerschmilzt. Sie kann bis zu 200kg und 20 Schadenspunkte aushalten. Je komplizierter die Form ist, desto länger benötigt das Wirken dieser Fähigkeit. Dies benötigt Flüssigkeit.\n" +
                    "<b>Kauterisierung: <i>(Hitze)</i></b> Der Held kauterisiert eine Wunde und heilt ein Ziel um 4W4+KF Lebenspunkte. Der Ziel nimmt daraufhin für 2 Runden 2W6 Schaden pro Runde.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Unterstützung Stärke

            case "Windschatten":
                c.text = "<b><i>Windschatten</i></b>\n\n" +
                    "Wetter-Fähigkeiten erhöhen die SL aller Gruppenmitglieder um 2 für eine Runde.(Dies zählt nicht für wetterberuhigende Fähigkeiten)";

                //

                break;
        }
    }

    public string SwitchBy(string s)
    {
        string c = "";

        switch (s)
        {
            //ROBOTIK
            //Basisfähigkeiten

            case "Vervielfältigung":
                c = "<b><i>Vervielfältigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held gibt ein Signal an den Roboterbegleiter und Dieser vervielfältigt sich. Der Roboter erschafft Kopien von sich, die für 4 Runden halten und sich danach wieder mit dem Begleiter zusammensetzen. Alle Kopien haben die Hälfte der aktuellen Lebenspunkte des Originals.\n" +
                    "Der Roboter kann maximal 2+KF Kopien von sich herstellen.\n" +
                    "Sobald der Roboterbegleiter zerstört ist, sind alle weiteren Kopien ebenfalls zerstört.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Mecha-Modus gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                break;

            case "Mecha-Modus":
                c = "<b><i>Mecha-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Roboterbegleiter funktioniert sich in eine Mecha-Rüstung um und verbindet sich mit einer Person. Diese Person erhält 5 Schild solange der Mecha-Modus aktiv ist.\n" +
                    "Jeder Schaden über dem Schild kostet den Roboter SS solange er im Mecha-Modus ist.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Vervielfältigung gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Attillerieangriff":
                c = "<b><i>Attillerieangriff</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Roboter\n\n" +
                    "<b>Beschreibung:</b>\nJeder aktive Roboter eröffnet ein Bombardement von Plasmaraketen, welche in einem kleinen Radius 1W6+KF Schaden pro Roboter anrichten.\n" +
                    "Der Roboterbegleiter richtet 1W6 Schaden mehr an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //stil-frei stärke

            case "Roboterarmee":
                c = "<b><i>Roboterarmee</i></b>\n\n" +
                    "Wenn der Held 2 oder mehr Roboterkopien gleichzeitig kontrolliert, kann neben Vervielfältigung eine weitere Fähigkeit pro Runde gewirkt werden.";



                break;


            //Kontrolle

            case "Festungsbefehl":
                c = "<b><i>Festungsbefehl</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle aktiven Roboter formen sich zusammen zu einem bodenlosen Raum mit 2 Meter hohen Wänden. Dieser Raum hält 24h lang und besitzt 12+KF LP pro Roboter.\n" +
                    "2 Roboter ergeben einen kleinen Raum, 4 Roboter ergeben einen mittleren Raum, 6 Roboter ergeben einen großen Raum.\n" +
                    "Die Roboter verbrauchen keine Energie mehr, können aber mit Ausnahme des Roboterbegleiters nicht aus der Festung entfernt werden.\n" +
                    "Diese Fähigkeit erfordert keine Kampfaktion, kann aber nicht zusammen mit Mecha-Modus gewirkt werden.Die Struktur kann für Deckung genutzt werden.\n\n " +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Statischer Stoß":
                c = "<b><i>Statischer Stoß</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle aktiven Roboter entfesseln einen statischen Stoß um sich herum, der Allen innerhalb der Reichweite einen Stromstoß gibt und sie für eine Runde betäubt, sofern sie nicht widerstehen, und ihnen 2W4+KF Schaden anrichtet.\n" +
                    "2 Roboter ergeben einen kleinen Raum, 4 Roboter ergeben einen mittleren Raum, 6 Roboter ergeben einen großen Raum.Schaden und Betäubung sind bei Maschinen verdoppelt.\n" +
                    "Um zu widerstehen muss das Ziel eine 2er-Würfel KK-Körper-Probe würfeln.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Kontrolle Stärke

            case "Künstliche Intelligenz":
                c = "<b><i>Künstliche Intelligenz</i></b>\n\n" +
                    "Wenn der Held einen Kontrolleffekt einsetzt und mehr als 1 Roboter besitzt, wird der Effekt von einem Roboter in der nächsten Runde wiederholt.";

                //

                break;

            //Zerstörung

            case "Angriffs-Modus":
                c = "<b><i>Angriffs-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDiese Fähigkeit kann zusammen mit Vervielfältigung gewirkt werden.1-3 Roboter funktionieren sich in den Angriffs-Modus um. Hierbei halten sie eine Runde weniger, allerdings richten sie nach jedem Angriff noch 1W4+KF zusätzlichen Schaden an, solange sie noch aktiv sind.\n" +
                    "Diese Fähigkeit kann zusammen mit Vervielfältigung gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Satellitenschlag":
                c = "<b><i>Satellitenschlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter heben ab und sammeln Energie von der Sonne für eine Runde. Die Runde darauf schießen sie von oben einen gebündelten Energiestrahl, der 2W6+KF Schaden pro Roboter anrichtet. Dies ist auch in Raumschiff-Schlachten möglich\n" +
                    "Das Ziel muss sich unter freiem Himmel befinden und eine Sonne muss für die Roboter erreichbar sein\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Letzter Funke":
                c = "<b><i>Letzter Funke</i></b>\n\n" +
                    "Wann immer ein Roboter zerstört oder abgeschaltet werden würde, können sie sich nach Wunsch des Spielers für 1W6 Schaden pro Roboter am nächsten Gegner entladen.";

                //

                break;

            //Risiko

            case "Mechafusion":
                c = "<b><i>Mechafusion</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Roboter fusioniert sich mit der Waffe des Helden und fügt sie zum Mecha-Modus hinzu solange der Mecha-Modus aktiv ist. Die Waffe richtet KF als Zusatzschaden an und darf neben normalen Kampf-Aktionen gefeuert werden.\n" +
                    "Mecha-Modus muss dafür aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Super-Mecha-Modus":
                c = "<b><i>Super-Mecha-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Mecha transformiert sich in einen 5 Meter großen Roboter, dabei wird der KF verdoppelt.\n" +
                    "Der Held innerhalb des Mechas erhält 10+KF Schild während die Fähigkeit aktiv ist.\n" +
                    "Jeder Schaden über dem Schild kostet den Roboter SS solange er im Mecha-Modus ist.\n" +
                    "Diese Fähigkeit geht nur innerhalb des Mecha-Moduses und zählt selbst als Mecha-Modus für alle weiteren Fähigkeiten und Stärken.\n" +
                    "Der Schild des vorherigen Mecha-Moduses wird nicht übernommen.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            //Risiko Stärke

            case "Mecha aktiviert!":
                c = "<b><i>Mecha aktiviert!</i></b>\n\n" +
                    "Solange der Held keine aktiven Roboter hat, kosten alle “Mecha-Modus”- Fähigkeiten 1 VE weniger.";

                //

                break;

            //Unterstützung

            case "Schutz-Modus":
                c = "<b><i>Schutz-Modus</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter heben ab und sammeln Energie von der Sonne für eine Runde. Die Runde darauf schießen sie von oben einen gebündelten Energiestrahl, der 2W6+KF Schaden pro Roboter anrichtet. Dies ist auch in Raumschiff-Schlachten möglich1-3 Roboter bauen sich um und erreichen Schutz-Modus, wobei sie, pro Roboter, 1W4+KF Schaden von Verbündeten in kleiner Reichweite abfangen und selbst erhalten. Wann immer ein Gegner einen Roboter im Schutz-Modus angreift erhält der Gegner 1W4+KF Schaden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Schildwall":
                c = "<b><i>Schildwall</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Roboter funktionieren sich zu einem Wall um, der beliebig eingesetzt werden kann. Gegner können nicht ungewollt an den Robotern passieren, bis sie nicht mindestens 10+KF Schaden genommen haben. Verbündete hinter dem Wall erhalten Deckung und können Ausweichen-Proben um 1 Würfel erleichtert würfeln.\n" +
                    "Dies kann gewirkt werden ohne eine Aktion zu benutzen und die Roboter können beliebig viele Ziele schützen, solange nicht mehr als ein Roboter pro Ziel eingesetzt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Unterstützung Stärke

            case "Schutzanzug":
                c = "<b><i>Schutzanzug</i></b>\n\n" +
                    "Der Held kann Verbündete ebenfalls mit Mecha-Modus mit seinem Roboter rüsten, allerdings können sie weiterhin nur ihre eigenen Fähigkeiten benutzen.";

                //

                break;

            //GRAVITATION   
            //Basisfähigkeiten

            case "Kraftzug":
                c = "<b><i>Kraftzug</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann einen Gegenstand oder eine Person bis zu 100kg an sich heranziehen. Bei schwereren Zielen, zieht er sich heran.\n" +
                    "Diese Fähigkeit darf statt einer Bewegungsaktion gewirkt werden\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Seismischer Schlag":
                c = "<b><i>Seismischer Schlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lädt seine Faust mit Gravitationskraft auf und schlägt das Ziel mit voller Wucht. Hierbei richtet er 2W4+KF Schaden an. Der Schaden wird verdoppeln, wenn das Ziel oder der Held in der Runde durch Gravitation bewegt wurde.\n\n" +
                    "<b>Reichweite:</b>\nKlein";



                break;


            case "Schwerelosigkeit":
                c = "<b><i>Schwerelosigkeit</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held  oder ein willentliches Ziel wird schwerelos und kann für eine Stunde langsam durch die Luft schweben. Dies wird ab dem ersten Bodenkontakt abgebrochen.\n\n" +
                    "<b>Reichweite:</b>\n---";



                break;

            //Stil-frei Stärke

            case "Ereignishorizont":
                c = "<b><i>Ereignishorizont</i></b>\n\n" +
                    "Der KF aller Fähigkeiten wird um 3 erhöht, wenn sie aus kleiner Distanz angewendet werden.";



                break;

            //Kontrolle

            case "Schwergewicht":
                c = "<b><i>Schwergewicht</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann das Gewicht eines Gegenstands für 2+KF Runden verdoppeln, allerdings geht dies nur für Gegenstände, die maximal 500kg schwer sind.\n" +
                    "Lebewesen verlieren stattdessen bei diesem Effekt 5 SL.\n" +
                    "Der Held muss das Ziel für diesen Effekt berühren.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Sogzentrum":
                c = "<b><i>Sogzentrum</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieball zu dem alles in einem mittleren Radius hingezogen wird. Um sich zu befreien müssen, abhängig von der Distanz zum Zentrum, 1-3 3er Würfel KK-Körper-Proben gemacht werden. Zum Ende des Sogs erhalten alle Ziele im Zentrum 2W6+KF Schaden.\n" +
                    "Der Sog hält 5 Runden lang an, kann aber früher beendet werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Bis zur Unendlichkeit":
                c = "<b><i>Bis zur Unendlichkeit</i></b>\n\n" +
                    "Die Reichweite aller Fähigkeiten, die Ziele  bewegen ist um eine Distanzklasse erhöht.";

                //

                break;

            //Zerstörung

            case "Schockstoß":
                c = "<b><i>Schockstoß</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held rammt seine Fäuste in die Richtung des Ziels und schickt einen Schock aus, der allen in einer geraden Linie 3W4+KF Schaden zufügt.\n" +
                    "Bei vollem Schaden werden alle Ziele umgeworfen und für eine Runde betäubt.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Erdbeben":
                c = "<b><i>Erdbeben</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieimpuls in die Erde. Kurze Zeit später bebt die Erde und richtet 3W4+KF Schaden an Allem innerhalb eines großen Radius pro Runde für 3 Runden an. Alles im kleinen Radius zum Helden nimmt keinen Schaden. Alle innerhalb des Bebens haben halbe Bewegungsreichweite.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Zerstörung Stärke

            case "Nachbeben":
                c = "<b><i>Nachbeben</i></b>\n\n" +
                    "Alle Fähigkeiten, die Schaden an nur einem Ziel anrichten, richten zusätzlich halben Schaden an Zielen im kleinen Radius hinter dem Ziel an.";

                //

                break;

            //Risiko

            case "Konterschlag":
                c = "<b><i>Konterschlag</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held nutzt seine Gravitationskräfte, um das Ziel zu beschleunigen, während es ihn angreift.\n" +
                    "Wenn die Fähigkeit gelingt, richtet sie 3W4+KF Schaden an.\n" +
                    "Wenn die Fähigkeit misslingt, richtet der Gegner zusätzlich 1W4 Schaden an.\n" +
                    "Diese Fähigkeit kann nur innerhalb eines gegnerischen Angriffs gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            case "Kometenhieb":
                c = "<b><i>Kometenhieb</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schmettert ein Ziel von sich weg und richtet 3W4+KF Schaden an.\n" +
                    "< 100kg = Ziel wird um eine große Distanz bewegt\n" +
                    "< 200kg = Ziel wird um eine mittlere Distanz bewegt\n" +
                    "> 200kg = Der Held nimmt stattdessen den Schaden und wird eine mittlere Distanz wegbewegt.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Risiko Stärke

            case "Schwerkraft":
                c = "<b><i>Schwerkraft</i></b>\n\n" +
                    "Ziele, die vom Helden angegriffen wurden verlieren einmalig 1W4 SL bis zum Ende des Kampfs, allerdings verliert der Held ebenfalls 2 SL.";

                //

                break;

            //Unterstützung

            case "Beschleunigung":
                c = "<b><i>Beschleunigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nEin Ziel der Wahl erhält +3 SL für 1+KF Runden und kann einen Waffenangriff zusätzlich pro Runde machen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Raumkrümmung":
                c = "<b><i>Raumkrümmung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (KK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held krümmt für eine Runde das Raumgefüge und kann damit dafür sorgen, dass eine Distanz innerhalb des Radiuses für bis zu 5 Runden verdoppelt oder halbiert ist. Er darf die Verhältnisse der Distanz innerhalb der 5 Runden einmal pro Runde ändern.\n" +
                    "Während Raumkrümmung aufrecht erhalten wird, darf der Held weitere Aktionen ausführen.\n\n" +
                    "<b>Reichweite:</b>\nGroßer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Defying Gravity":
                c = "<b><i>Defying Gravity</i></b>\n\n" +
                    "Verbündete, die von einer Fähigkeit des Helden getroffen werden, können für eine Runde die doppelte Distanz an Bewegung zurücklegen.";

                //

                break;


            //MIKROBIOLOGIE
            //Basisfähigkeiten

            case "Muskelschlaffheit":
                c = "<b><i>Muskelschlaffheit</i></b>\n\n" +
                    "<b>Wurf:</b>\n Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt eine Reihe an Pilzsporen gezielt frei, die ihren Weg zum Ziel finden. Die Sporen sorgen dafür, dass die Muskeln des Ziels sich entspannen und es träge wird.\n" +
                    "Ein waches Ziel verliert 1 SL und 1 KK pro Runde und ab der dritten Runde 4 KK. Ein schlafendes Ziel verfällt nach 10 Sekunden in Tiefschlaf für die nächste Stunde und kann nur durch Schmerz, Kälte, Fallgefühl oder Lebensgefahr geweckt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Regenerator":
                c = "<b><i>Regenerator</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erhöht die regenerativen Fähigkeiten des Ziels.\n" +
                    "Das Ziel heilt sich um 2+KF LP pro Runde. Diese Fähigkeit hält 3 Runden lang an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Blutplage":
                c = "<b><i>Blutplage</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDas Ziel wird mit Viren infiziert, die das Ziel innerlich bluten lassen und ihm jede Runde  1W6+KF Schaden zufügen. Dies hält für 6 Runden an.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            //Stil-frei Stärke

            case "Seuchenschleuder":
                c = "<b><i>Seuchen- schleuder</i></b>\n\n" +
                    "Für jede neue Fähigkeit richtet jede vorherige Fähigkeit auf dem selben Ziel 1 Schaden mehr pro Runde an.";



                break;

            //Kontrolle

            case "Schwarzmagenseuche":
                c = "<b><i>Schwarzmagenseuche</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt Bakterien los, die das Ziel infizieren. Die 1. Runde bemerkt das Ziel nichts, doch in der 2. Runde erhält es 1W6+KF Schaden und in der dritten Runde erhält es 6+KF Schaden und übergibt sich, was es für eine Runde betäubt, sofern es nicht widersteht.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er Probe von KK-Körper gelingen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Fieberwelle":
                c = "<b><i>Fieberwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Feinde im Radius erkranken plötzlich an einem Fieber. In der 1. Runde sind alle Proben der Feinde um 1 Würfel erschwert, in der 2. Runde sind alle Proben um 2 Würfel erschwert und in der dritten Runde besteht eine 1/6 Chance, dass das Ziel für 3 Runden erblindet, was seinen WW aller Proben um 3+KF senkt und die Erschwernis beibehält. Falls dies nicht eintritt, bleibt der Effekt von der 2. Runde.\n\n" +
                    "<b>Reichweite:</b>\nGroßer Radius";

                //

                break;

            //Kontrolle Stärke

            case "Steigernde Symptomatik":
                c = "<b><i>Steigernde Symptomatik</i></b>\n\n" +
                    "Schadenslose Zusatzeffekte treten eine Runde früher auf.";

                //

                break;

            //Zerstörung

            case "Ausbreitung":
                c = "<b><i>Ausbreitung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann alle existierenden Fähigkeiten eines Ziels, die er gewirkt hat, auf ein weiteres Ziel übertragen. Sie beginnen beim neuen Ziel, als wären sie frisch gewirkt worden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Nekrotische Seuche":
                c = "<b><i>Nekrotische Seuche</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entfesselt eine fleischfressende Seuche, die bis zu 3 Gegner von innen verrotten lässt. Diese hält für 4+KF Runden an.\n" +
                    "Der Held würfelt in der ersten Runde 2W4+KF. Die Seuche richtet in der ersten Runde das gewürfelte Ergebnis an Schaden an, der Schaden verdoppelt sich allerdings jede Runde. Leblose Körper, die von nekrotischer Seuche befallen sind, lösen sich restlos auf.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Zerstörung Stärke

            case "Ausbruch":
                c = "<b><i>Ausbruch</i></b>\n\n" +
                    "Fähigkeiten des Helden halten nur 3 Runden lang, richten aber in der ersten Runde doppelten Schaden an.";

                //

                break;

            //Risiko

            case "Transfusion":
                c = "<b><i>Transfusion</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held heilt ein Ziel sofort  um 3W6+KF LP, allerdings nimmt der Held die Hälfte der Heilung als Schaden.\n" +
                    "Der Held kann sich nicht selbst mit dieser Fähigkeit heilen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Viraler Schock":
                c = "<b><i>Viraler Schock</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entlädt eine gigantische Welle an Krankheitserregern auf den Gegner, die 4W6+KF auf dem Ziel an Schaden anrichtet. Sollte das Ziel den Viralen Schock jedoch überleben, richten alle mikrobiologischen Fähigkeiten daraufhin nur noch halben Schaden am Ziel an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Risiko Stärke

            case "Übertragbarkeit":
                c = "<b><i>Übertragbarkeit</i></b>\n\n" +
                    "Sollten die Fähigkeiten des Helden ein Ziel töten, besteht eine 5/20 Chance, dass sie auf ein weiteres Ziel im mittleren Radius überspringt. Bei einer 20 ist dieses Ziel ein Verbündeter.";

                //

                break;

            //Unterstützung

            case "Synthetischer Parasit":
                c = "<b><i>Synthetischer Parasit</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann zwei Ziele mit einem verbundenen Organismus belegen, welcher dem ersten Ziel Lebenskraft entzieht und jede Runde 2W4+KF Schaden zufügt und das zweite Ziel um den Schaden am ersten Ziel heilt.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Biotische Gaswolke":
                c = "<b><i>Biotische Gaswolke</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt eine Gaswolke um sich herum erscheinen, die alle Lebewesen innerhalb der Wolke um 2W4+KF pro Runde für 3 Runden heilt.\n" +
                    "Der Held kann bis zu 3 Ziele bestimmen, die nicht geheilt werden sollen.\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Symbiose":
                c = "<b><i>Symbiose</i></b>\n\n" +
                    "Heilungsfähigkeiten des Helden heilen einen weiteren Verbündeten in mittlerer Distanz.";

                //

                break;

            //MUTATION
            //Basisfähigkeiten 

            case "Animalischer Furor":
                c = "<b><i>Animalischer Furor</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann einen weiteren Angriff zusätzlich zu seiner normalen Angriffsaktion durchführen.\n" +
                    "Dies erfordert keine Kampfaktion.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Monsterkralle":
                c = "<b><i>Mutation: Monsterkralle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held transformiert eine seiner Hände zu einer gigantischen Pranke mit messerscharfen Klauen und schlägt einen Feind damit. Er richtet 3W4+KF Schaden an. \n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Monsterkralle” keine VE\n" +
                    "Monsterkralle erhöht ebenfalls die KK um 2\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nKlein";



                break;

            case "Genverzeichnis":
                c = "<b><i>Genverzeichnis</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (Wn)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nWenn der Held einen Feind getötet hat, kann er diesen fressen und erhält einen passiven Vorteil gegen die Spezies. Dadurch erhält er permanent 3+KF weniger Schaden von dieser Spezies.\n" +
                    "Dies ist nicht stapelbar.\n" +
                    "Die Schadensreduktion schließt unnatürlichen Schaden, wie Maschinen und Waffen aus.Der Held kann sich nicht selbst mit dieser Fähigkeit heilen.\n\n" +
                    "<b>Reichweite:</b>\n---";



                break;

            //Stil-frei Stärke

            case "Adaptabilität":
                c = "<b><i>Adaptabilität</i></b>\n\n" +
                    "Der Held kann 3 aktive Mutationen gleichzeitig auf seinem Körper halten.";



                break;

            //Kontrolle

            case "Mutation: Giftzunge":
                c = "<b><i>Mutation: Giftzunge</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert, so dass seine Zunge sich stark verlängert und mit einem Nervengift getränkt ist.\n" +
                    "Er greift das Ziel daraufhin mit einem peitschenden Zungenschlag an. Das Ziel erhält 2W4+KF Schaden. Bei jedem Angriff mit der Giftzunge besteht eine 5/20 Chance, dass das Ziel betäubt wird.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden.\n" +
                    "Solange die Mutation aktiv ist, kostet die nächste 'Giftzunge' keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Monstergebrüll":
                c = "<b><i>Monstergebrüll</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held adaptiert für eine kurze Zeit die Stimmen mehrerer Raubtiere und lässt ein furchteinflößendes Gebrüll erschallen.\n" +
                    "Alle Feinde in Reichweite verlieren 2W6+KF GG und werden vor Furcht betäubt, sofern eine 3er WK-Geist-Probe misslingt.\n" +
                    "Der Held kann das Gebrüll auch nutzen, um in einer furchteinflößenden Monsterstimme bis zu 10 Sekunden lang zu sprechen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Furchteinflößend":
                c = "<b><i>Furchteinflößend</i></b>\n\n" +
                    "Während der Held eine Mutation aktiv hat ist das Einschüchtern von Zielen um 1 Würfel erleichtert.";

                //

                break;

            //Zerstörung

            case "Raubtierinstinkt":
                c = "<b><i>Raubtierinstinkt</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erhält die Jagdinstinkte eines Raubtiers und erhält eine 1W4+KF Chance auf kritische Treffer mit seinen Fähigkeiten gegenüber ein vorher bestimmtes Ziel, sofern Raubtierinstinkt aktiv ist.\n" +
                    "Dies benötigt keine Kampfaktion.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Stachelregen":
                c = "<b><i>Mutation: Stachelregen</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDem Helden wachsen messerscharfe Stacheln aus dem Körper. Diese kann er nach Belieben herausschießen lassen und auf bis zu 3 Ziele niederregnen lassen kann. Jedes Ziel erhält 3W6+KF Schaden.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Stachelregen” keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Zerstörung Stärke

            case "Blutrausch":
                c = "<b><i>Blutrausch</i></b>\n\n" +
                    "Ziele, die den Helden bereits angegriffen haben, nehmen +3 Schaden vom Helden pro Fähigkeit.";

                //

                break;

            //Risiko

            case "Adrenalinpumpe":
                c = "<b><i>Adrenalinpumpe</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held pumpt seinen Körper voll mit Adrenalin. Sein KF steigt um 1W6 und erhält seinen KF an weniger Schaden während Adrenalinpumpe aktiv ist. \n" +
                    "Sobald Adrenalinpumpe nicht länger gewirkt wird, verliert er 2W6 Lebenspunkte.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Mutation: Ultimativer Jäger":
                c = "<b><i>Mutation: Ultimativer Jäger</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert seinen Körper so, dass er den letzten Angriff reproduzieren kann, der ihn in diesem Kampf getroffen hat und verwendet ihn. Dies ist auch bei nicht-natürlichen Angriffen möglich.\n" +
                    "Hierbei richtet der Angriff exakt den gleichen Schaden an, den er am Helden angerichtet hat +KF. Die Fähigkeit übernimmt nur den Schaden und keine Zusatzeffekte.\n" +
                    "Die Mutation selbst hält 2 Runden an, kann aber früher deaktiviert werden. Solange die Mutation aktiv ist, kostet die nächste “Ultimativer Jäger” keine VE\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nAbhängig vom Angriff";

                //

                break;

            //Risiko Stärke

            case "Blut für Blut":
                c = "<b><i>Blut für Blut</i></b>\n\n" +
                    "Der Held kann 5 LP opfern, um seine VE-Kosten für die Runde um 1 zu verringern.";

                //

                break;

            //Unterstützung

            case "Mutation: Plattenpanzer":
                c = "<b><i>Mutation: Plattenpanzer</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held mutiert seinen Körper so, dass sich robuste Panzerplatten an seinem Körper bilden. Dadurch erhält er 7+KF Schild.\n" +
                    "Die Mutation selbst hält 4 Runden an, kann aber früher deaktiviert werden.\n" +
                    "Es können nur 2 Mutationen gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Genvermittlung":
                c = "<b><i>Genvermittlung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann all seine aktiven Mutationen auf einen Verbündeten übertragen, wenn er ihn berührt. Der Verbündete muss dies wollen.\n" +
                    "Ihre Laufzeiten gehen regulär weiter, bis sie ablaufen und alle Fähigkeiten der Mutationen können genutzt werden. Die Mutation selbst kann allerdings nicht durch den Verbündeten erneuert werden.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Unterstützung Stärke

            case "Lebensquell":
                c = "<b><i>Lebensquell</i></b>\n\n" +
                    "Der Held kann mit jeder Mutation, bis zu 10 LP von sich an eine weitere Person übertragen, sofern er sie berührt.";

                //

                break;

            //PHOTONIK
            //Basisfähigkeiten

            case "Aufladung":
                c = "<b><i>Aufladung</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1-3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDDer Held lädt sein Modul mit Energie auf. Für jeden investierten VE-Punkt erhält die nächste Fähigkeit +4 KF. Dies gilt nur für die erste Runde jeder Fähigkeit.\n" +
                    "Aufladen wird nicht an Fähigkeiten verbraucht, die keine KF benutzen, kann aber nur für maximal 5 Runden aufgehoben werden.\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Energiekonvertierung":
                c = "<b><i>Energiekonvertierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held greift ein Ziel mit einer Energiewelle an und richtet 1W4+KF Schaden an. Die nächste Fähigkeit des Helden kostet ihn 1 VE weniger für jeden 2. Schadenspunkt, den Energiekonvertierung angerichtet hat.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            case "Instabiler Quantenball":
                c = "<b><i>Instabiler Quantenball</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt einen Energieball heraus, der bei erstem Impakt explodiert und in einem kleinen Radius 2W4+KF Schaden anrichtet.\n" +
                    "Bei mehr als 14 Schaden wird der Radius verdoppelt.\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            //Stil-frei Stärke

            case "Umleitung":
                c = "<b><i>Umleitung</i></b>\n\n" +
                    "Der Held kann seine Fähigkeiten in seine Waffe umleiten und die Effekte, sowie den halben Schaden der Waffe zusätzlich wirken.Der Schaden zählt nicht als KF.";



                break;

            //Kontrolle

            case "Blitzbetäubung":
                c = "<b><i>Blitzbetäubung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erzeugt einen nicht-tödlichen Energiestrahl, der 1W6+KF Schaden anrichtet und ein Ziel lähmen kann. Das Ziel ist für 1 Runde betäubt, allerdings funktioniert dies nur einmal pro Ziel. Dies ist nur möglich, wenn das Ziel den Helden ansieht.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Statikfeld":
                c = "<b><i>Statikfeld</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erzeugt ein hochenergetisches Feld um sich herum. Alle Gegner in dem Feld erhalten jede Runde 1W6+KF Schaden und verlieren einmalig 1W6 an SL.\n" +
                    "Während das Feld aktiv ist, kann der Held weitere Fähigkeiten wirken.\n" +
                    "“Umleitung” betrifft diese Fähigkeit nicht.\n" +
                    "Bei mehr als 5 SL-Verlust, werden die Ziele die erste Runde betäubt, sofern sie nicht widerstehen können.\n" +
                    "Um zu widerstehen muss eine 3er KK-Körper-Probe gewürfelt werden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //kontrolle Stärke

            case "Blendung":
                c = "<b><i>Blendung</i></b>\n\n" +
                    "Die Fähigkeiten des Helden blenden alle erfassten Ziele zusätzlich für eine Runde, wenn sie mit einem kritischen Erfolg geschafft werden. Aktionen von geblendeten Zielen sind um 2 Würfel erschwert.";

                //

                break;

            //Zerstörung

            case "Detonieren":
                c = "<b><i>Detonieren</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDiese Fähigkeit kann gewirkt werden,ohne eine Aktion zu erfordern, nachdem eine andere Fähigkeit ein Ziel getroffen hat. Um das Ziel herum explodiert die Energie der vorherigen Fähigkeit und richtet in einem kleinen Radius 2W4+KF Schaden an.\n" +
                    "Sollte Detonieren mehr als 12 Schaden anrichten, darf der Spieler.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            case "Todessstrahl":
                c = "<b><i>Todessstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held bündelt einen hochenergetischen Strahl, der Alles in einer Linie trifft und nicht aufgehalten oder reflektiert werden kann. Er richtet 3W6+KF Schaden an.\n" +
                    "Bei mehr als 18 Schaden, wird er in der nächsten Runde weiterhin gewirkt, ohne VE zu kosten. Dieser Effekt tritt nur einmal pro Wurf auf. Der Held kann die Richtung des Strahls nicht ändern.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Die Strahlen kreuzen":
                c = "<b><i>Die Strahlen kreuzen</i></b>\n\n" +
                    "Der Held kann in einer Runde zwei Fähigkeiten gleichzeitig einsetzen, muss aber in der Runde danach aussetzen.";

                //

                break;

            //Risiko

            case "Dematerialisierungskanone":
                c = "<b><i>Dematerialisierungskanone</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt ein Energieprojektil in die Luft, welches in einem hohen Bogen fliegt und in einem kleinen Radius landet. Dort richtet es 2W6+KF Schaden an Allen an. Getötete Lebewesen dematerialisieren sich komplett und stellen 1 VE beim Helden wieder her.\n" +
                    "Bei mehr als 15 Schaden an einem Ziel stellt sich auch ohne Tod eines Lebewesens 1 VE beim Helden wieder her.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            case "Photonenroulette":
                c = "<b><i>Photonenroulette</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (GE)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erstellt einen extrem instabilen Ball an Energie und schießt ihn auf ein Ziel. Der Ball richtet 2W6+KF Schaden an dem Ziel an.\n" +
                    "Der Held würfelt 1W6, bei einer 6 implodiert der Ball und richtet zusätzliche 2W6+KF Schaden an. \n" +
                    "Bei einer 2-5 springt er weiter. Falls es weitere feindliche Ziele in Reichweite gibt, springt er auf sie über und implodiert dort. Falls es keine weiteren Feinde gibt, implodiert er in der Luft.\n" +
                    "Bei einer 1 springt der Ball auf ein Verbündetes Ziel in Reichweite oder den Helden über und implodiert dort.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke

            case "Instabile Kraft":
                c = "<b><i>Instabile Kraft</i></b>\n\n" +
                    "Die kritische Erfolgschance und die kritische Misserfolgs- chance aller Fähigkeiten des Helden sind um 1 erhöht.";

                //

                break;

            //Unterstützung

            case "Rückstoßschild":
                c = "<b><i>Rückstoßschild</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schießt eine Energieladung auf einen Verbündeten. Das Schild des Verbündeten wird mit Energie aufgeladen und erhält 1W6+KF Schild für 3 Runden. Jeder Nahkampfangriff auf den Verbündeten, der das Schild trifft, während es verstärkt ist, schickt einen Kraftimpuls durch den Feind, der 1W4+KF Schaden anrichtet und ihn leicht zurückschleudert. \n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Schildstrahl":
                c = "<b><i>Schildstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held stellt einen beschädigten/ zerstörten Schild um 2W6+KF Punkte wieder her.\n" +
                    "Falls dabei Punkte übrig bleiben sollten, stellt Schildstrahl 1VE beim Ziel für eine Runde wieder her.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Unterstützung Stärke

            case "Starthilfe":
                c = "<b><i>Starthilfe</i></b>\n\n" +
                    "Fähigkeiten, die auf Verbündete gewirkt werden, stellen 1 VE bei dem Verbündeten wieder her. (Dies gilt nur einmal pro Verbündeter)";

                //

                break;

            //HOLOTECH
            //Basisfähigkeiten

            case "Hologramm: Holo-Kopie":
                c = "<b><i>Hologramm: Holo-Kopie</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE alle 3 Runden\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erstellt eine holographische Kopie von einem Gegenstand oder Lebewesen seiner Wahl, die Gewichtsbelastung von bis zu 1+KF kg und 5+KF Schadenspunkte aushält und sich maximal eine riesige Reichweite weit von dem Helden entfernen kann. Die Kopie ist nur optisch und haptisch wahrnehmbar.\n" +
                    "Der Held kann die Kopie allein per Gedanken befehligen, solange er sie sehen kann.\n" +
                    "Der Held kann bis zu 3 Hologramme gleichzeitig aktiv halten.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Hologramm: Augmentierung":
                c = "<b><i>Hologramm: Augmentierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held umhüllt eine Waffe mit energiegeladenen Projektionen und erhöht ihren Schaden um 2W4+KF für 4 Runden.\n" +
                    "Dies nimmt keine Aktion in Anspruch\n\n" +
                    "<b>Reichweite:</b>\nGroß";



                break;

            case "Hologramm: Reflektorwand":
                c = "<b><i>Hologramm: Reflektorwand</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held baut eine massive Wand aus Licht auf, die eine mittlere Reichweite lang ist. Diese Wand hält für eine Runde und hält Projektile auf, die 8+KF Schaden anrichten würden. Jedes Projektil, dass über diese Grenze geht, durchdringt die Wand, jedes Projektil, dass die Grenze nicht überschreitet, wird zurück auf den Schützen geschossen und richtet seinen Schaden +KF des Helden an.\n" +
                    "Der Held kann wählen Projektile nicht abzufangen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //Stil-frei Stärke

            case "Intelligentes Design":
                c = "<b><i>Intelligentes Design</i></b>\n\n" +
                    "Alle Hologramme können auch agieren, wenn der Held sie nicht sehen kann, sofern sie nicht weiter als eine riesige Distanz von ihm entfernt sind.";



                break;

            //Kontrolle

            case "Verzerrtes Bild":
                c = "<b><i>Verzerrtes Bild</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held setzt ein Hologramm nahezu exakt an die Stelle eines Ziels. Die Körper der Halluzination überschneiden sich, bewegen sich aber leicht anders, als das Ziel selbst. Dies macht es schwieriger das Ziel zu treffen und erschwert alle Angriffe auf das Ziel so, dass ohne AW gewürfelt werden muss.\n" +
                    "Wann immer der Feind das Ziel verfehlt, greift das verzerrte Bild den Feind an und fügt ihm 1W6+KF Schaden zu. Dies hält 3 Runden lang an. Es kann nur ein “Verzerrtes Bild” gleichzeitig aktiv sein.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Reflektionsmaximum":
                c = "<b><i>Reflektionsmaximum</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held verändert die Lichtreflektion in einem mittleren Radius  so, dass gewünschte Objekte innerhalb entweder komplett unsichtbar werden oder der Ort zu einem lichtleeren Raum wird. Ziele innerhalb des unsichtbaren Raums können um 3 Würfel erleichtert ausweichen und sie zu bemerken, wenn sie sich nicht erkenntlich geben, ist um 3 Würfel erschwert.\n" +
                    "In den Runden nach der ersten Wirkung von Reflektionsmaximum können weitere Aktionen gewirkt werden, während es aufrecht erhalten wird.\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Kontrolle Stärke

            case "Sensorisches Erlebnis":
                c = "<b><i>Sensorisches Erlebnis</i></b>\n\n" +
                    "Alle Illusionen beinhalten auch Geräusche, sofern der Held das nachgebildete Geräusch bereits gehört hat.";

                //

                break;

            //Zerstörung

            case "Hologramm: Lichtwaffe":
                c = "<b><i>Hologramm: Lichtwaffe</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held erschafft eine holographische Kopie seiner Waffe, die im Raum schwebt und jede Runde  Waffenschaden+KF Schaden anrichtet. Der Held kann die Waffe gedanklich befehligen, die Waffe kann sich nicht weiter als eine große Distanz vom Ziel wegbewegen.\n" +
                    "Die Waffe kann nicht zerstört werden und keine Fähigkeiten, die Waffen betreffen betreffen die Lichtwaffe, mit Ausnahme von “Augmentierung”.\n" +
                    "Die Waffe hält 4 Runden lang, der Held kann maximal 3 Lichtwaffen gleichzeitig aktiv haben.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Lichtspaltung":
                c = "<b><i>Lichtspaltung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n4 VE\n\n" +
                    "<b>Beschreibung:</b>\nAlle Hologramme des Helden explodieren und richten 4W4+KF Schaden pro Hologramm in einem kleinen Radius an.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Durchdringung":
                c = "<b><i>Durchdringung</i></b>\n\n" +
                    "Alle Fähigkeiten ignorieren 4 Punkte gegnerischer Schilde.";

                //

                break;

            //Risiko

            case "Lichtschlucker":
                c = "<b><i>Lichtschlucker</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann seine gesamte Schildkapazität opfern, um für eine Runde die Hälfte seines Schilds als KF zu erhalten. \n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Lichtkaskade":
                c = "<b><i>Lichtkaskade</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held schickt drei Lichtwellen mit mittlerem Durchmesser vor sich aus, die jeweils 1W6+KF Schaden an allen Zielen anrichtet. Sollte allerdings eine 6 gewürfelt werden, wird die Welle reflektiert, sobald sie ihr Ende erreicht hat und wird erneut in Richtung des Helden gewirkt, wobei sie ebenfalls den Helden trifft.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke

            case "Schildkonverter":
                c = "<b><i>Schildkonverter</i></b>\n\n" +
                    "Der Held erhält 1 KF für jeden 3. aktuellen Schildpunkt, wann immer er sein Schild deaktiviert.";

                //

                break;

            //Unterstützung

            case "Hologramm: Holographische Textur":
                c = "<b><i>Hologramm: Holographische Textur</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Stunde\n\n" +
                    "<b>Beschreibung:</b>\nDer Held legt eine holographische Schicht über ein Objekt oder ein Lebewesen und kann dessen Aussehen leicht verändern. Die generelle Form des Objekts bleibt gleich, aber einzelne Details, wie Gesichter oder Rillen können anders dargestellt werden. Die Textur ist rein optisch.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Holographische Kuppel":
                c = "<b><i>Holographische Kuppel</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>Der Held erschafft eine Kuppel aus Licht, die Schaden absorbiert. Die Kuppel hält 14+KF Schaden aus und umgibt ihren gesamten Bereich. Der Held kann die Kuppel deaktivieren, um allen außerhalb der Kuppel in einem Ring den Schaden anzurichten, der abgefangen wurde.\n" +
                    "Schadensquellen innerhalb der Kuppel werden ignoriert.\n" +
                    "Der Held kann sich bewegen, während er die Kuppel aktiv hält und kann weitere Aktionen tätigen (außer in der Runde der Aktivierung und der Runde der Deaktivierung)\n\n" +
                    "<b>Reichweite:</b>\nMittlerer Radius";

                //

                break;

            //Unterstützung Stärke

            case "Kompatibel":
                c = "<b><i>Kompatibel</i></b>\n\n" +
                    "Der Held darf einem Verbündeten in großer Reichweite zu Beginn der Runde einmal pro Runde 2 Schildpunkte geben, die eine Runde lang halten.";

                //

                break;

            //PSYONIK
            //Basisfähigkeiten

            case "Glücksstrahl":
                c = "<b><i>Glücksstrahl</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held bestrahlt ein Ziel mit einem Glücksstrahler, der für alle sichtbar ist. Sollte das Ziel nicht widerstehen können, wird das Ziel wird beruhigt und dem Helden gegenüber gutgestimmt. Sofern es nicht widersteht, erinnert sich nach 30min jedoch an die Bestrahlung und verliert den Effekt.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Gedankenschinden":
                c = "<b><i>Gedankenschinden</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held greift die Psyche des Gegners direkt an, wodurch dieser massive Kopfschmerzen bekommt und richtet 3W4 + KF Schaden und 2W4 GG-Schaden an. \n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            case "Psychokinese":
                c = "<b><i>Psychokinese</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann ein Objekt oder ein Lebewesen unter 50 x KF kg mit Hilfe seiner Gedankenkraft  eine kleine Distanz weit bewegen. Sollte das Lebewesen geworfen werden, richtet ein gewöhnlicher Aufprall 3W4+KF Schaden an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";



                break;

            //Stil-frei Stärke

            case "Geistige Beherrschung":
                c = "<b><i>Geistige Beherrschung</i></b>\n\n" +
                    "Der Held kann bei Fähigkeiten Stress auswürfeln, nachdem der ursprüngliche  Wurf bereits gefallen ist, allerdings muss er hierfür 2 GG zusätzlich verbrauchen.";



                break;

            //Kontrolle

            case "Innere Schockwelle":
                c = "<b><i>Innere Schockwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held sendet eine mentale Schockwelle aus, die allen Feinden 2W4+KF Schaden anrichtet und sie für 1 Runde festhält und an Bewegungsaktionen hindert, sofern sie nicht widerstehen.\n" +
                    "Um zu widerstehen muss dem Ziel eine 3er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Geistesergriff":
                c = "<b><i>Geistesergriff</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held fesselt die Gedanken seines Gegners und zwingt sie an etwas zu denken, was der Held vorher festlegen kann. Der Feind ist für eine Runde betäubt außer, wenn er alles ausspricht, was ihm zu dem Gedanken in den Kopf kommt.\n" +
                    "Um zu widerstehen muss dem Ziel eine 5er WK-Geist-Probe gelingen.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Kontrolle Stärke

            case "Unbrechbarer Wille":
                c = "<b><i>Unbrechbarer Wille</i></b>\n\n" +
                    "Der Held kann seine Kontroll-Effekte um eine Runde verlängern, wenn ihm eine AW-lose 5er WK-Probe gelingt.";

                //

                break;

            //Zerstörung

            case "Psychischer Schrei":
                c = "<b><i>Psychischer Schrei</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held entfesselt einen markerschütternden Schrei innerhalb des Geistes aller Feinde und richtet dabei  2W4 Schaden und 2W4+KF GG-Schaden an allen an.\n" +
                    "Sollte der zugefügte GG-Schaden höher sein, als der normale Schaden, darf der Held 1W4+KF Schaden zusätzlich verursachen.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Neuronaler Zusammenbruch":
                c = "<b><i>Neuronaler Zusammenbruch</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held lässt das Gehirn des Gegners zusammenbrechen und richtet dabei 4W4+KF Schaden und GG-Schaden an. Sollte der Feind vor dem Angriff unter der Hälfte seiner GG sein, ist der Schaden verdoppelt.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Zerstörung Stärke

            case "Gebrochener Geist":
                c = "<b><i>Gebrochener Geist</i></b>\n\n" +
                    "Für jeden 4. Schadenspunkt der Fähigkeiten des Helden verliert das Ziel 1 GG.";

                //

                break;


            //Risiko

            case "Erinnerungsfresser":
                c = "<b><i>Erinnerungsfresser</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held frisst sich durch die Gedanken seines Ziels und richtet 1W10+KF Schaden an. Abhängig vom Wurf geschieht folgender Zusatzeffekt.\n" +
                    "1: Der Held vergisst eine zufällige Fähigkeit bis zum Ende des Tages\n2 - 5: Der Held und das Ziel verlieren beide 2W6 GG\n6 - 9: Das Ziel vergisst eine Erinnerung, die der Held lesen kann\n10: Das Ziel vergisst eine Erinnerung, die der Held lesen kann und das Ziel verliert 3W6 GG.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Strahlender Wahnsinn":
                c = "<b><i>Strahlender Wahnsinn</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDDer Held verknüpft den Geist seines Ziels mit seinem eigenen Geist und füllt diesen mit Visionen des Wahnsinns. Dabei fügt er ihm und sich selbst 3W6+KF GG-Schaden zu.\n" +
                    "Sollte der Held bereits dabei unter die Hälfte seiner GG erreichen, richtet die Fähigkeit den doppelten GG Schaden als physischen Schaden am Gegner an.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Risiko Stärke 

            case "Dem Wahnsinn verfallen":
                c = "<b><i>Dem Wahnsinn verfallen</i></b>\n\n" +
                    "Der Held kann 10 GG opfern um seine VE-Kosten für die Runde um 1 zu verringern.";

                //

                break;

            //Unterstützung

            case "Ruhiger Geist":
                c = "<b><i>Ruhiger Geist</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nRuhiger Geist\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Körper und Geist":
                c = "<b><i>Körper und Geist</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (WK)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held nimmt einem Verbündeten jeglichen Schmerz und sorgt dafür, dass er so viele Lebenspunkte wie GG hat (sofern er mehr GG als LP besitzt). Nach Ablaufen der Fähigkeit verliert der Verbündete die Hälfte der Lebenspunkte wieder.\n" +
                    "Diese Fähigkeit kann als Reaktion im gegnerischen Zug gewirkt werden.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Unterstützung Stärke

            case "Neurologische Stimulierung":
                c = "<b><i>Neurologische Stimulierung</i></b>\n\n" +
                    "Am Ende des Kampfs stellt der Held 1W6GG bei sich und allen Verbündeten wieder her.";

                //

                break;

            //THERMOCHEMIE
            //Basisfähigkeiten

            case "Thermische Einstellung":
                c = "<b><i>Thermische Einstellung</i></b>\n\n" +
                    "<b>Wurf:</b>\n---\n\n" +
                    "<b>Kosten:</b>\n---\n\n" +
                    "<b>Beschreibung:</b>\nDer Held stellt sein Modul auf Hitze oder Kälte ein. Dies modifiziert alle weiteren Fähigkeiten, die mit “Hitze” oder “Kälte” versehen wurden.\n" +
                    "Die Einstellung erfordert keine Aktion\n\n" +
                    "<b>Reichweite:</b>\n---";

                //

                break;

            case "Thermischer Schock":
                c = "<b><i>Thermischer Schock</i></b>\n\n" +
                    "<b>Wurf:</b>\n1 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE\n\n" +
                    "<b>Beschreibung:</b>Der Held kann Hitze oder Kälte der Umgebung oder eines Objekts umwandeln und in einem Angriff freisetzen. Die Umgebung oder das Objekt verliert dabei ein wenig an Hitze/ Kälte.\n" +
                    "Dampfschock: (Hitze) Kann als Reaktion im gegnerischen Zug gewirkt werden und richtet 2W4+KF Schaden an.\n" +
                    "Frostschock: (Kälte)  Richtet 3W4+KF Schaden an.\n\n" +
                    "<b>Reichweite:</b>\nGroße Reichweite";



                break;

            case "Wetterumschwung":
                c = "<b><i>Wetterumschwung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n3 VE\n\n" +
                    "<b>Beschreibung:</b>Das Wetter der Gegend kann, abhängig von der thermischen Einstellung und den existierenden Bedingungen vorübergehend verändert werden. Dafür wird eine Atmosphäre benötigt. Das Wetter hält 5 + KF Runden an.\n" +
                    "<b>Sonnig + <i>Kälte</i></b> = Bewölkt mit Chance auf Regen\n" +
                    "<b>Sonnig + <i>Hitze</i></b> = Dürre (alle Einheiten haben -2 KK, +2 KF für Hitze)\n" +
                    "<b>Dürre + <i>Kälte</i></b> = Sonnig\n" +
                    "<b>Bewölkt + <i>Hitze</i></b> = Gewitter (1/20 Chance pro Ziel 3W4 Schaden zu erhalten)\n" +
                    "<b>Bewölkt + <i>Kälte</i></b> = Schnee\n" +
                    "<b>Gewitter + <i>Kälte</i></b> = Schneesturm  (Alle Einheiten verlieren 3 Lebenspunkte pro Runde. +2 KF für Kälte)\n" +
                    "<b>Schnee + <i>Hitze</i></b> = Regenschauer\n" +
                    "<b>Schneesturm + <i>Hitze</i></b> = Gewitter\n\n" +
                    "<b>Reichweite:</b>\nRiesiger Radius";



                break;

            //Stil-frei Stärke

            case "Sturmwarnung":
                c = "<b><i>Sturmwarnung</i></b>\n\n" +
                    "Alle Wetterzustände halten 2 Runden länger an.";



                break;

            //Kontrolle

            case "Verfestigung":
                c = "<b><i>Verfestigung</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held kann den Aggregatzustand eines flüssigen oder gasförmigen Objekts ändern und es verfestigen, solange er es berührt. Das Objekt wird für eine Stunde verfestigt und nimmt danach seine vorherige Form an.\n" +
                    "Dies funktioniert nicht für Lebewesen.\n" +
                    "Der Held kann dies nicht für Objekte nutzen, die eine größere Masse besitzen, als er. \n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;


            case "Eisige Adern/ Brodelndes Blut":
                c = "<b><i>Eisige Adern/ Brodelndes Blut</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisige Adern: <i>(Kälte)</i></b> Der Held durchflutet den Körper des Ziels mit Kälte, wobei es 3W6 Schaden nimmt und für 3+KF Runden 4 SL verliert. Dies ist nicht stapelbar.\n" +
                    "<b>Brodelndes Blut: <i>(Hitze)</i></b> Der Held durchflutet den Körper des Ziels mit Hitze, wobei es 3W6 +KF Schaden nimmt und für eine Runde bewegungsunfähig wird, wenn es keine 3er Körper-Probe schafft.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            //Kontrolle Stärke

            case "Gefrierbrand":
                c = "<b><i>Gefrierbrand</i></b>\n\n" +
                    "Alle Fähigkeiten mit SL-Verringerungen oder Betäubung erhalten +2 KF";

                //

                break;

            //Zerstörung

            case "Froststrahl/Feuerwelle":
                c = "<b><i>Froststrahl/Feuerwelle</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Froststrahl: <i>(Kälte)</i></b> Der Held greift das Ziel mit einem eisigen Projektil an und richtet 3W4+KF Schaden an. Ziele die in den letzten 3 Runden von Kälte-Fähigkeiten getroffen wurden, erhalten doppelten Schaden.\n" +
                    "<b>Feuerwelle: <i>(Hitze)</i></b> Der Held beschießt bis zu 3 Ziele mit eine Welle an konzentrierter Hitze, die 2W6+KF Schaden pro Ziel anrichten. Sollte das Ziel 6 oder mehr Schaden an einem Ziel verursachen, nimmt das Ziel die nächsten 3 Runden 1W6 Brandschaden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Blizzard/Sengende Luft":
                c = "<b><i>Blizzard/Sengende Luft</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n1 VE pro Runde\n\n" +
                    "<b>Beschreibung:</b>\n<b>Blizzard: <i>(Kälte)</i></b>  Der Held erlangt Meisterschaft über das Wetter und beschwört einen Blizzard herauf. Dieser richtet 2W6+KF Schaden an allen Zielen innerhalb des Radiuses pro Runde an und verlangsamt sie um 3 SL.  Alle Ziele innerhalb eines kleinen Radiuses vom Helden aus, sind nicht vom Blizzard betroffen. Blizzard zählt als Schneesturm und übernimmt seine Eigenschaften, aber nicht den Schaden.\n" +
                    "<b>Sengende Luft: <i>(Hitze)</i></b> Der Held erlangt Meisterschaft über das Wetter und beschwört eine massive Hitzewelle herauf. Diese richtet 2W6+KF Schaden an allen Zielen innerhalb des Radiuses an. Alle Ziele, die 10 oder mehr Schaden erhalten, nehmen zusätzlich 1W4 Brandschaden pro Runde für 3 Runden. Brennende Luft zählt als Dürre und übernimmt ihre Eigenschaften.\n" +
                    "Während Blizzard/ Sengende Luft wirkt, kann der Held weitere Fähigkeiten wirken.\n\n" +
                    "<b>Reichweite:</b>\nRiesig";

                //

                break;

            //Zerstörung Stärke

            case "Inferno":
                c = "<b><i>Inferno</i></b>\n\n" +
                    "Jede dritte Hitze-Fähigkeit im Kampf richtet doppelten Schaden an.";

                //

                break;

            //Risiko

            case "Eisspeere/Feuersalve":
                c = "<b><i>Eisspeere/Feuersalve</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisspeere: <i>(Kälte)</i></b> Der Held schießt 1W4 Eisspeere, die jeweils 1W4+KF Schaden anrichten, auf bis zu 1W4 Ziele (die selbe Anzahl wie Eisspeere). Jeder Eisspeer richtet +2 Schaden für jeden weiteren Eisspeer der das Ziel in der Runde trifft an.\n" +
                    "<b>Feuersalve: <i>(Hitze)</i></b> Der Held schießt 5 flammende Projektile, die zufällig auf alle Ziele in Reichweite verteilt werden und 1W4+KF Schaden anrichten. Verbündete Ziele erhalten nur den halben Schaden.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            case "Lebende Bombe":
                c = "<b><i>Lebende Bombe</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n4 VE\n\n" +
                    "<b>Beschreibung:</b>\nDer Held belegt das Ziel für 2 Runden mit einer Welle an thermischer Energie, die herausbricht, wenn das Ziel in einer Runde mehr als 10-KF Schaden genommen hat. Daraufhin explodiert die Energie aus dem Ziel heraus und richtet 3W6 Schaden an.\n" +
                    "<b><i>Kälte:</i></b> Die Explosion hat eine 10/20 Chance pro Ziel zu betäuben und es festzufrieren.\n" +
                    "<b><i>Hitze:</i></b> Die Explosion verteilt Lebende Bombe auf ein weiteres Ziel der Wahl in der Nähe.\n\n" +
                    "<b>Reichweite:</b>\nGroß";

                //

                break;

            //Risiko Stärke 

            case "Wechselwirkung":
                c = "<b><i>Wechselwirkung</i></b>\n\n" +
                    "Wann immer der Held einen Fähigkeitstypen (Hitze/Kälte) wiederholt, kostet die zweite Fähigkeit 1 VE mehr. Wann immer er einen Typ nicht wiederholt, kostet die 2. Fähigkeit 1 VE weniger.";

                //

                break;

            //Unterstützung

            case "Eisige Berührung/ Feuerteufel":
                c = "<b><i>Eisige Berührung/ Feuerteufel</i></b>\n\n" +
                    "<b>Wurf:</b>\n2 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisige Berührung: <i>(Kälte)</i></b> Der Held verstärkt eine Waffe  mit thermischer Kälteenergie, woraufhin die Waffe für 1+KF Runden lang mit jedem Waffenangriff eine 1/6 Chance hat ein Ziel zu betäuben.\n" +
                    "<b>Feuerteufel: <i>(Hitze)</i></b> Der Held verstärkt eine Waffe mit thermischer Hitzeenergie, woraufhin die Waffe für 1+KF Runden lang mit jedem Waffenangriff eine 1W6 Chance hat zu überhitzen und einen weiteren Angriff abzufeuern.\n\n" +
                    "<b>Reichweite:</b>\nMittel";

                //

                break;

            case "Eisstruktur/ Kauterisierung":
                c = "<b><i>Eisstruktur/ Kauterisierung</i></b>\n\n" +
                    "<b>Wurf:</b>\n3 Würfel (IN)\n\n" +
                    "<b>Kosten:</b>\n2 VE\n\n" +
                    "<b>Beschreibung:</b>\n<b>Eisstruktur: <i>(Kälte)</i></b> Der Held erzeugt eine gefrorene Struktur, die bis zu 5 Meter groß ist und 10 Runden lang hält, bis sie zerschmilzt. Sie kann bis zu 200kg und 20 Schadenspunkte aushalten. Je komplizierter die Form ist, desto länger benötigt das Wirken dieser Fähigkeit. Dies benötigt Flüssigkeit.\n" +
                    "<b>Kauterisierung: <i>(Hitze)</i></b> Der Held kauterisiert eine Wunde und heilt ein Ziel um 4W4+KF Lebenspunkte. Der Ziel nimmt daraufhin für 2 Runden 2W6 Schaden pro Runde.\n\n" +
                    "<b>Reichweite:</b>\nKlein";

                //

                break;

            //Unterstützung Stärke

            case "Windschatten":
                c = "<b><i>Windschatten</i></b>\n\n" +
                    "Wetter-Fähigkeiten erhöhen die SL aller Gruppenmitglieder um 2 für eine Runde.(Dies zählt nicht für wetterberuhigende Fähigkeiten)";

                //

                break;
        }

        return c;
    }
}


