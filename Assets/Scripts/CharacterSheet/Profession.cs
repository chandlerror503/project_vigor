﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Profession : MonoBehaviour
{

    public Text profS1;
    public Text profS2;
    public Text profS3;
    public Text profS4;

    public Text aus1;
    public Text aus2;

    public Text info;

    public int toggleCount;
    public Toggle[] togs;

    public Button firstEnabled;


    void Start()
    {
        togs = new Toggle[3];
        togs = GetComponentsInChildren<Toggle>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (FindObjectOfType<CharacterSheet>().profession)
        {
            case "Militär":

                profS1.text = "Coup de Grace";
                profS2.text = "Respektable Erscheinung";
                profS3.text = "Adrenalin";
                profS4.text = "Steuermann";

                aus1.text = "Entdeckungsausstattung";
                aus2.text = "Kriegsausstattung";

                info.text = "<b><i>Coup de Grace:</i></b>\nWaffenangriffe richten +3 Schaden an, wenn der Gegner betäubt ist.\n\n\n" +
                    "<b><i>Respektable Erscheinung:</i></b>\nTäuschungen des Helden festzustellen ist dem Gegner um 1 Würfel erschwert.\n\n\n" +
                    "<b><i>Adrenalin:</i></b>\nDer AW von Körper ist um 2 erhöht wann immer der Held unter der Hälfte seines Lebens besitzt.\n\n\n" +
                    "<b><i>Steuermann:</i></b>\nDas Steuern von Raumschiffen in Weltraumschlachten ist um 1 Würfel erleichtert.\n";

                break;

            case "Spionage":

                profS1.text = "Assassine";
                profS2.text = "Tausend Gesichter";
                profS3.text = "Kühler Kopf";
                profS4.text = "Geheimcode";

                aus1.text = "Entdeckungsausstattung";
                aus2.text = "Trickausstattung";

                info.text = "<b><i>Assassine:</i></b>\nDie kritische Erfolgschance von Angriffen ist um 2 erhöht, wenn der Held unentdeckt ist.\n\n\n" +
                    "<b><i>Tausend Gesichter:</i></b>\nWann immer der Held sich erfolgreich für eine andere Person ausgibt, sind Geist-Proben um 1 Würfel erleichtert.\n\n\n" +
                    "<b><i>Kühler Kopf:</i></b>\nFinesse-Proben sind nicht von Erschwernissen durch fehlende GG betroffen.\n\n\n" +
                    "<b><i>Geheimcode:</i></b>\nDer Held beherrscht eine hochkomplexe Geheimsprache, die nur unter Spionen bekannt ist.\n";

                break;

            case "Wissenschaft":

                profS1.text = "Waffentest";
                profS2.text = "Psychoanalytisch";
                profS3.text = "Verrücktes Genie";
                profS4.text = "Materialverständnis";

                aus1.text = "Forschungsausstattung";
                aus2.text = "Raumfahrtausstattung";

                info.text = "<b><i>Waffentest:</i></b>\nDer Held richtet +2 Schaden an Zielen an, dessen Spezies er schon im Labor analysiert hat.\n\n\n" +
                   "<b><i>Psychoanalytisch:</i></b>\nDas Einschätzen Anderer ist um 1 Würfel erleichtert.\n\n\n" +
                   "<b><i>Verrücktes Genie:</i></b>\nDer AW von Verstand ist um 2 erhöht wann immer er unter der Hälfte seiner GG besitzt.\n\n\n" +
                   "<b><i>Materialverständnis:</i></b>\nDer Held kann einmal pro Herstellung einen Wert eines Materials um 1 erhöhen.\n";

                break;

            case "Medizin":

                profS1.text = "Anatomie";
                profS2.text = "Therapeut";
                profS3.text = "Notarzt";
                profS4.text = "Ärztlicher Eid";

                aus1.text = "Forschungsausstattung";
                aus2.text = "Arztausstattung";

                info.text = "<b><i>Anatomie:</i></b>\nDer Held kann Verstand WN-Proben benutzen um eine körperliche Schwachstelle bei seinem Feind zu erkennen.\n\n\n" +
                   "<b><i>Therapeut:</i></b>\nDer Held kann Zielen mit Geist-EM Proben 1W6 GG pro Tag wiederherstellen.\n\n\n" +
                   "<b><i>Notarzt:</i></b>\nSobald ein Verbündeter unter 10 LP besitzt, darf der Held die Zugreihenfolge ignorieren und eine Aktion machen.\n\n\n" +
                   "<b><i>Ärztlicher Eid:</i></b>\nDer Held regeneriert 2W6 GG, wenn er einen verwundeten Feind versorgt.\n";

                break;

            case "Mechanik":

                profS1.text = "MacVigor";
                profS2.text = "Computersprache";
                profS3.text = "Verstecktes Potenzial";
                profS4.text = "Ausschlachtung";

                aus1.text = "Ingeneursausstattung";
                aus2.text = "Raumfahrtausstattung";

                info.text = "<b><i>MacVigor:</i></b>\nDer Held kann eine Waffe mit 0 SS in eine Bombe umwandeln, die nächste Runde 3W6 Schaden in einem kleinen Radius anrichtet.\n\n\n" +
                  "<b><i>Computersprache:</i></b>\nDer Held kann Geist-Proben für die Sabotage von mechanischen Lebewesen verwenden.\n\n\n" +
                  "<b><i>Verstecktes Potenzial:</i></b>\nKritische Erfolge bei der Herstellung von Modulchips des Helden erhöhen, bei Bedarf die Modulstufe um 1.\n\n\n" +
                  "<b><i>Schiffverständnis:</i></b>\nDer Held kann mit einer IN-Technik-Probe zu Beginn des Kampfs ermitteln, was für Waffen ein gegnerisches Raumschiff besitzt.\n";

                break;

            case "Vigorismus":

                profS1.text = "Energiesparmodus";
                profS2.text = "Hoffnungsfunke";
                profS3.text = "Kristallgespür";
                profS4.text = "Notumleitung";

                aus1.text = "Ingeneursausstattung";
                aus2.text = "Kriegsausstattung";

                info.text = "<b><i>Energiesparmodus:</i></b>\nDie erste Fähigkeit des Tages kostet 1 VE weniger.\n\n\n" +
                 "<b><i>Hoffnungsfunke:</i></b>\nDer Held hat um 2 erhöhte AW abhängig von der Vigoritart seines Moduls, wenn er keine VE hat.\nGrün, Blau: Verstand\nGelb, Rot: Geist\n\n\n" +
                 "<b><i>Kristallgespür:</i></b>\nDer Held kann durch Technik-WN-Proben erkennen, dass Maschinen Vigorit verwenden und kann bestimmen um welche Art es sich handelt.\n\n\n" +
                 "<b><i>Notumleitung:</i></b>\nDer Held kann mit einer IN-Technik-Probe kostenlos das Raumschiff-Schild für die Runde verdoppeln. Dafür darf das Schiff sich diese Runde nicht bewegen.\n";

                break;

            case "Diplomatie":

                profS1.text = "Scharfe Worte";
                profS2.text = "Erster Eindruck";
                profS3.text = "Gehobene Gesellschaft";
                profS4.text = "Politischer Einfluss";

                aus1.text = "Trickausstattung";
                aus2.text = "Zivilausstattung";

                info.text = "<b><i>Scharfe Worte:</i></b>\nDas Verspotten von Gegnern kann ihre  Angriffe um 1 Würfel erschweren.\n\n\n" +
                "<b><i>Erster Eindruck:</i></b>\nDie kritische Trefferchance bei Geist-Proben im ersten Gespräch mit Personen ist um 2 erhöht.\n\n\n" +
                "<b><i>Gehobene Gesellschaft:</i></b>\nDer Held erhält +2W4 XP für die Erfüllung von Aufträgen aus höheren sozialen Schichten.\n\n\n" +
                "<b><i>Politischer Einfluss:</i></b>\nDer Held hat einflussreiche Freunde in der Regierung, die ihm einen Gefallen schulden.\n";

                break;

            case "Religion":

                profS1.text = "Vom Glück gesegnet";
                profS2.text = "Vertrauensperson";
                profS3.text = "Inspirierende Präsenz";
                profS4.text = "Glaubenssprung";

                aus1.text = "Zivilausstattung";
                aus2.text = "Arztausstattung";

                info.text = "<b><i>Vom Glück gesegnet:</i></b>\nStresswürfe dürfen in Kampfsituationen  auch kritische Misserfolge ersetzen.\n\n\n" +
                "<b><i>Vertrauensperson:</i></b>\nDer AW von Geist-Proben ist um 3 erhöht, wenn der Held mit Personen aus unteren Schichten interagiert.\n\n\n" +
                "<b><i>Inspirierende Präsenz:</i></b>\nVom Helden rekrutierte Crewmitglieder starten einmalig mit 1W4 Moral mehr.\n\n\n" +
                "<b><i>Glaubenssprung:</i></b>\nDer Mitspieler kann seine eigene GG verwenden, um Spielern Stresswürfe zu ermöglichen.\n";

                break;
        }



        if (toggleCount >= 2)
        {
            for (int i = 0; i <= 3; i++)
            {
                if (!togs[i].isOn)
                {
                    togs[i].interactable = false;
                }
            }
        }
        else
        {
            for (int i = 0; i <= 3; i++)
            {
                if (!togs[i].isOn)
                {
                    togs[i].interactable = true;
                }
            }
        }

    }
}
