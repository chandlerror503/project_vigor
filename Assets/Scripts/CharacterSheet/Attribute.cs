﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Attribute : MonoBehaviour
{
    public Text kk;
    public int kk_int;
    public Text ge;
    public int ge_int;
    public Text wn;
    public int wn_int;
    public Text em;
    public int em_int;
    public Text in_;
    public int in_int;
    public Text wk;
    public int wk_int;

    public Text k;
    public int k_int;
    public Text g;
    public int g_int;
    public Text v;
    public int v_int;
    public Text t;
    public int t_int;
    public Text f;
    public int f_int;

    public Text maxAW;
    public Text maxWW;
    public int awAktuell;
    public int wwAktuell;

    public Text lp;
    public Text gg;
    public Text sl;
    public Text ve;

    // Start is called before the first frame update
    void Start()
    {
        kk.text = "5";
        ge.text = "5";
        wn.text = "5";
        em.text = "5";
        in_.text = "5";
        wk.text = "5";

        k.text = "1";
        g.text = "1";
        v.text = "1";
        t.text = "1";
        f.text = "1";

    }

    // Update is called once per frame
    void Update()
    {
        SaveAttributes();
        Calculate();

        int.TryParse(kk.text, out kk_int);
        int.TryParse(ge.text, out ge_int);
        int.TryParse(wn.text, out wn_int);
        int.TryParse(em.text, out em_int);
        int.TryParse(in_.text, out in_int);
        int.TryParse(wk.text, out wk_int);

        int.TryParse(k.text, out k_int);
        int.TryParse(g.text, out g_int);
        int.TryParse(v.text, out v_int);
        int.TryParse(t.text, out t_int);
        int.TryParse(f.text, out f_int);
    }

    void OnGUI()
    {
        awAktuell = 15 - k_int - g_int - v_int - t_int - f_int;
        maxAW.text = awAktuell.ToString();

        wwAktuell = 62 - kk_int - ge_int - wn_int - em_int - in_int - wk_int;
        maxWW.text = wwAktuell.ToString();
    }

    public void SaveAttributes()
    {
        FindObjectOfType<CharacterSheet>().kk = kk_int;
        FindObjectOfType<CharacterSheet>().ge = ge_int;
        FindObjectOfType<CharacterSheet>().wn = wn_int;
        FindObjectOfType<CharacterSheet>().em = em_int;
        FindObjectOfType<CharacterSheet>().in_ = in_int;
        FindObjectOfType<CharacterSheet>().wk = wk_int;

        FindObjectOfType<CharacterSheet>().k = k_int;
        FindObjectOfType<CharacterSheet>().g = g_int;
        FindObjectOfType<CharacterSheet>().v = v_int;
        FindObjectOfType<CharacterSheet>().t = t_int;
        FindObjectOfType<CharacterSheet>().f = f_int;
    }

    public void Calculate()
    {
        FindObjectOfType<CharacterSheet>().lp = ((FindObjectOfType<CharacterSheet>().kk + FindObjectOfType<CharacterSheet>().kk + FindObjectOfType<CharacterSheet>().wk) / 2) + 10;
        FindObjectOfType<CharacterSheet>().gg = ((FindObjectOfType<CharacterSheet>().wk + FindObjectOfType<CharacterSheet>().em) / 2) + 15;
        FindObjectOfType<CharacterSheet>().sl = (FindObjectOfType<CharacterSheet>().ge + FindObjectOfType<CharacterSheet>().ge + FindObjectOfType<CharacterSheet>().wn) / 3;
        FindObjectOfType<CharacterSheet>().ve = 5 + FindObjectOfType<CharacterSheet>().level * 2;

        lp.text = FindObjectOfType<CharacterSheet>().lp.ToString();
        gg.text = FindObjectOfType<CharacterSheet>().gg.ToString();
        sl.text = FindObjectOfType<CharacterSheet>().sl.ToString();
        ve.text = FindObjectOfType<CharacterSheet>().ve.ToString();
    }
}
