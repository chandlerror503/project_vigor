﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solarsystem : MonoBehaviour
{
    public Planet planet;
    public GameObject prefabSun;


    // Use this for initialization

    public void InstantiateSolarsystem(int planets)
    {

        for (int i = 0; i < planets; i++)
        {
            planet.InstantiatePlanet(Random.Range(1, 6), Random.Range(1, 4), Random.Range(1, 5), Random.Range(1, 5));
        }

        Instantiate(prefabSun, new Vector3(0, 0,0), new Quaternion(0, 0, 0, 0));
        //OnUnitSphere + Position of Sun Vector3 = center of sphere 
    }
}
