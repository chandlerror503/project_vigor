﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Galaxy : MonoBehaviour
{

    public Solarsystem solarsystem;

    void Start()
    {
        InstantiateGalaxy(10);
    }

    public void InstantiateGalaxy(int solarsystems)
    {

        for (int i = 0; i < solarsystems; i++)
        {
            solarsystem.InstantiateSolarsystem(Random.Range(2, 5));
        }
    }

}
