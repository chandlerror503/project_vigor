﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    public Camera cam;
    public float camPosZ;
    public Vector3 dragOrigin;
    public Vector3 dragEnd;
    public float smooth;
    public Text textfield;
    public float dragSpeed;

    // Use this for initialization
    void Start()
    {
        camPosZ = GetComponent<Transform>().position.z;
    }

    // Update is called once per frame
    void Update()
    {
        //cam.transform.LookAt(Vector3.zero);

        //drag
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            dragEnd = Input.mousePosition;
            Vector3 move = Camera.main.ScreenToViewportPoint(dragOrigin-dragEnd);
            transform.Translate(move *dragSpeed);
        }

        //zoom
        if (Input.GetAxis("Mouse ScrollWheel")>0)
        {
            if(cam.transform.position.z < 10)
            {
                camPosZ+=10;
            }
            
           cam.transform.position = new Vector3(transform.position.x, transform.position.y, camPosZ);

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (cam.transform.position.z < 1000)
            {
                camPosZ-=10;
            }

          cam.transform.position = new Vector3(transform.position.x, transform.position.y, camPosZ);
        }

        //cam.transform.position = new Vector3(x, y, camPosZ);


        //select object
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray mouseRay = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(mouseRay, out hit, 1000f))
            {
                if(hit.collider.tag == "Planet")
                {
                    textfield.text = "Planet Information: \nLife: " + hit.collider.GetComponent<Planet>().lifeString + "\n" + 
                                                                  "Terrain: " + hit.collider.GetComponent<Planet>().terrainString + "\n" +
                                                                  "Size: " + hit.collider.GetComponent<Planet>().sizeString + "\n" +
                                                                  "Distance: " + hit.collider.GetComponent<Planet>().distanceString + "\n" +
                                                                  "Vigorit: " + hit.collider.GetComponent<Planet>().vigoritArt;
                }
            }
        }
        
    }
}
//Terrain, Size, Distance, Vigorit