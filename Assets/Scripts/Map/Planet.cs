﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    public Vector3 position;
    public Vector3 scale;
    public GameObject prefabPlanet;
    public GameObject prefabSun;
    public GameObject solarsystem;
    public string lifeString;
    public string terrainString;
    public string sizeString;
    public string distanceString;
    public string vigoritArt; //set vigorit


    void Update()
    {
        if (terrainString != null || terrainString != "")
        {

            switch (terrainString)
            {
                case "lava":
                    prefabPlanet.GetComponent<Renderer>().material.color = Color.red;
                    break;

                case "dry":
                    prefabPlanet.GetComponent<Renderer>().material.color = Color.grey;
                    break;

                case "water":
                    prefabPlanet.GetComponent<Renderer>().material.color = Color.blue;
                    break;

                case "ice":
                    prefabPlanet.GetComponent<Renderer>().material.color = Color.white;
                    break;

                case "gas":
                    prefabPlanet.GetComponent<Renderer>().material.color = Color.green;
                    break;
            }
        }
    }


    public void InstantiatePlanet(int terrain, int life, int size, int distanceToSun)
    {

        switch (terrain)
        {
            case 1:
                terrainString = "lava";
                break;

            case 2:
                terrainString = "dry";
                break;

            case 3:
                terrainString = "water";
                break;

            case 4:
                terrainString = "ice";
                break;

            case 5:
                terrainString = "gas";
                break;
        }


        /*
         * Restrictions life
        */

        switch (life)
        {
            case 1:
                lifeString = "possible";
                break;

            case 2:
                lifeString = "populated";
                break;

            case 3:
                lifeString = "highly populated";
                break;
        }

        switch (size)
        {
            case 1:
                sizeString = "small";
                prefabPlanet.GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
                break;

            case 2:
                sizeString = "medium";
                prefabPlanet.GetComponent<Transform>().localScale = new Vector3(2, 2, 2);
                break;

            case 3:
                sizeString = "big";
                prefabPlanet.GetComponent<Transform>().localScale = new Vector3(3, 3, 3);
                break;

            case 4:
                sizeString = "gigantic";
                prefabPlanet.GetComponent<Transform>().localScale = new Vector3(4, 4, 4);
                break;
        }


        if (terrainString == "lava")
        {
            distanceToSun = 1;
        }

        if (terrainString == "water")
        {
            distanceToSun = Random.Range(2, 3);
        }

        if (terrainString == "ice")
        {
            distanceToSun = 4;
        }


        switch (distanceToSun)
        {
            case 1:
                distanceString = "small";
                life = 1;
                Instantiate(prefabPlanet, Random.onUnitSphere * 100, new Quaternion(0, 0, 0, 0));
                break;

            case 2:
                distanceString = "medium";
                life = Random.Range(1, 4);
                Instantiate(prefabPlanet, Random.onUnitSphere * 200, new Quaternion(0, 0, 0, 0));
                break;

            case 3:
                distanceString = "big";
                life = 1;
                Instantiate(prefabPlanet, Random.onUnitSphere * 300, new Quaternion(0, 0, 0, 0));

                break;

            case 4:
                distanceString = "huge";
                lifeString = "impossible";
                Instantiate(prefabPlanet, Random.onUnitSphere * 400, new Quaternion(0, 0, 0, 0));
                break;
        }
    }
}
