﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavedChar : MonoBehaviour
{
    public Text buttonT;
    public int index;
    public GameObject parent;
    public GameObject buttonPre;
    public Vector3 pos;

    public void Update()
    {
    }

    public void createButtons()
    {

        for (int i = 0; i < FindObjectOfType<GameManager>().saveID; i++)
        {
            string value = PlayerPrefs.GetString("charName_" + (i + 1));

            if (value != null | value != "")
            {
                pos = new Vector3(buttonPre.transform.position.x, buttonPre.transform.position.y - (i * 55), buttonPre.transform.position.z);
                GameObject saveButton = Instantiate(buttonPre) as GameObject;
                saveButton.transform.position = pos;
                saveButton.transform.SetParent(parent.transform, false);

                saveButton.GetComponentInChildren<Text>().text = PlayerPrefs.GetString("charName_" + (i + 1));
            }
        }
    }

}
