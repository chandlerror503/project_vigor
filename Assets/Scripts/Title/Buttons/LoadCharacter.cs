﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadCharacter : MonoBehaviour
{
    public string charName;
    public int charSaveID;

    // Start is called before the first frame update

    public void Update()
    {
        charName = GetComponentInChildren<Text>().text;
        charSaveID = PlayerPrefs.GetInt(charName);
    }
    public void OnClick()
    {
 
        FindObjectOfType<GameManager>().currentSaveID = PlayerPrefs.GetInt(charName);
        FindObjectOfType<GameManager>().LoadCharacter();


        SceneManager.LoadScene(2);
    }
}
