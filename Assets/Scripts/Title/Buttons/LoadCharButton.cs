﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadCharButton : MonoBehaviour
{
    public GameObject displaySavedChars;

    // Start is called before the first frame update
    public void OnClick()
    {
        if (FindObjectOfType<GameManager>().saveID != 0)
        {
            displaySavedChars.SetActive(true);
            FindObjectOfType<SavedChar>().createButtons();
        }
    }
}
