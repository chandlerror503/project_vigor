﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int saveID;
    public int currentSaveID;
    public CharacterSheet currentCharacter;
    public Button loadButton;


    void Awake()
    {
        int numGameManagers = FindObjectsOfType<GameManager>().Length;
        if (numGameManagers != 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        saveID = PlayerPrefs.GetInt("saveID");
    }

    // Start is called before the first frame update
    void Start()
    {
        if (saveID == 0)
        {
            loadButton.interactable = false;
            loadButton.GetComponentInChildren<Text>().color = Color.gray;
        }
        else
        {
            loadButton.interactable = true;
            loadButton.GetComponentInChildren<Text>().color = Color.white;

        }
    }

    public void LoadCharacter()
    {
        currentCharacter = gameObject.AddComponent<CharacterSheet>();

        currentCharacter.charName = PlayerPrefs.GetString("charName_" + currentSaveID);

        currentCharacter.volk = PlayerPrefs.GetString("volk_" + currentSaveID);
        currentCharacter.volkStaerke = PlayerPrefs.GetString("volkStaerke_" + currentSaveID);
        currentCharacter.volkStaerkeInfo = PlayerPrefs.GetString("volkStaerkeInfo_" + currentSaveID);
        currentCharacter.modul = PlayerPrefs.GetString("modul_" + currentSaveID);
        currentCharacter.stil = PlayerPrefs.GetString("stil_" + currentSaveID);

        currentCharacter.basisf1 = PlayerPrefs.GetString("basisf1_" + currentSaveID);
        currentCharacter.basisf1Info = PlayerPrefs.GetString("basisf1Info_" + currentSaveID);
        currentCharacter.basisf2 = PlayerPrefs.GetString("basisf2_" + currentSaveID);
        currentCharacter.basisf2Info = PlayerPrefs.GetString("basisf2Info_" + currentSaveID);
        currentCharacter.basisf3 = PlayerPrefs.GetString("basisf3_" + currentSaveID);
        currentCharacter.basisf3Info = PlayerPrefs.GetString("basisf3Info_" + currentSaveID);
        currentCharacter.basisStaerke = PlayerPrefs.GetString("basisStaerke_" + currentSaveID);
        currentCharacter.basisStaerkeInfo = PlayerPrefs.GetString("basisStaerkeInfo_" + currentSaveID);
        currentCharacter.stilf1 = PlayerPrefs.GetString("stilf1_" + currentSaveID);
        currentCharacter.stilf1Info = PlayerPrefs.GetString("stilf1Info_" + currentSaveID);
        currentCharacter.stilf2 = PlayerPrefs.GetString("stilf2_" + currentSaveID);
        currentCharacter.stilf2Info = PlayerPrefs.GetString("stilf2Info_" + currentSaveID);
        currentCharacter.stilStaerke = PlayerPrefs.GetString("stilStaerke_" + currentSaveID);
        currentCharacter.stilStaerkeInfo = PlayerPrefs.GetString("stilStaerkeInfo_" + currentSaveID);



        currentCharacter.profession = PlayerPrefs.GetString("profession_" + currentSaveID);
        currentCharacter.professionsStaerken.Add(PlayerPrefs.GetString("professionsStaerke0_" + currentSaveID));
        currentCharacter.professionsStaerken.Add(PlayerPrefs.GetString("professionsStaerke1_" + currentSaveID));
        currentCharacter.professionsInfo.Add(PlayerPrefs.GetString("professionsInfo0_" + currentSaveID));
        currentCharacter.professionsInfo.Add(PlayerPrefs.GetString("professionsInfo1_" + currentSaveID));

        currentCharacter.ausstattungTyp = PlayerPrefs.GetString("ausstattungTyp_" + currentSaveID);
        currentCharacter.schild = PlayerPrefs.GetString("schild_" + currentSaveID);

        currentCharacter.charakterAussehen = PlayerPrefs.GetString("charakterAussehen_" + currentSaveID);
        currentCharacter.charakterHintergrundgeschichte = PlayerPrefs.GetString("charakterHintergrundgeschichte_" + currentSaveID);
        currentCharacter.charakterNotizen = PlayerPrefs.GetString("charakterNotizen_" + currentSaveID);
        for (int i = 0; i < currentCharacter.charakterZug.Length; i++)
        {
            string zug;
            zug = PlayerPrefs.GetString("charakterZug" + i + "_" + currentSaveID);
            currentCharacter.charakterZug.SetValue(zug, i);
        }

        currentCharacter.level = PlayerPrefs.GetInt("level_" + currentSaveID);

        currentCharacter.kk = PlayerPrefs.GetInt("kk_" + currentSaveID);
        currentCharacter.ge = PlayerPrefs.GetInt("ge_" + currentSaveID);
        currentCharacter.wn = PlayerPrefs.GetInt("wn_" + currentSaveID);
        currentCharacter.em = PlayerPrefs.GetInt("em_" + currentSaveID);
        currentCharacter.in_ = PlayerPrefs.GetInt("in_" + currentSaveID);
        currentCharacter.wk = PlayerPrefs.GetInt("wk_" + currentSaveID);

        currentCharacter.k = PlayerPrefs.GetInt("k_" + currentSaveID);
        currentCharacter.g = PlayerPrefs.GetInt("g_" + currentSaveID);
        currentCharacter.v = PlayerPrefs.GetInt("v_" + currentSaveID);
        currentCharacter.t = PlayerPrefs.GetInt("t_" + currentSaveID);
        currentCharacter.f = PlayerPrefs.GetInt("f_" + currentSaveID);

        currentCharacter.lp = PlayerPrefs.GetInt("lp_" + currentSaveID);
        currentCharacter.gg = PlayerPrefs.GetInt("gg_" + currentSaveID);
        currentCharacter.sl = PlayerPrefs.GetInt("sl_" + currentSaveID);
        currentCharacter.ve = PlayerPrefs.GetInt("ve_" + currentSaveID);

        for (int i = 0; i < 6; i++)//determine size 
        {
            currentCharacter.schwaechen.Add(PlayerPrefs.GetString("schwaechen" + i + "_" + currentSaveID));
        }

        for (int i = 0; i < currentCharacter.schwaechen.Count; i++)
        {
            currentCharacter.schwaechenInfo.Add(PlayerPrefs.GetString("schwaechenInfo" + i + "_" + currentSaveID));
        }

        currentCharacter.xp = PlayerPrefs.GetInt("xp_" + currentSaveID);
        currentCharacter.hp = PlayerPrefs.GetInt("hp_" + currentSaveID);
        currentCharacter.leonen = PlayerPrefs.GetInt("leonen_" + currentSaveID);
    }
}

