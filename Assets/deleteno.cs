﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deleteno : MonoBehaviour
{
    // Start is called before the first frame update
    public void OnClick()
    {
        FindObjectOfType<DeleteChar>().delete = false;
        FindObjectOfType<DeleteChar>().deleteMessage.SetActive(false);
    }
}
