﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterOverview : MonoBehaviour
{
    public Text viewName;
    public Text viewVolk;
    public Text viewProfession;
    public Text viewModifikation;
    public Text viewStil;
    public Text viewAussehen;
    public Text viewHintergrundgeschichte;
    public Text viewCharakterzüge;

    public Text viewWurfwerte;
    public Text viewAusgleichswerte;
    public Text viewAttribute;

    //Fähigkeiten

    public Text viewVS;
    public Text viewVSInfo;
    public Text viewPS1;
    public Text viewPS1Info;
    public Text viewPS2;
    public Text viewPS2Info;
    public Text viewSS;
    public Text viewSSInfo;
    public Text viewMS;
    public Text viewMSInfo;

    public Text viewSchwaechen;

    public Text viewInventar;

    public Text viewNotizen;
    public bool weak;


    void Start()
    {
        weak = false;
    }

    // Update is called once per frame
    void Update()
    {
        viewName.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charName;
        viewVolk.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().volk;
        viewProfession.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().profession;
        viewModifikation.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().modul;
        viewStil.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().stil;
        viewAussehen.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterAussehen;
        viewHintergrundgeschichte.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterHintergrundgeschichte;
        viewCharakterzüge.text = "Rechtverpflichtet - Chaotisch  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[0].ToString() + ">\r\n" +
                                 "Ehrenhaft - Hinterlistig  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[1].ToString() + ">\r\n" +
                                 "Introvertiert - Extrovertiert  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[2].ToString() + ">\r\n" +
                                 "Pazifistisch - Streitsuchend  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[3].ToString() + ">\r\n" +
                                 "Eigennützig - Gemeinnützig  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[4].ToString() + ">\r\n" +
                                 "Arbeitsam - Faul  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[5].ToString() + ">\r\n" +
                                 "Loyal - Selbstständig  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[6].ToString() + ">\r\n" +
                                 "Nachtragend - Vergebend  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[7].ToString() + ">\r\n" +
                                 "Wortgewand - Wortkarg  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[8].ToString() + ">\r\n" +
                                 "Großzügig - Geizig  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[9].ToString() + ">\r\n" +
                                 "Naiv - Hinterfragend  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[10].ToString() + ">\r\n" +
                                 "Witzig - Ernst " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[11].ToString() + ">\r\n" +
                                 "Traditionell - Weltoffen  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[12].ToString() + ">\r\n" +
                                 "Kalkulierend - Willkürlich  <" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterZug[13].ToString() + ">\r\n";



        viewWurfwerte.text = "Körperkraft (KK): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().kk +
                                 "\r\nGeschick (GE): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().ge +
                                 "\r\nWahrnehmung (WN): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().wn +
                                 "\r\nEmpathie (EM): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().em +
                                 "\r\nIntelligenz (IN): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().in_ +
                                 "\r\nWillenskraft (WK): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().wk;

        viewAusgleichswerte.text = "Körper(K): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().k +
                                "\r\nGeist (G): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().g +
                                "\r\nVerstand (V): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().v +
                                "\r\nTechnik (T): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().t +
                                "\r\nFinesse (F): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().f;

        viewAttribute.text = "Lebenspunkte(LP): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().lp +
                                 "\r\nGeistige Gesundheit (GG): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().gg +
                                 "\r\nSchnelligkeit (SL): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().sl +
                                 "\r\nVigoritenergie (VE): " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().ve;



        //Fähigkeiten

        viewVS.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().volkStaerke;
        viewVSInfo.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().volkStaerkeInfo;
        viewPS1.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().professionsStaerken[0];
        viewPS1Info.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().professionsInfo[0];
        viewPS2.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().professionsStaerken[1];
        viewPS2Info.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().professionsInfo[1];
        if (FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().stilStaerke == null || FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().stilStaerke == "")
        {
            viewMS.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().basisStaerke;
            viewMSInfo.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().basisStaerkeInfo;
            //Hide Bezeichnung
        }
        else
        {
            viewSS.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().stilStaerke;
            viewSSInfo.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().stilStaerkeInfo;
            //Hide Bezeichnung
        }

        if (!weak)
        {
            for (int i = 0; i < FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().schwaechen.Count; i++)
            {
                int j = i + 1;
                viewSchwaechen.text = viewSchwaechen.text + j + ": " + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().schwaechen[i] + "\r\n" + FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().schwaechenInfo[i] + "\r\n\r\n";
            }
            weak = true;
        }


        //viewInventar.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>();
        viewNotizen.text = FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charakterNotizen;

    }

}

