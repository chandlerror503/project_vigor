﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeleteChar : MonoBehaviour
{

    public GameObject deleteMessage;
    public Text messageText;
    public bool delete;
    public int saveID;

    // Start is called before the first frame update
    public void OnClick()
    {
        messageText.text = "Möchtest du den Charakter wirklich löschen?";
        deleteMessage.SetActive(true);
        saveID = FindObjectOfType<GameManager>().currentSaveID;


    }

    private void Update()
    {
        
        if (delete)
        {

            PlayerPrefs.DeleteKey("charName_" + saveID);
            PlayerPrefs.DeleteKey("volk_" + saveID);
            PlayerPrefs.DeleteKey("volkStaerke_" + saveID);
            PlayerPrefs.DeleteKey("volkStaerkeInfo_" + saveID);
            PlayerPrefs.DeleteKey("modul_" + saveID);
            PlayerPrefs.DeleteKey("stil_" + saveID);

            PlayerPrefs.DeleteKey("basisf1_" + saveID);
            PlayerPrefs.DeleteKey("basisf1Info_" + saveID);
            PlayerPrefs.DeleteKey("basisf2_" + saveID);
            PlayerPrefs.DeleteKey("basisf2Info_" + saveID);
            PlayerPrefs.DeleteKey("basisf3_" + saveID);
            PlayerPrefs.DeleteKey("basisf3Info_" + saveID);
            PlayerPrefs.DeleteKey("basisStaerke_" + saveID);
            PlayerPrefs.DeleteKey("basisStaerkeInfo_" + saveID);
            PlayerPrefs.DeleteKey("stilf1_" + saveID);
            PlayerPrefs.DeleteKey("stilf1Info_" + saveID);
            PlayerPrefs.DeleteKey("stilf2_" + saveID);
            PlayerPrefs.DeleteKey("stilf2Info_" + saveID);
            PlayerPrefs.DeleteKey("stilStaerke_" + saveID);
            PlayerPrefs.DeleteKey("stilStaerkeInfo_" + saveID);

            PlayerPrefs.DeleteKey("profession_" + saveID);
            for (int i = 0; i < 2; i++)
            {
                PlayerPrefs.DeleteKey("professionsStaerke" + i + "_" + saveID);
            }
            PlayerPrefs.DeleteKey("ausstattungTyp_" + saveID);
            PlayerPrefs.DeleteKey("schild_" + saveID);

            PlayerPrefs.DeleteKey("charakterAussehen_" + saveID);
            PlayerPrefs.DeleteKey("charakterHintergrundgeschichte_" + saveID);
            PlayerPrefs.DeleteKey("charakterNotizen_" + saveID);
            for (int i = 0; i < 14; i++)
            {
                PlayerPrefs.DeleteKey("charakterZug" + i + "_" + saveID);
            }

            PlayerPrefs.DeleteKey("level_" + saveID);

            PlayerPrefs.DeleteKey("kk_" + saveID);
            PlayerPrefs.DeleteKey("ge_" + saveID);
            PlayerPrefs.DeleteKey("wn_" + saveID);
            PlayerPrefs.DeleteKey("em_" + saveID);
            PlayerPrefs.DeleteKey("in_" + saveID);
            PlayerPrefs.DeleteKey("wk_" + saveID);

            PlayerPrefs.DeleteKey("k_" + saveID);
            PlayerPrefs.DeleteKey("g_" + saveID);
            PlayerPrefs.DeleteKey("v_" + saveID);
            PlayerPrefs.DeleteKey("t_" + saveID);
            PlayerPrefs.DeleteKey("f_" + saveID);
            PlayerPrefs.DeleteKey("lp_" + saveID);
            PlayerPrefs.DeleteKey("gg_" + saveID);
            PlayerPrefs.DeleteKey("sl_" + saveID);
            PlayerPrefs.DeleteKey("ve_" + saveID);

            for (int i = 0; i < FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().schwaechen.Count; i++)
            {
                PlayerPrefs.DeleteKey("schwaechen" + i + "_" + saveID);
            }

            PlayerPrefs.DeleteKey("xp_" + saveID);
            PlayerPrefs.DeleteKey("hp_" + saveID);

            PlayerPrefs.DeleteKey(FindObjectOfType<GameManager>().GetComponentInChildren<CharacterSheet>().charName);

            PlayerPrefs.Save();

            deleteMessage.SetActive(false);
            SceneManager.LoadScene(0);

            delete = false;
        }
    }

  
}
