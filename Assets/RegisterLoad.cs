﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterLoad : MonoBehaviour
{

    public GameObject charakter;
    public GameObject werte;
    public GameObject faehigkeiten;
    public GameObject staerken;
    public GameObject schwaechen;
    public GameObject inventar;
    public GameObject notizen;

    public string buttonName;

    void Start()
    {
        buttonName = GetComponentInChildren<Text>().text;
    }

    public void OnClick()
    {
        switch (buttonName)
        {
            case "Charakter":
                charakter.SetActive(true);
                werte.SetActive(false);
                faehigkeiten.SetActive(false);
                staerken.SetActive(false);
                schwaechen.SetActive(false);
                inventar.SetActive(false);
                notizen.SetActive(false);
                break;

            case "Werte":
                charakter.SetActive(false);
                werte.SetActive(true);
                faehigkeiten.SetActive(false);
                staerken.SetActive(false);
                schwaechen.SetActive(false);
                inventar.SetActive(false);
                notizen.SetActive(false);
                break;

            case "Fähigkeiten":
                charakter.SetActive(false);
                werte.SetActive(false);
                faehigkeiten.SetActive(true);
                staerken.SetActive(false);
                schwaechen.SetActive(false);
                inventar.SetActive(false);
                notizen.SetActive(false);
                break;

            case "Stärken":
                charakter.SetActive(false);
                werte.SetActive(false);
                faehigkeiten.SetActive(false);
                staerken.SetActive(true);
                schwaechen.SetActive(false);
                inventar.SetActive(false);
                notizen.SetActive(false);
                break;

            case "Schwächen":
                charakter.SetActive(false);
                werte.SetActive(false);
                faehigkeiten.SetActive(false);
                staerken.SetActive(false);
                schwaechen.SetActive(true);
                inventar.SetActive(false);
                notizen.SetActive(false);
                break;

            case "Inventar":
                charakter.SetActive(false);
                werte.SetActive(false);
                faehigkeiten.SetActive(false);
                staerken.SetActive(false);
                schwaechen.SetActive(false);
                inventar.SetActive(true);
                notizen.SetActive(false);
                break;

            case "Notizen":
                charakter.SetActive(false);
                werte.SetActive(false);
                faehigkeiten.SetActive(false);
                staerken.SetActive(false);
                schwaechen.SetActive(false);
                inventar.SetActive(false);
                notizen.SetActive(true);
                break;


        }
    }
}
